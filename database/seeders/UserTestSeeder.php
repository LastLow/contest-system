<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTestSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $test = \App\Models\User::create(
            [
                'name'     => 'test',
                'login'    => 'test',
                'password' => Hash::make('123456'),
                'open_password' => '123456'
            ]
        );

        $admin = \App\Models\User::create(
            [
                'name'     => 'admin',
                'login'    => 'admin',
                'password' => Hash::make(env('ADMIN_PASSWORD'))
            ]
        );

        $roles = [
            [
                "role" => "admin",
                "user" => $admin
            ],
            [
                "role" => "student",
                "user" => $test
            ]
        ];

        foreach ($roles as $value) {
            $role = Role::where('name', '=', $value['role'])->first();
            if (!$role) continue;

            DB::insert(
                'insert into users_roles (role_id, user_id) values (?,?)',
                [
                    $role->id,
                    $value['user']->id
                ]
            );
        }
    }
}
