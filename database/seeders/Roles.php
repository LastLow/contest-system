<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                "name" => "student",
                "slug" => "Студент"
            ],
            [
                "name" => "teacher",
                "slug" => "Преподаватель"
            ],
            [
                "name" => "admin",
                "slug" => "Администратор"
            ]
        ];

        foreach ($roles as $role) {
            Role::create([
                "name" => $role['name'],
                "slug" => $role['slug']
            ]);
        }
    }
}
