<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rolesPermissions = [
            [
                "name" => "admin",
                "permissions" => [
                    [
                        "name" => "create_courses_tasks",
                        "slug" => "Создание курсов и задач"
                    ],
                    [
                        "name" => "remove_courses_tasks",
                        "slug" => "Удаление курсов и задач"
                    ],
                    [
                        "name" => "change_courses_tasks",
                        "slug" => "Изменение курсов и задач"
                    ],
                    [
                        "name" => "create_appointment",
                        "slug" => "Назначение курсов и задач студентам"
                    ],
                    [
                        "name" => "remove_appointment",
                        "slug" => "Удаление назначений курсов и задач студентам"
                    ],
                    [
                        "name" => "access_admin",
                        "slug" => "Доступ в админ панель"
                    ],
                    [
                        "name" => "access_student",
                        "slug" => "Доступ в студенскую панель"
                    ],
                    [
                        "name" => "change_permissions_teachers",
                        "slug" => "Изменение прав преподавательского аккаунта"
                    ],
                    [
                        "name" => "approve_users_students",
                        "slug" => "Одобрение регистрации студенского аккаунта"
                    ],
                    [
                        "name" => "approve_users_teachers",
                        "slug" => "Одобрение регистрации преподавательского аккаунта"
                    ],
                    [
                        "name" => "ban_users_students",
                        "slug" => "Бан студенского аккаунта"
                    ],
                    [
                        "name" => "ban_users_teachers",
                        "slug" => "Бан преподавательского аккаунта"
                    ],
                    [
                        "name" => "delete_users_students",
                        "slug" => "Удаление студенского аккаунта"
                    ],
                    [
                        "name" => "delete_users_teachers",
                        "slug" => "Удаление преподавательского аккаунта"
                    ],
                    [
                        "name" => "create_roles",
                        "slug" => "Создание ролей"
                    ],
                    [
                        "name" => "delete_roles",
                        "slug" => "Удаление ролей"
                    ],
                    [
                        "name" => "change_roles",
                        "slug" => "Изменение ролей"
                    ],
                    [
                        "name" => "change_users_role",
                        "slug" => "Изменение ролей у пользователей"
                    ]
                ]
            ],
            [
                "name" => "teacher",
                "permissions" => [
                    [
                        "name" => "create_courses_tasks",
                        "slug" => "Создание курсов и задач"
                    ],
                    [
                        "name" => "create_appointment",
                        "slug" => "Назначение курсов и задач студентам"
                    ],
                    [
                        "name" => "remove_appointment",
                        "slug" => "Удаление назначений курсов и задач студентам"
                    ],
                    [
                        "name" => "access_admin",
                        "slug" => "Доступ в админ панель"
                    ],
                    [
                        "name" => "approve_users_students",
                        "slug" => "Одобрение регистрации студенского аккаунта"
                    ],
                    [
                        "name" => "ban_users_students",
                        "slug" => "Бан студенского аккаунта"
                    ],
                    [
                        "name" => "create_roles",
                        "slug" => "Создание ролей"
                    ],
                    [
                        "name" => "delete_roles",
                        "slug" => "Удаление ролей"
                    ],
                    [
                        "name" => "change_roles",
                        "slug" => "Изменение ролей"
                    ]
                ]
            ],
            [
                "name" => "student",
                "permissions" => [
                    [
                        "name" => "access_student",
                        "slug" => "Доступ в студенскую панель"
                    ]
                ]
            ]
        ];

        foreach ($rolesPermissions as $value) {
             $role = Role::where('name', '=', $value['name'])->first();
             if (!$role) continue;

             foreach ($value['permissions'] as $permission) {
                 $permissionCollection = Permission::where('name', '=', $permission['name'])->first();
                 if (!$permissionCollection) continue;

                 DB::insert('insert into roles_permissions (role_id, permission_id) values (?,?)',
                     [
                        $role->id,
                        $permissionCollection->id
                     ]
                 );
             }
        }
    }
}
