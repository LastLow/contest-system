<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Permissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permissions = [
            [
                "name" => "create_courses_tasks",
                "slug" => "Создание курсов и задач"
            ],
            [
                "name" => "remove_courses_tasks",
                "slug" => "Удаление курсов и задач"
            ],
            [
                "name" => "change_courses_tasks",
                "slug" => "Изменение курсов и задач"
            ],
            [
                "name" => "create_appointment",
                "slug" => "Назначение курсов и задач студентам"
            ],
            [
                "name" => "remove_appointment",
                "slug" => "Удаление назначений курсов и задач студентам"
            ],
            [
                "name" => "access_admin",
                "slug" => "Доступ в админ панель"
            ],
            [
                "name" => "access_student",
                "slug" => "Доступ в студенскую панель"
            ],
            [
                "name" => "change_permissions_teachers",
                "slug" => "Изменение прав преподавательского аккаунта"
            ],
            [
                "name" => "approve_users_students",
                "slug" => "Одобрение регистрации студенского аккаунта"
            ],
            [
                "name" => "approve_users_teachers",
                "slug" => "Одобрение регистрации преподавательского аккаунта"
            ],
            [
                "name" => "ban_users_students",
                "slug" => "Бан студенского аккаунта"
            ],
            [
                "name" => "ban_users_teachers",
                "slug" => "Бан преподавательского аккаунта"
            ],
            [
                "name" => "delete_users_students",
                "slug" => "Удаление студенского аккаунта"
            ],
            [
                "name" => "delete_users_teachers",
                "slug" => "Удаление преподавательского аккаунта"
            ],
            [
                "name" => "create_roles",
                "slug" => "Создание ролей"
            ],
            [
                "name" => "delete_roles",
                "slug" => "Удаление ролей"
            ],
            [
                "name" => "change_roles",
                "slug" => "Изменение ролей"
            ],
            [
                "name" => "change_users_role",
                "slug" => "Изменение ролей у пользователей"
            ]
        ];

        foreach ($permissions as $permission) {
            Permission::create(
                [
                    "name" => $permission['name'],
                    "slug" => $permission['slug']
                ]
            );
        }
    }
}
