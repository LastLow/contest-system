<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('result_test', function (Blueprint $table) {
            $table->id();
            $table->text('in');
            $table->text('out');
            $table->integer('score');
            $table->integer('time');
            $table->enum('status', ['success', 'error']);
            $table->unsignedBigInteger('history_id');
            $table->foreign('history_id')->references('id')->on('history')->onDelete('cascade');
            $table->text('test_out');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('result_test');
    }
};
