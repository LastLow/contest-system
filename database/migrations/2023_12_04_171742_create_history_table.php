<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('history', function (Blueprint $table) {
            $table->id();
            $table->text('code');
            $table->enum('status', ['zero', 'error', 'success'])
                ->default('zero');
            $table->integer('score')->default(0);
            $table->integer('max_score');
            $table->string('language')->default('python');
            $table->unsignedBigInteger('user_course_task_id');
            $table->foreign('user_course_task_id')->references('id')
                ->on('user_course_tasks')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('history');
    }
};
