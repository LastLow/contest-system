<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('open_password')->after('password')->nullable();
            $table->dropColumn('middle_name');
            $table->dropColumn('last_name');
            $table->renameColumn('email', 'login');
            $table->dropColumn('vstu_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('open_password');
            $table->string('middle_name', 255)->nullable();
            $table->string('last_name', 255)->nullable();
            $table->renameColumn('login', 'email');
            $table->string('vstu_id')->nullable();
        });
    }
};
