<?php

use App\Http\ControllersNew\AuthController;
use App\Http\ControllersNew\CourseController;
use App\Http\ControllersNew\TaskController;
use App\Http\ControllersNew\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', [AuthController::class, 'login']);


Route::middleware(['auth:sanctum'])->group(function() {
    Route::post('logout', [AuthController::class, 'logout']);

    Route::get('user', [UserController::class, 'getInfoUser']);

    Route::get('courses', [CourseController::class, 'getCoursesByUser']);
    Route::get('courses/{course}/tasks', [CourseController::class, 'getTasksByCourseId']);

    Route::get('tasks/{taskId}', [TaskController::class, 'getTaskById']);

    Route::post('check', [TaskController::class, 'sendTaskToCheck']);
});



