<?php

use App\Http\Controllers\Admin\CourseController;
use App\Http\Controllers\Admin\GroupController;
use App\Http\Controllers\Admin\TaskController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

Route::prefix('courses')->group(function() {
    Route::post('', [CourseController::class, 'create']);
    Route::post('format', [CourseController::class, 'createByShitFormat']);
    Route::get('', [CourseController::class, 'getAll']);

    Route::prefix('{course}')->group(function() {
        Route::get('tasks', [CourseController::class, 'getTasksByCourse']);

        Route::get('users', [CourseController::class, 'getUsersByCourse']);
        Route::get('users/out', [CourseController::class, 'getUsersOutCourse']);

        Route::get('groups', [CourseController::class, 'getGroupsByCourse']);
        Route::get('groups/out', [CourseController::class, 'getGroupsOutCourse']);
        Route::get('groups/{group}/tasks', [CourseController::class, 'getGroupTasksByCourse'])->scopeBindings();
    });
});


Route::prefix('users')->group(function() {
    Route::get('', [UserController::class, 'getAll']);

    Route::prefix('{user}')->group(function() {
        Route::get('courses', [UserController::class, 'getCoursesByUser']);
        Route::get('courses/{course}/tasks', [UserController::class, 'getTasksByCourse']);
        Route::get('tasks/{task}', [UserController::class, 'getInfoAboutUserTask']);
    });
});

Route::prefix('users-new')->group(function() {

    Route::prefix('{user}')->group(function() {
        Route::get('courses', [UserController::class, 'getCoursesByUser']);
        Route::get('courses/{course}/tasks', [UserController::class, 'getTasksByCourse']);
        Route::get('tasks/{task}', [UserController::class, 'getInfoAboutUserTask']);
    });
});

Route::prefix('groups')->group(function() {
    Route::get('', [GroupController::class, 'getAll']);
    Route::post('', [GroupController::class, 'createGroup']);

    Route::prefix('{group}')->group(function() {
        Route::get('users', [GroupController::class, 'getUsers']);
        Route::get('courses', [GroupController::class, 'getCoursesByGroup']);
        Route::get('exportUsers', [GroupController::class, 'exportUsers']);

        Route::get('courses/{course}/users', [GroupController::class, 'getGroupUsersByCourse']);
        Route::post('courses/{course}/random', [GroupController::class, 'setRandomCourseTasksToGroup']);
        Route::get('courses/{course}/stat', [GroupController::class, 'getStaticByGroup']);

        Route::delete('courses/{course}', [GroupController::class, 'deleteGroupFromCourse']);
        Route::delete('', [GroupController::class, 'deleteGroup']);
    });
});

Route::prefix('tasks')->group(function() {
    Route::get('{task}', [TaskController::class, 'getTask']);
    Route::post('courses/{course}/format', [TaskController::class, 'createTasksFromShitFormat']);
    Route::put('{task}', [TaskController::class, 'updateTask']);
    Route::delete('{task}', [TaskController::class, 'deleteTask']);
});
