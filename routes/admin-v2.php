<?php

use App\Http\ControllersNew\Admin\CourseController;
use App\Http\ControllersNew\Admin\GroupController;
use Illuminate\Support\Facades\Route;

Route::prefix('courses')->group(function() {
    Route::get('/', [CourseController::class, 'getCourses']);
    Route::post('/', [CourseController::class, 'create']);
    Route::post('/json', [CourseController::class, 'createByShitFormat']);
    Route::patch('{courseId}', [CourseController::class, 'updateCourse']);
    Route::delete('{courseId}', [CourseController::class, 'deleteCourse']);
});

Route::prefix('groups')->group(function() {
    Route::get('/', [GroupController::class, 'getGroups']);
    Route::post('/', [GroupController::class, 'createGroup']);
    Route::post('/{groupId}/course', [GroupController::class, 'setCourseToGroup']);
    Route::delete('/', [GroupController::class, 'deleteGroup']);
    Route::delete('/{groupId}/course', [GroupController::class, 'deleteCourseFromGroup']);
});
