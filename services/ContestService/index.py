import traceback

import mysql.connector
from subprocess import PIPE, Popen, STDOUT
import json
import time
import logging

logging.basicConfig(
    filename='./logs/error.log',
    level=logging.ERROR,
    format='%(asctime)s [%(levelname)s] - %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)

with open('./contest_server.json', 'r', encoding='utf-8') as file:
    data = json.load(file)

fname = "test"

while True:

    try:
        cnx = mysql.connector.connect(**data['db_info'])

        cursor = cnx.cursor(dictionary=True)

        query = (
            "SELECT history.id as history_id, history.code, history.language, user_course_tasks.variant_id FROM history "
            "JOIN user_course_tasks ON history.user_course_task_id = user_course_tasks.id JOIN tasks ON "
            "user_course_tasks.task_id = tasks.id WHERE status = 'zero'"
        )
        cursor.execute(query)

        history_rows = cursor.fetchall()

        try:
            for row in history_rows:

                with open(fname, 'w') as write_file:
                    write_file.write(row['code'])

                cursor.execute("SELECT * FROM tests WHERE variant_id = " + str(row['variant_id']))

                results = {}
                success_count = 0
                res_score = 0
                max_res_score = 0
                run_strs = data['languages'][row['language']]
                for row_test in cursor.fetchall():
                    test_in = row_test['in'] if row_test['in'] is not None else ""
                    test_out = row_test['out'] if row_test['out'] is not None else ""

                    result = {'score': 0, 'out': '', 'in': test_in}
                    test_score = row_test['score']
                    max_time = row_test.get('time', 10)
                    max_res_score += test_score
                    success = False
                    try:
                        for s in run_strs[:-1]:
                            run_str = s.replace("#", fname)
                            p = Popen(run_str, shell=True, stdout=PIPE, stdin=PIPE, stderr=STDOUT)
                            outs, errs = p.communicate(input=str.encode(test_in))
                        run_str = run_strs[-1].replace("#", fname)
                        p = Popen(run_str, shell=True, stdout=PIPE, stdin=PIPE, stderr=STDOUT)
                        outs, errs = p.communicate(input=str.encode(test_in), timeout=max_time)
                        outs = outs.decode("utf-8").rstrip()
                        result['out'] = outs
                        if test_out == outs:
                            result['score'] = test_score
                            res_score += test_score
                            success_count += 1
                            success = True
                    except Exception as e:
                        result['out'] = str(e)

                    query = (
                        f"INSERT INTO result_test (`in`, `out`, score, time, history_id, test_out, status) VALUES (%s, %s, %s, "
                        f"%s, %s, %s, %s)"
                    )

                    cursor.execute(query, (
                        result['in'], result['out'], result['score'], max_time, row['history_id'], row_test['out'],
                        'success' if success else 'error'))

                query = (
                    f"UPDATE history SET status = '{'success' if res_score == max_res_score else 'error'}', "
                    f"score = {res_score}, max_score = {max_res_score} WHERE id = {row['history_id']}"
                )

                cursor.execute(query)
                cnx.commit()
        except Exception as exception:
            cnx.rollback()
            error_message = traceback.format_exc()
            print(error_message)
            logging.error(error_message)

        cnx.close()

    except Exception as exception_connect:
        error_message = traceback.format_exc()
        print(error_message)
        logging.error(error_message)

    time.sleep(10)


