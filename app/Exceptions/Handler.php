<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
//        // TODO: Добавить логирование
//        // Система такая:
//        // время,
//        // класс ошибки,
//        // callback для отправки куда либо - telegram, например,
//        // краткое сообщение,
//        // ссылка на файл с подробным описание ошибки
//
//        // Отдельный файл для подробных сообщений.
//        // Сделать этот модуль как отдельный пакет.
//
//        Log::error($e->getMessage());
//
//        if ($e instanceof ValidationException) {
//            return response()->json(
//                [
//                    'errorMessage' => $e->getMessage(),
//                ],
//                404,
//                ['Content-type' => 'application/json; charset=utf-8', 'Charset' => 'utf-8'],
//                JSON_UNESCAPED_UNICODE
//            );
//        }
//
//        if ($e instanceof HandleException) {
//            $errorMessage = [
//                'errorMessage' => $e->getMessage(),
//            ];
//
//            if (!empty($e->param)) {
//                $errorMessage = array_merge($errorMessage, ['errorParams' => $e->param]);
//            }
//
//            if (!empty($e->statusMessage)) {
//                $errorMessage['status'] = $e->statusMessage;
//            }
//
//            return response()->json(
//                $errorMessage,
//                $e->status,
//                ['Content-type' => 'application/json; charset=utf-8', 'Charset' => 'utf-8'],
//                JSON_UNESCAPED_UNICODE
//            );
//        }
//
//        if ($e instanceof MethodNotAllowedHttpException) {
//            return response()->json(
//                [
//                    'errorMessage' => $e->getMessage(),
//                ],
//                405,
//                ['Content-type' => 'application/json; charset=utf-8', 'Charset' => 'utf-8'],
//                JSON_UNESCAPED_UNICODE
//            );
//        }
//
//        if ($e instanceof ModelNotFoundException) {
//            return response()->json(
//                [
//                    'errorMessage' => ($e->getModel())::getBadRequestErrorMessage(),
//                ],
//                404,
//                ['Content-type' => 'application/json; charset=utf-8', 'Charset' => 'utf-8'],
//                JSON_UNESCAPED_UNICODE
//            );
//        }
//
//        if ($e instanceof NotFoundHttpException) {
//            return response()->json(
//                [
//                    'errorMessage' => "Роут не существует",
//                ],
//                404,
//                ['Content-type' => 'application/json; charset=utf-8', 'Charset' => 'utf-8'],
//                JSON_UNESCAPED_UNICODE
//            );
//        }
//
//        return response()->json(
//            [
//                'errorMessage' => $e->getMessage(),
//            ],
//            500,
//            ['Content-type' => 'application/json; charset=utf-8', 'Charset' => 'utf-8'],
//            JSON_UNESCAPED_UNICODE
//        );

        return parent::render($request, $e);
    }
}
