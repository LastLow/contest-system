<?php

namespace App\Exceptions;

class HandleException extends \Exception
{
    public array $param;
    public int $status;
    public string $statusMessage;
    public function __construct(string $message, int $status = 500, array $param = [], string $statusMessage = "") {
        $this->param = $param;
        $this->status = $status;
        $this->statusMessage = $statusMessage;
        parent::__construct($message);

        return $this;
    }
}
