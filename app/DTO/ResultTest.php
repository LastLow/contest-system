<?php

namespace App\DTO;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="ResultTestDTO",
 *     description="ResultTest model",
 *     @OA\Xml(
 *         name="ResultTestDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="score", type="number"),
 * @OA\Property(property="test_in", type="string", nullable=true),
 * @OA\Property(property="test_out", type="string", nullable=true)
 * @OA\Property(property="out", type="string", nullable=true)
 * @OA\Property(property="status", type="string", enum={"success", "error", "zero"}),
 */
class ResultTest extends BaseSchema
{
    public int $id;
    public int $score;
    public string|null $test_in;
    public string|null $test_out;
    public string|null $out;
    public string $status;

    public function __construct(\App\Models\ResultTest $test)
    {
        $this->id = $test->id;
        $this->score = $test->score;
        $this->test_in = $test->in;
        $this->test_out = $test->out;
        $this->out = $test->test_out;
        $this->status = $test->status;
    }
}
