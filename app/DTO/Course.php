<?php

namespace App\DTO;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="CourseDTO",
 *     description="Course schema",
 *     @OA\Xml(
 *         name="CourseDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="name", type="string"),
 * @OA\Property(property="count", type="number")
 */

class Course extends BaseSchema
{
    public int $id;
    public string $name;
    public int $count;

    public function __construct(\App\Models\Course $course)
    {
        $this->id = $course->id;
        $this->name = $course->name;
        $this->count = $course->tasks->count();
    }
}
