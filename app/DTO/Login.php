<?php

namespace App\DTO;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="LoginDTO",
 *     description="Login schema",
 *     @OA\Xml(
 *         name="LoginDTO"
 *     )
 * )
 *
 * @OA\Property(property="login", type="string"),
 * @OA\Property(property="password", type="string")
 */

class Login extends BaseSchema
{
    public string $login;
    public string $password;
    public function __construct(string $login, string $password)
    {
        $this->login = $login;
        $this->password = $password;
    }
}
