<?php

namespace App\DTO;

class BaseSchema
{
    public function toArray(): array
    {
        // Получаем все свойства объекта
        $properties = get_object_vars($this);

        // Преобразуем каждое свойство в массив, если это необходимо
        foreach ($properties as $key => $value) {

            if (is_object($value) && method_exists($value, 'toArray')) {
                $properties[$key] = $value->toArray();
            }

            if (is_array($value)) {
                foreach ($value as $keyAttr => $attr) {
                    if (is_object($attr) && method_exists($attr, 'toArray')) {
                        $properties[$key][$keyAttr] = $attr->toArray();
                    }
                }
            }
        }

        return $properties;
    }
}
