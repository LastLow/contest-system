<?php

namespace App\DTO;

use App\Models\UserCourseTask;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="UserTaskDTO",
 *     description="UserTask model",
 *     @OA\Xml(
 *         name="UserTaskDTO"
 *     )
 * )
 *  @OA\Property(property="id", type="number"),
 *  @OA\Property(property="description", type="string"),
 *  @OA\Property(property="rating", type="number"),
 *  @OA\Property(property="variant", type="number"),
 *  @OA\Property(property="status", type="string", enum={"success", "error", "zero"}),
 *  @OA\Property(property="score", type="number"),
 *  @OA\Property(property="max_score", type="number")
 *  @OA\Property(property="history", type="array", nullable=true, @OA\Items(ref="#/components/schemas/History"))
 */

class UserTask extends UserTaskInfo
{
    public array $history;

    public function __construct(UserCourseTask $userCourseTask, bool $onlyTestError = true)
    {
        parent::__construct($userCourseTask);

        $this->history = $userCourseTask
            ->history
            ->map(fn(\App\Models\History $history) => new History($history, $onlyTestError))
            ->values()
            ->toArray();
    }
}
