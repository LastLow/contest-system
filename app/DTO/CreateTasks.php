<?php

namespace App\DTO;

use App\DTO\Collections\BaseCollection;
use App\Exceptions\HandleException;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class CreateTasks extends BaseCollection
{
    public function __construct(array $data)
    {
        if (empty($data))
            throw new HandleException("Нет задач", ResponseAlias::HTTP_BAD_REQUEST);

        for ($i = 1; $i <= count($data); $i++) {
            try {
                if (empty($data[$i - 1])) {
                    throw new HandleException("Задачи должны быть массивом обьектов", ResponseAlias::HTTP_BAD_REQUEST);
                }

                $this->checkItem($data[$i - 1]);

                $this->items[] = (new CreateTask(
                    $data[$i - 1]['description'],
                    $data[$i - 1]['rating'],
                    $data[$i - 1]['variants']
                ));
            } catch (HandleException $exception) {
                throw new HandleException(
                    $exception->getMessage() . ", в {$i} задаче",
                    $exception->status,
                    $exception->param
                );
            }
        }
    }

    public function checkItem(array $task): void
    {

        $validator = Validator::make($task, [
            "description" => "required|string",
            "rating" => "required|int",
            "variants" => "required|array",
        ]);

        if ($validator->fails()) {
            $errors = array_map(function(array $message) {
                return (string) $message[0];
            }, $validator->errors()->toArray());

            throw new HandleException(
                "Пропущенны значения",
                ResponseAlias::HTTP_BAD_REQUEST,
                array_values($errors)
            );
        }
    }
}
