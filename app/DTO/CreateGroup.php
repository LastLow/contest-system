<?php

namespace App\DTO;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="CreateGroupDTO",
 *     description="CreateGroup schema",
 *     @OA\Xml(
 *         name="CreateGroupDTO"
 *     )
 * )
 * @OA\Property(property="name", type="string"),
 * @OA\Property(property="count_generate", type="number")
 */
class CreateGroup extends BaseSchema
{
    public string $name;
    public int $count_generate;

    public function __construct(string $name, ?int $count_generate = 0)
    {
        $this->name = $name;
        $this->count_generate = $count_generate;
    }
}
