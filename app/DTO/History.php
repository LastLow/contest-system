<?php

namespace App\DTO;

use Illuminate\Support\Collection;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="HistoryDTO",
 *     description="History schema",
 *     required={"id", "code", "status", "score"},
 *     @OA\Xml(
 *         name="HistoryDTO"
 *     )
 * )
 *    @OA\Property(property="id", type="number"),
 *    @OA\Property(property="code", type="string"),
 *    @OA\Property(property="status", type="string", enum={"success", "error", "zero"}),
 *    @OA\Property(property="score", type="number"),
 *    @OA\Property(property="tests", type="array", @OA\Items(ref="#/components/schemas/ResultTest"))
 */

class History extends BaseSchema
{
    public int $id;
    public string $code;
    public string $status;
    public int $score;
    public array $tests;

    public function __construct(\App\Models\History $history, bool $onlyError = true)
    {
        $this->id = $history->id;
        $this->code = $history->code;
        $this->status = $history->status;
        $this->score = $history->score;
        $this->tests = $history->resultTests->reduce(function (array $carry, \App\Models\ResultTest $test) use ($onlyError) {
            if (!$onlyError || $test->status == "error") {
                $carry[] = new ResultTest($test);
            }

            return $carry;
        }, []);
    }
}
