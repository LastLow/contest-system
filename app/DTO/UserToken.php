<?php

namespace App\DTO;

use App\Models\User as UserModel;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="UserDTO",
 *     description="User schema",
 *     @OA\Xml(
 *         name="UserDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="name", type="string"),
 * @OA\Property(property="login", type="string"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string", nullable=true),
 * @OA\Property(property="roles", type="array", nullable=true, @OA\Items(type="string")),
 * @OA\Property(property="permissions", type="array", nullable=true, @OA\Items(type="string")),
 * @OA\Property(property="token", type="string")
 */
class UserToken extends User
{
    public string $token;

    public function __construct(UserModel $user, string $token)
    {
        parent::__construct($user);
        $this->token = $token;
    }
}
