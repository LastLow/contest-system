<?php

namespace App\DTO;

use App\Models\UserCourseTask;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="UserTaskInfoDTO",
 *     description="UserTaskInfo model",
 *     @OA\Xml(
 *         name="UserTaskInfoDTO"
 *     )
 * )
 *  @OA\Property(property="id", type="number"),
 *  @OA\Property(property="description", type="string"),
 *  @OA\Property(property="rating", type="number"),
 *  @OA\Property(property="variant", type="number"),
 *  @OA\Property(property="status", type="string", enum={"success", "error", "zero"}),
 *  @OA\Property(property="score", type="number"),
 *  @OA\Property(property="max_score", type="number")
 */

class UserTaskInfo extends BaseSchema
{
    public int $id;
    public string $description;
    public int $rating;
    public int $variant;
    public string $status;
    public int $score;
    public int $max_score;

    public function __construct(UserCourseTask $userCourseTask)
    {
        $this->id = $userCourseTask->id;
        $this->description = $userCourseTask->task->description;
        $this->rating = $userCourseTask->task->rating;
        $this->variant = $userCourseTask->variant->number;
        $this->score = $userCourseTask->history()->max('score') ?? 0;
        $this->max_score = $userCourseTask->variant->getMaxScore();
        $this->status = $userCourseTask->history()->latest()->first()->status ?? 'zero';
    }
}
