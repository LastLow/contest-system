<?php

namespace App\DTO;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="SetCourseRatingDTO",
 *     description="SetCourseRating schema",
 *     @OA\Xml(
 *         name="SetCourseRatingDTO"
 *     )
 * )
 * @OA\Property(property="rating", type="number"),
 * @OA\Property(property="count", type="number")
 */

class SetCourseRating
{
    public int $rating;
    public int $count;

    public function __construct(int $rating, int $count)
    {
        $this->count = $count;
        $this->rating = $rating;
    }
}
