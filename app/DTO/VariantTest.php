<?php

namespace App\DTO;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="TestDTO",
 *     description="Test schema",
 *     @OA\Xml(
 *         name="TestDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="in", type="string", nullable=true),
 * @OA\Property(property="out", type="string", nullable=true),
 * @OA\Property(property="score", type="number"),
 * @OA\Property(property="time", type="number")
 */

class VariantTest extends BaseSchema
{
    public int|null $id;
    public string|null $in;
    public string|null $out;
    public int $score;
    public int $time;

    public function __construct(int|null $id, string|null $in, string|null $out, int $score, int $time)
    {
        $this->id = $id;
        $this->in = $in;
        $this->out = $out;
        $this->score = $score;
        $this->time = $time;
    }
}
