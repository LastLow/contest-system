<?php

namespace App\DTO;

use App\Models\UserCourse as UserCourseModel;
use App\Models\UserCourseTask;
use OpenApi\Annotations as OA;


/**
 * @OA\Schema(
 *     title="UserCourseDTO",
 *     description="Course model",
 *     @OA\Xml(
 *         name="UserCourseDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="name", type="string"),
 * @OA\Property(property="count", type="number"),
 * @OA\Property(property="success_count", type="number")
 *
 */
class UserCourse extends BaseSchema
{
    public int $id;
    public string $name;
    public int $count;
    public int $success_count;
    public function __construct(UserCourseModel $userCourse)
    {
        $this->id = $userCourse->id;
        $this->name = $userCourse->course->name;
        $this->count = $userCourse->userTasks()->count();
        $this->success_count = $userCourse->userTasks->reduce(function(int $carry, UserCourseTask $task) {
            if ($task->history()->where('status', '=', 'success')->first()) {
                $carry++;
            }

            return $carry;
        }, 0);
    }
}
