<?php

namespace App\DTO;

use App\Models\UserCourseTask;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="CourseUserInfoDTO",
 *     description="CourseUserInfo schema",
 *     @OA\Xml(
 *         name="CourseUserInfoDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="name", type="string"),
 * @OA\Property(property="login", type="string")
 * @OA\Property(property="success_count_tasks", type="number"),
 * @OA\Property(property="count_tasks", type="number"),
 * @OA\Property(property="adding_score_tasks", type="number"),
 * @OA\Property(property="score_tasks", type="number"),
 */
class CourseUserInfo extends UserInfo
{
    public int $success_count_tasks = 0;
    public int $count_tasks = 0;
    public int $adding_score_tasks = 0;
    public int $score_tasks = 0;

    public function __construct(\App\Models\User $user, \App\Models\Course $course)
    {
        parent::__construct($user);

        $userTasks = $user->userTasksByCourse($course);

        $this->count_tasks = $userTasks->count();
        $userTasks->each(function(UserCourseTask $task) {
            $this->adding_score_tasks += $task->history()->max('score');
            if ($task->history()->latest()->first() && $task->history()->latest()->first()->status == "success")
                $this->success_count_tasks++;
            $this->score_tasks += $task->variant->getMaxScore();
        });
    }
}
