<?php

namespace App\DTO;

use App\DTO\Collections\UserInfoCollection;
use App\Models\Task;
use Illuminate\Database\Eloquent\Collection;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="GroupTaskInfoDTO",
 *     description="GroupTaskInfo schema",
 *     @OA\Xml(
 *         name="GroupTaskInfoDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="description", type="string"),
 * @OA\Property(property="rating", type="number")
 * @OA\Property(property="variant_count", type="number")
 * @OA\Property(property="history", type="array", @OA\Items(ref="#/components/schemas/UserInfo"))
 */
class GroupTaskInfo extends TaskInfo
{
    public array $users;

    public function __construct(Task $task, Collection $users)
    {
        parent::__construct($task);
        $this->users = (new UserInfoCollection($users))->getData();
    }
}
