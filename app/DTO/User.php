<?php

namespace App\DTO;

use App\Models\User as UserModel;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="UserDTO",
 *     description="User schema",
 *     @OA\Xml(
 *         name="UserDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="name", type="string"),
 * @OA\Property(property="login", type="string"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string", nullable=true),
 * @OA\Property(property="roles", type="array", nullable=true, @OA\Items(type="string")),
 * @OA\Property(property="permissions", type="array", nullable=true, @OA\Items(type="string"))
 */

class User extends BaseSchema
{
    public int $id;
    public string $name;
    public string $login;
    public string $created_at;
    public string|null $updated_at;
    public bool|null $ban;
    public bool|null $non_active;
    public array|null $roles;
    public array|null $permissions;

    private UserModel $user;

    public function __construct(UserModel $user) {
        $this->user = $user;

        $this->id = $user->id;
        $this->name = $user->name;
        $this->login = $user->login;
        $this->created_at = $user->created_at;
        $this->updated_at = $user->updated_at;
        $this->roles = $user->roles->pluck('name')->toArray();
        $this->permissions = $user->getAllPermissionsInUser()->pluck('name')->toArray();
    }

    public function addInfoAboutBanNActive(): static
    {
        $this->ban = $this->user->checkBan();
        $this->non_active = $this->user->checkNonActive();

        return $this;
    }
}
