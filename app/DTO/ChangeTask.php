<?php

namespace App\DTO;

use App\DTO\Collections\VariantCollection;
use App\Exceptions\HandleException;
use Illuminate\Support\Facades\Validator;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ChangeTask extends BaseSchema
{
    public int $id;
    public string $description;
    public int $rating;
    public array $variants;

    public function __construct(array $data)
    {

        $this->checkItem($data);

        $this->id = $data['id'];
        $this->description = $data['description'];
        $this->rating = $data['rating'];

        $this->variants = (new VariantCollection($data['variants']))->getData();
    }

    public function checkItem(array $task): void
    {

        $validator = Validator::make($task, [
            "id" => "required|int",
            "description" => "required|string",
            "rating" => "required|int",
            "variants" => "required|array",
        ]);

        if ($validator->fails()) {
            $errors = array_map(function(array $message) {
                return (string) $message[0];
            }, $validator->errors()->toArray());

            throw new HandleException(
                "Пропущенны значения",
                ResponseAlias::HTTP_BAD_REQUEST,
                array_values($errors)
            );
        }
    }
}
