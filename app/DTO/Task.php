<?php

namespace App\DTO;

use App\DTO\Collections\VariantCollection;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="TaskDTO",
 *     description="Task schema",
 *     @OA\Xml(
 *         name="TaskDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="description", type="string"),
 * @OA\Property(property="rating", type="number"),
 * @OA\Property(property="variants", type="array", @OA\Items(ref="#/components/schemas/Variant"))
 */

class Task extends BaseSchema
{
    public int $id;
    public string $description;
    public int $rating;
    public array $variants;
    public function __construct(\App\Models\Task $task)
    {
        $task->variants->each(function(\App\Models\Variant $variant) {
            $variant->tests;
        });

        $this->id = $task->id;
        $this->description = $task->description;
        $this->rating = $task->rating;

        $this->variants = (new VariantCollection($task->variants->toArray()))->getData();
    }
}
