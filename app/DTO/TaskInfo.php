<?php

namespace App\DTO;

use App\Models\Task;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="TaskInfoDTO",
 *     description="TaskInfo schema",
 *     @OA\Xml(
 *         name="TaskInfoDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="description", type="string"),
 * @OA\Property(property="rating", type="number")
 * @OA\Property(property="variant_count", type="number")
 */
class TaskInfo extends BaseSchema
{
    public int $id;
    public string $description;
    public int $rating;
    public int $variant_count;

    public function __construct(Task $task)
    {
        $this->id = $task->id;
        $this->description = $task->description;
        $this->rating = $task->rating;
        $this->variant_count = $task->variants->count();
    }
}
