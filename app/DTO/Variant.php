<?php

namespace App\DTO;

use App\DTO\Collections\VariantTestCollection;
use OpenApi\Annotations as OA;


/**
 * @OA\Schema(
 *     title="VariantDTO",
 *     description="Variant schema",
 *     @OA\Xml(
 *         name="VariantDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="number", type="string"),
 * @OA\Property(property="tests", type="array", @OA\Items(ref="#/components/schemas/VariantTest"))
 */

class Variant extends BaseSchema
{
    public int|null $id;

    public int $number;
    /**
     * @var VariantTest[]
     */
    public array $tests;

    public function __construct(int|null $id, int $number, VariantTestCollection $tests)
    {
        $this->id = $id;
        $this->number = $number;
        $this->tests = $tests->getData();
    }
}
