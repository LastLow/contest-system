<?php

namespace App\DTO;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="GroupDTO",
 *     description="Group schema",
 *     @OA\Xml(
 *         name="GroupDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="name", type="string")
 * @OA\Property(property="count_users", type="number")
 */

class Group extends BaseSchema
{
    public int $id;
    public string $name;
    public int $count_users;

    public function __construct(\App\Models\Group $group)
    {
        $this->id = $group->id;
        $this->name = $group->name;
        $this->count_users = $group->users()->count();
    }
}
