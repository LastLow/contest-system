<?php

namespace App\DTO\Collections;

use App\DTO\CreateCourseTasks;
use App\Exceptions\HandleException;

class CreateCourseTasksCollection extends BaseCollection
{
    /**
     * @var CreateCourseTasks[]
     */
    protected array $items = [];

    /**
     * @param array $data
     * @throws HandleException
     */
    public function __construct(array $data)
    {
        if (empty($data)) {
            throw new HandleException("Пустой массив не принимается");
        }

        foreach ($data as $key => $item) {
            $this->checkData($item, $key+1);
            $this->items[] = new CreateCourseTasks($item['course_name'], $item['tasks']);
        }
    }

    public function checkData(array $data, $key): void
    {
        if (empty($data['course_name'])) {
            throw new HandleException("Нет названия курса в $key элементе");
        }

        if (empty($data['tasks'])) {
            throw new HandleException("Нет задач в $key элементе");
        }
    }
}
