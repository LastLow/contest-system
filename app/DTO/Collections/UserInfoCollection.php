<?php

namespace App\DTO\Collections;

use App\DTO\UserInfo;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class UserInfoCollection extends BaseCollection
{
    public function __construct(Collection $users)
    {
        $this->items = $users->map(fn (User $user) => new UserInfo($user))->toArray();
    }
}
