<?php

namespace App\DTO\Collections;

use App\Models\Group;
use Illuminate\Database\Eloquent\Collection;

class GroupCollection extends BaseCollection
{
    public function __construct(Collection $groups)
    {
        $this->items = $groups->map(fn(Group $group) => new \App\DTO\Group($group))->toArray();
    }
}
