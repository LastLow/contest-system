<?php

namespace App\DTO\Collections;

use App\DTO\Variant;
use App\Exceptions\HandleException;

class VariantCollection extends BaseCollection
{
    public function __construct(array $variants)
    {
        $variantsNumber = [];
        $idNumbers = [];

        if (empty($variants)) {
            throw new HandleException("Задача не может быть без вариантов", 400);
        }

        foreach ($variants as $index => $variant) {
            try {
                if (empty($variant['number'])) {
                    throw new HandleException("Нет номера варианта");
                }

                if (array_search($variant['number'], $variantsNumber) !== false) {
                    throw new HandleException("Номер у варианта должен быть индивидуальным");
                }

                if (isset($variant['id']) && array_search($variant['id'], $idNumbers) !== false) {
                    throw new HandleException("Id варианта должен быть индивидуальным");
                }

                $variantsNumber[] = $variant['number'];

                if (isset($variant['id'])) $idNumbers[] = $variant['id'];

                $this->items[] = new Variant(
                    id: $variant['id'] ?? null,
                    number: $variant['number'],
                    tests:  new VariantTestCollection($variant['tests'] ?? array())
                );
            } catch (HandleException $exception) {
                $idx = $index + 1;
                throw new HandleException(
                    $exception->getMessage() . ", в {$idx} варианте",
                    $exception->status,
                    $exception->param
                );
            }
        }
    }
}
