<?php

namespace App\DTO\Collections;

use App\DTO\SetCourseRating;
use App\Exceptions\HandleException;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class SetCourseCollection extends BaseCollection
{
    public function __construct(array $data)
    {
        if (empty($data)) {
            throw new HandleException("Пустые данные по рейтингам");
        }

        foreach ($data as $key => $item) {
            $this->checkData($item, $key+1);

            $this->items[] = new SetCourseRating($item['rating'], $item['count']);
        }
    }

    private function checkData(array $item, int $key)
    {
        $validator = Validator::make(
            $item,
            [
                "rating" => "required|numeric",
                "count" => "required|numeric"
            ]
        );

        if ($validator->fails()) {
            $errors = array_map(function(array $message) {
                return (string) $message[0];
            }, $validator->errors()->toArray());

            throw new HandleException(
                "Неправильно заполнены данные в $key элементе",
                ResponseAlias::HTTP_BAD_REQUEST,
                array_values($errors)
            );
        }
    }
}
