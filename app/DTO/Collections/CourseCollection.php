<?php

namespace App\DTO\Collections;

use App\Models\Course;
use Illuminate\Database\Eloquent\Collection;

class CourseCollection extends BaseCollection
{
    public function __construct(Collection $courses)
    {
        $this->items = $courses->map(fn (Course $course) => new \App\DTO\Course($course))->toArray();
    }
}
