<?php

namespace App\DTO\Collections;

use App\DTO\UserTaskInfo;
use App\Models\UserCourseTask;
use Illuminate\Database\Eloquent\Collection;

class UserTaskInfoCollection extends BaseCollection
{
    public function __construct(Collection $userCourseTasks)
    {
        $this->items = $userCourseTasks->map(function(UserCourseTask $userCourseTask) {
            return new UserTaskInfo($userCourseTask);
        })->toArray();
    }
}
