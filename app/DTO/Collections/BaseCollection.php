<?php

namespace App\DTO\Collections;

class BaseCollection
{
    protected array $items = [];

    public function getData(): array
    {
        return $this->items;
    }
}
