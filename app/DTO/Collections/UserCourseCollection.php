<?php

namespace App\DTO\Collections;

use App\DTO\UserCourse;
use App\Models\UserCourse as UserCourseModel;
use Illuminate\Database\Eloquent\Collection;

class UserCourseCollection extends BaseCollection
{
    /**
     * @param Collection $userCourses
     */
    public function __construct(Collection $userCourses)
    {
        $this->items = $userCourses->map(function(UserCourseModel $userCourse) {
            return new UserCourse($userCourse);
        })->toArray();
    }

}
