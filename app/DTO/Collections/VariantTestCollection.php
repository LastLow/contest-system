<?php

namespace App\DTO\Collections;

use App\DTO\VariantTest;
use App\Exceptions\HandleException;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class VariantTestCollection extends BaseCollection
{

    public function __construct(array $tests)
    {
        if (empty($tests)) {
            throw new HandleException("Вариант не может быть без тестов", 400);
        }

        $this->items = array_map(function(array $test) {
            $this->checkData($test);

            return new VariantTest(
                id:    $test['id'] ?? null,
                in:    $test['in'],
                out:   $test['out'],
                score: $test['score'],
                time:  $test['time']
            );
        }, $tests);
    }

    public function checkData(array $item): void
    {
        $validator = Validator::make(
            $item,
            [
                "in" => "nullable|string",
                "out" => "nullable|string",
                "score" => "required|numeric",
                "time" => "required|numeric"
            ]
        );

        if ($validator->fails()) {
            $errors = array_map(function(array $message) {
                return (string) $message[0];
            }, $validator->errors()->toArray());

            throw new HandleException(
                "Неправильно заполнены данные в тесте",
                ResponseAlias::HTTP_BAD_REQUEST,
                array_values($errors)
            );
        }
    }
}
