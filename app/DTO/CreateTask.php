<?php

namespace App\DTO;

use App\DTO\Collections\VariantCollection;

class CreateTask
{
    public string $description;
    public int $rating;
    /**
     * @var Variant[]
     */
    public array $variants;

    public function __construct(string $description, int $rating, array $variants)
    {
        $this->description = $description;
        $this->rating = $rating;
        $this->variants = (new VariantCollection($variants))->getData();
    }
}
