<?php

namespace App\DTO;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="CheckDTO",
 *     description="Check schema",
 *     @OA\Xml(
 *         name="CheckDTO"
 *     )
 * )
 * @OA\Property(property="task_id", type="number"),
 * @OA\Property(property="code", type="string"),
 * @OA\Property(property="language", type="string", nullable=true)
 */

class CheckCode extends BaseSchema
{
    public int $user_course_task_id;
    public string $code;
    public string $language;

    public function __construct(int $task_id, string $code, ?string $language = null)
    {
        $this->user_course_task_id = $task_id;
        $this->code = $code;
        $this->language = $language ?? "python";
    }
}
