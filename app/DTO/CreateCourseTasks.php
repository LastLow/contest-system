<?php

namespace App\DTO;

use App\Exceptions\HandleException;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class CreateCourseTasks
{
    public string $course_name;

    /**
     * @var CreateTask[]
     */
    public array $tasks = [];

    public function __construct(string $course_name, array $tasks)
    {
        if (empty($tasks))
            throw new HandleException("Нет задач", Response::HTTP_BAD_REQUEST);

        $this->course_name = $course_name;

        $this->tasks = (new CreateTasks($tasks))->getData();
    }
}
