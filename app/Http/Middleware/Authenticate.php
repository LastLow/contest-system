<?php

namespace App\Http\Middleware;

use App\Exceptions\HandleException;
use App\Http\Controllers\AuthController;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param Request $request
     * @throws HandleException
     */
    protected function redirectTo(Request $request)
    {
        if (!$request->expectsJson()) {
            throw new HandleException("Нет аутентификации", 401);
        }
    }

    public function handle($request, Closure $next, ...$guards) {
        if($jwt = $request->cookie('jwt')) {
            $request->headers->set('Authorization', 'Bearer ' . $jwt);
        }

        $this->authenticate($request, $guards);

        return $next($request);
    }
}
