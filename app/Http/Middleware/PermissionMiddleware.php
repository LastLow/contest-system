<?php

namespace App\Http\Middleware;

use App\Exceptions\HandleException;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PermissionMiddleware
{
    public function handle(Request $request, Closure $next, $permissionName)
    {
        if (!Auth::user()->hasPermission($permissionName)) {
            throw new HandleException("Нет прав", 403);
        }

        return $next($request);
    }
}
