<?php

namespace App\Http\Controllers;

use App\DTO\CheckCode;
use App\DTO\UserTask;
use App\Exceptions\HandleException;
use App\Models\History;
use App\Models\UserCourseTask;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Auth;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;

class TaskController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/tasks/{task_id}",
     *     summary="Получение задачи",
     *     description="Возвращает задачу",
     *     operationId="getStudentTask",
     *     tags={"Tasks"},
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="task_id",
     *         in="path",
     *         description="Id задачи",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/UserTask")
     *     )
     * )
     */

    public function get(UserCourseTask $userCourseTask): array
    {
        if ($userCourseTask->userCourse->user->id !== Auth::user()->getAuthIdentifier()) {
            throw new HandleException($userCourseTask::getBadRequestErrorMessage(), Response::HTTP_NOT_FOUND);
        }

        return (new UserTask($userCourseTask))->toArray();
    }


    /**
     * @OA\Post(
     *     path="/api/check",
     *     summary="Закинуть задачу на проверку",
     *     tags={"Check"},
     *     operationId="setCheckTask",
     *     security={{"sanctum":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/CheckCode")
     *         )
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description=""
     *     )
     * )
     */

    public function check(CheckCode $checkCode): Application|\Illuminate\Http\Response|\Illuminate\Contracts\Foundation\Application|ResponseFactory
    {
        $createParams = array_merge(
            ["max_score" => UserCourseTask::whereId($checkCode->user_course_task_id)->first()->variant->getMaxScore()],
            $checkCode->toArray()
        );

        History::create($createParams);

        return response('', 204);
    }
}
