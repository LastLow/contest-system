<?php

namespace App\Http\Controllers;

use App\DTO\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use OpenApi\Annotations as OA;

class UserController extends Controller
{

    /**
     * @OA\Get(
     *     path="/api/user",
     *     summary="Получение информации о пользователе",
     *     tags={"User"},
     *     operationId="getInfoUser",
     *     security={{"sanctum":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Успешный ответ",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     ),
     *      @OA\Response(
     *          response="401",
     *          description="Не авторизирован"
     *      )
     * )
     */

    public function get(): array
    {
        return (new User(\App\Models\User::whereId(Auth::user()->id)->first()))->toArray();
    }
}
