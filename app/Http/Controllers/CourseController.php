<?php

namespace App\Http\Controllers;

use App\DTO\Collections\UserCourseCollection;
use App\DTO\Collections\UserTaskInfoCollection;
use App\Exceptions\HandleException;
use App\Models\UserCourse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use OpenApi\Annotations as OA;

class CourseController extends Controller
{

    /**
     * @OA\Get(
     *     path="/api/courses",
     *     summary="Получение курсов",
     *     description="Возвращает список курсов",
     *     tags={"Courses"},
     *     operationId="getStudentCourses",
     *     security={{"sanctum":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *          @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/UserCourse"
     *            )
     *         )
     *     )
     * )
     */
    public function get()
    {
        return (new UserCourseCollection(Auth::user()->userCourses))->getData();
    }

    /**
     * @OA\Get(
     *     path="/api/courses/{course_id}/tasks",
     *     summary="Получение задач",
     *     description="Возвращает список задач",
     *     tags={"Courses"},
     *     operationId="getStudentTasks",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/UserTaskInfo"
     *            )
     *         )
     *     )
     * )
     */
    public function getTasks(UserCourse $userCourse)
    {
        if ($userCourse->user_id !== Auth::user()->getAuthIdentifier()) {
            throw new HandleException($userCourse::getBadRequestErrorMessage(), Response::HTTP_NOT_FOUND);
        }

        return (new UserTaskInfoCollection($userCourse->userTasks))->getData();
    }
}
