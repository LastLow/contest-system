<?php

namespace App\Http\Controllers;

use App\DTO\Login;
use App\DTO\UserToken;
use App\Exceptions\HandleException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{

    /**
     * Функция регистрации
     *
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request): mixed
    {
        // TODO: Изменить регистрацию

        return User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password'))
        ]);
    }

    /**
     * Функция авторизации
     *
     */

    /**
     * @OA\Post(
     *     path="/api/login",
     *     summary="Авторизация пользователя",
     *     tags={"Auth"},
     *     operationId="login",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Login")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успешный ответ",
     *         @OA\JsonContent(ref="#/components/schemas/UserToken")
     *     )
     * )
     */

    public function login(Request $request, Login $login): \Illuminate\Http\Response|Application|ResponseFactory
    {
        if (!Auth::attempt($login->toArray()))
            throw new HandleException('Неправильные данные', Response::HTTP_UNAUTHORIZED);

        $token = Auth::user()->createToken('token')->plainTextToken;

        return response(
            (new UserToken(User::whereId(Auth::user()->getAuthIdentifier())->first(), $token))->toArray()
        );
    }

    /**
     * Выход из авторизации
     *
     * @return Application|ResponseFactory|\Illuminate\Http\Response
     */

    /**
     * @OA\Post(
     *     path="/api/logout",
     *     summary="Разлогирование пользователя",
     *     tags={"Auth"},
     *     operationId="logout",
     *     security={{"sanctum":{}}},
     *     @OA\Response(
     *         response=204,
     *          description=""
     *     )
     * )
     */

    public function logout() {
        $cookie = Cookie::forget('jwt');

        return response('', 204)->withCookie($cookie);
    }
}
