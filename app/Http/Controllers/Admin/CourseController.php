<?php

namespace App\Http\Controllers\Admin;

use App\DTO\Collections\CourseCollection;
use App\DTO\Collections\CreateCourseTasksCollection;
use App\DTO\Collections\GroupCollection;
use App\DTO\Collections\UserInfoCollection;
use App\DTO\Collections\UserTaskInfoCollection;
use App\DTO\CourseUserInfo;
use App\DTO\GroupTaskInfo;
use App\DTO\TaskInfo;
use App\DTO\UserTaskInfo;
use App\Exceptions\HandleException;
use App\Http\Controllers\Admin\Interfaces\ICourseController;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Group;
use App\Models\GroupCourse;
use App\Models\Task;
use App\Models\Test;
use App\Models\User;
use App\Models\UserCourseTask;
use App\Models\Variant;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller implements ICourseController
{
    public function getAll(): array
    {
        return (new CourseCollection(Course::all()))->getData();
    }

    public function create(CreateCourseTasksCollection $collection): JsonResponse
    {
        try {
            DB::beginTransaction();

            foreach ($collection->getData() as $courseTask) {
                $course = Course::create(['name' => $courseTask->course_name]);

                foreach ($courseTask->tasks as $taskItem) {
                    $task = Task::create([
                                     "description" => $taskItem->description,
                                     "rating"      => $taskItem->rating,
                                     "course_id"   => $course->id
                                 ]);

                    foreach ($taskItem->variants as $variantItem) {
                        $variant = Variant::create([
                                                       "number" => $variantItem->number,
                                                       "task_id" => $task->id
                                                   ]);

                        foreach ($variantItem->tests as $testItem) {
                            Test::create(array_merge(
                                ["variant_id" => $variant->id],
                                $testItem->toArray()
                                         ));
                        }
                    }
                }
            }

            DB::commit();

            return response()->json(["message" => "Создание прошло успешно"]);
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function createByShitFormat(Request $request): JsonResponse
    {
        if (!$request->has('tasks') || !$request->has('course_name')) {
            throw new HandleException("Нет tasks или course_name", 400);
        }

        $validator = Validator::make($request->input('tasks'), [
            '*.rating' => 'required|integer',
            '*.task' => 'required|string',
            '*.variants.*.*' => 'array',
            '*.variants.*.*.in' => 'nullable|string',
            '*.variants.*.*.out' => 'nullable|string',
            '*.variants.*.*.score' => 'required|integer',
            '*.variants.*.*.time' => 'nullable|integer'
        ]);

        if ($validator->fails()) {
            $errors = array_map(function(array $message) {
                return (string) $message[0];
            }, $validator->errors()->toArray());

            throw new HandleException("Пропуск полей в задачах", 400, array_values($errors));
        }

        try {
            DB::beginTransaction();

            $course = Course::create(['name' => $request->input('course_name')]);

            foreach ($request->input('tasks') as $task) {
                $createdTask = Task::create([
                    "description" => $task['task'],
                    "rating" => $task['rating'],
                    "course_id" => $course->id
                             ]);

                foreach ($task['variants'] as $number => $tests) {
                    $variant = Variant::create([
                        "number" => $number,
                        "task_id" => $createdTask->id
                                               ]);

                    foreach ($tests as $key => $test) {
                        Test::create([
                            'in' => $test['in'],
                            'out' => $test['out'],
                            'score' => $test['score'],
                            'time' => $test['time'] ?? 40,
                            'variant_id' => $variant->id
                        ]);
                    }
                }
            }

            DB::commit();

                return response()->json([
                    "message" => "Успешно создан курс {$request->input('course_name')}"
                                        ], 201);
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function getTasksByCourse(Course $course): array
    {
        return $course->tasks->map(fn (Task $task) => new TaskInfo($task))->toArray();
    }


    public function getGroupsByCourse(Course $course): array
    {
        return (new GroupCollection($course->groups))->getData();
    }


    public function getUsersByCourse(Course $course): array
    {
        $users = $course->users->filter(function(User $user) use ($course) {
            $userGroup = $user->groups->first();

            return !$userGroup || !$userGroup->courses->contains($course);
        })->values();

        return (new UserInfoCollection($users))->getData();
    }

    public function getGroupTasksByCourse(Course $course, Group $group): array
    {
        if (!$group->courses()->where('id', $course->id)->first()) {
            throw new HandleException("Группа '$group->name' не имеет курса '$course->name'", 400);
        }

        return $course->tasks->reduce(function(Collection $carry, Task $task) use ($group) {
            $users = $task->userTasks
                ->map(fn (UserCourseTask $userTask) => $userTask->userCourse->user)
                ->intersect($group->users);

            if ($users->count() > 0) {
                $carry->push(new GroupTaskInfo($task, $users));
            }

            return $carry;
        }, collect())->toArray();
    }

    public function getGroupsOutCourse(Course $course): array
    {
        $groupsOutCourse = Group::all()->diff($course->groups);
        return (new GroupCollection($groupsOutCourse))->getData();
    }

    public function getUsersOutCourse(Course $course): array
    {
        $users = User::all()->filter(function(User $user) use ($course) {
            $userGroup = $user->groups->first();
            $isUserCourse = $user->userCourses()
                ->where('course_id', $course->id)
                ->first();

            return (!$userGroup || !$userGroup->courses->contains($course)) && !$isUserCourse;
        })->values();

        return (new UserInfoCollection($users))->getData();
    }
}
