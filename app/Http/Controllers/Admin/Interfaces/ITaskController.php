<?php

namespace App\Http\Controllers\Admin\Interfaces;

use App\DTO\ChangeTask;
use App\Models\Course;
use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

interface ITaskController
{

    /**
     * @OA\Get(
     *     path="/api/admin/tasks/{task_id}",
     *     summary="Получение задачи",
     *     description="Получение задачу",
     *     tags={"AdminTasks"},
     *     operationId="getTask",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="task_id",
     *         in="path",
     *         description="Id задачи",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="JSON файл",
     *         @OA\MediaType(
     *             mediaType="text/json",
     *             @OA\Schema(
     *                  type="object",
     *                  ref="#/components/schemas/Task"
     *              )
     *         )
     *     )
     *   )
     * )
     */

    public function getTask(Task $task);

    /**
     * @OA\Post(
     *     path="/api/admin/tasks/courses/{course_id}/format",
     *     tags={"AdminTasks"},
     *     summary="Создание задач",
     *     security={{"sanctum":{}}},
     *     operationId="createTasksFromShitFormat",
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     required={"task", "rating", "variants"},
     *                     @OA\Property(
     *                         property="task",
     *                         type="string",
     *                         example="Выстроить json"
     *                     ),
     *                     @OA\Property(
     *                         property="rating",
     *                         type="integer",
     *                         example=1
     *                     ),
     *                     @OA\Property(
     *                         property="variants",
     *                         type="object",
     *                         example={"1":{"1":{"in":"1223445799","out":"4","score":10,"time":30}}},
     *                         @OA\AdditionalProperties(
     *                             type="object",
     *                             required={"score"},
     *                             @OA\Property(
     *                                 property="in",
     *                                 type="string",
     *                                 nullable=true
     *                             ),
     *                             @OA\Property(
     *                                 property="out",
     *                                 type="string",
     *                                 nullable=true
     *                             ),
     *                             @OA\Property(
     *                                 property="score",
     *                                 type="integer"
     *                             ),
     *                              @OA\Property(
     *                                 property="time",
     *                                 type="integer",
     *                                 nullable=true
     *                             ),
     *                             example={"1": {"in": "example_in", "out": "example_out", "score": 1}}
     *                         )
     *                     )
     *                 )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *     )
     * )
     */

    public function createTasksFromShitFormat(Request $request, Course $course): JsonResponse;
    /**
     * @OA\Put(
     *     path="/api/admin/tasks/{task_id}",
     *     summary="Изменение задачи",
     *     description="Изменяет задачу",
     *     tags={"AdminTasks"},
     *     security={{"sanctum":{}}},
     *     operationId="updateTask",
     *     @OA\Parameter(
     *         name="task_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(ref="#/components/schemas/Task")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="object",
     *             ref="#/components/schemas/Task"
     *         )
     *     )
     * )
     */

    public function updateTask(Task $task, ChangeTask $changeTask): array;

    /**
     * @OA\Delete(
     *     path="/api/admin/tasks/{task_id}",
     *     summary="Удаление задачи",
     *     description="Удаляет задачу",
     *     tags={"AdminTasks"},
     *     operationId="deleteTask",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="task_id",
     *         in="path",
     *         description="Id задачи",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *     )
     * )
     */
    public function deleteTask(Task $task): void;
}
