<?php

namespace App\Http\Controllers\Admin\Interfaces;

use App\DTO\Collections\SetCourseCollection;
use App\DTO\CreateGroup;
use App\Models\Course;
use App\Models\Group;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\StreamedResponse;

interface IGroupController
{
    /**
     * @OA\Get(
     *     path="/api/admin/groups",
     *     summary="Получение всех групп",
     *     description="Возвращает список всех групп",
     *     tags={"AdminGroups"},
     *     operationId="getAll",
     *     security={{"sanctum":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/Group"
     *            )
     *         )
     *     )
     * )
     */
    public function getAll(): array;

    /**
     * @OA\Get(
     *     path="/api/admin/groups/{group_id}/users",
     *     summary="Получение пользователей в группе",
     *     description="Возвращает список пользователей в группе",
     *     tags={"AdminGroups"},
     *     operationId="getUsers",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="group_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/UserInfo"
     *            )
     *         )
     *     )
     * )
     */
    public function getUsers(Group $group): array;

    /**
     * @OA\Get(
     *     path="/api/admin/groups/{group_id}/courses/{course_id}/users",
     *     summary="Получение пользователей, которые имею курс и находятся в группе",
     *     description="Возвращает список пользователей, которые имею курс и находятся в группе",
     *     tags={"AdminGroups"},
     *     operationId="getGroupUsersByCourse",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="group_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/CourseUserInfo"
     *            )
     *         )
     *     )
     * )
     */
    public function getGroupUsersByCourse(Group $group, Course $course): array;

    /**
     * @OA\Post(
     *     path="/api/admin/groups",
     *     summary="Создание группы",
     *     description="Создание группы",
     *     tags={"AdminGroups"},
     *     operationId="createGroup",
     *     security={{"sanctum":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/CreateGroup")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="object",
     *             ref="#/components/schemas/Group"
     *         )
     *     )
     * )
     */
    public function createGroup(CreateGroup $createGroup): array;


    /**
     * @OA\Delete(
     *     path="/api/admin/groups/{group_id}/courses/{course_id}",
     *     summary="Удаление курса у группы",
     *     description="Удаляет курс у группы",
     *     tags={"AdminGroups"},
     *     operationId="deleteGroupFromCourse",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="group_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *     )
     * )
     */
    public function deleteGroupFromCourse(Group $group, Course $course): void;

    /**
     * @OA\Get(
     *     path="/api/admin/groups/{group_id}/exportUsers",
     *     summary="Получение пользователей группы в CSV",
     *     description="Получение пользователей группы в CSV",
     *     tags={"AdminGroups"},
     *     operationId="exportUsers",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="group_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="CSV файл",
     *         @OA\MediaType(
     *             mediaType="text/csv",
     *             @OA\Schema(
     *                 type="string",
     *                 format="binary"
     *             )
     *         )
     *     )
     * )
     */
    public function exportUsers(Group $group): StreamedResponse;


    /**
     * @OA\Post(
     *     path="/api/admin/groups/{group_id}/courses/{course_id}/random",
     *     summary="Распределение задач курса по группе",
     *     description="Распределение задач курса по группе",
     *     tags={"AdminGroups"},
     *     operationId="setRandomCourseTasksToGroup",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="group_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(ref="#/components/schemas/SetCourseRating")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *      )
     * )
     */
    public function setRandomCourseTasksToGroup(Group $group, Course $course, SetCourseCollection $collection): void;


    /**
     * @OA\Delete(
     *     path="/api/admin/groups/{group_id}",
     *     summary="Удаление курса",
     *     description="Удаляет курс",
     *     operationId="deleteGroup",
     *     tags={"AdminGroups"},
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="group_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *     )
     * )
     */
    public function deleteGroup(Group $group): void;
}
