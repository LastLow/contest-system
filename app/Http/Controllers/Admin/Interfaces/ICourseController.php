<?php

namespace App\Http\Controllers\Admin\Interfaces;

use App\DTO\Collections\CreateCourseTasksCollection;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Group;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

interface ICourseController
{
    public function create(CreateCourseTasksCollection $collection): JsonResponse;

    /**
     * @OA\Post(
     *     path="/api/admin/courses/format",
     *     tags={"AdminCourses"},
     *     summary="Создание курса",
     *     operationId="createCourse",
     *     security={{"sanctum":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="object",
     *             required={"course_name", "tasks"},
     *             @OA\Property(
     *                 property="course_name",
     *                 type="string",
     *                 example="Курс по строению json"
     *             ),
     *             @OA\Property(
     *                 property="tasks",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     required={"task", "rating", "variants"},
     *                     @OA\Property(
     *                         property="task",
     *                         type="string",
     *                         example="Выстроить json"
     *                     ),
     *                     @OA\Property(
     *                         property="rating",
     *                         type="integer",
     *                         example=1
     *                     ),
     *                     @OA\Property(
     *                         property="variants",
     *                         type="object",
     *                         example={"1":{"1":{"in":"1223445799","out":"4","score":10,"time":30}}},
     *                         @OA\AdditionalProperties(
     *                             type="object",
     *                             required={"score"},
     *                             @OA\Property(
     *                                 property="in",
     *                                 type="string",
     *                                 nullable=true
     *                             ),
     *                             @OA\Property(
     *                                 property="out",
     *                                 type="string",
     *                                 nullable=true
     *                             ),
     *                             @OA\Property(
     *                                 property="score",
     *                                 type="integer"
     *                             ),
     *                              @OA\Property(
     *                                 property="time",
     *                                 type="integer",
     *                                 nullable=true
     *                             ),
     *                             example={"1": {"in": "example_in", "out": "example_out", "score": 1}}
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *     )
     * )
     */
    public function createByShitFormat(Request $request): JsonResponse;

    /**
     * @OA\Get(
     *     path="/api/admin/courses",
     *     summary="Получение курсов",
     *     description="Возвращает список курсов",
     *     tags={"AdminCourses"},
     *     operationId="getCourses",
     *     security={{"sanctum":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *          @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/Course"
     *            )
     *         )
     *     )
     * )
     */
    public function getAll(): array;

    /**
     * @OA\Get(
     *     path="/api/admin/courses/{course_id}/tasks",
     *     summary="Получение задач курса",
     *     operationId="getTasksByCourse",
     *     description="Возвращает список задач курса",
     *     tags={"AdminCourses"},
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/TaskInfo"
     *            )
     *         )
     *     )
     * )
     */
    public function getTasksByCourse(Course $course): array;

    /**
     * @OA\Get(
     *     path="/api/admin/courses/{course_id}/groups",
     *     summary="Получение групп курса",
     *     description="Возвращает список групп, которым назаначен курс",
     *     tags={"AdminCourses"},
     *     operationId="getGroupsByCourse",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/Group"
     *            )
     *         )
     *     )
     * )
     */
    public function getGroupsByCourse(Course $course): array;

    /**
     * @OA\Get(
     *     path="/api/admin/courses/{course_id}/users",
     *     summary="Получение пользователей курса",
     *     description="Возвращает список пользователей, которым назаначен курс, вне группы",
     *     tags={"AdminCourses"},
     *     operationId="getUsersByCourse",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/UserInfo"
     *            )
     *         )
     *     )
     * )
     */
    public function getUsersByCourse(Course $course): array;

    /**
     * @OA\Get(
     *     path="/api/admin/courses/{course_id}/groups/{group_id}/tasks",
     *     summary="Получение задач курса группы",
     *     description="Возвращает список задач, которые назаначены пользователям данной группы",
     *     tags={"AdminCourses"},
     *     operationId="getGroupTasksByCourse",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="group_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/GroupTaskInfo"
     *            )
     *         )
     *     )
     * )
     */
    public function getGroupTasksByCourse(Course $course, Group $group): array;

    /**
     * @OA\Get(
     *     path="/api/admin/courses/{course_id}/groups/out",
     *     summary="Получение групп, которые не имею курса",
     *     description="Возвращает список групп, которым не назаначен данный курс",
     *     tags={"AdminCourses"},
     *     operationId="getGroupsOutCourse",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/Group"
     *            )
     *         )
     *     )
     * )
     */
    public function getGroupsOutCourse(Course $course): array;

    /**
     * Получение студентов, которым можно назначить курс
     * @deprecated Пока, что не нужен
     *
     * @param Course $course
     * @return array
     */
    public function getUsersOutCourse(Course $course): array;

}
