<?php

namespace App\Http\Controllers\Admin\Interfaces;

use App\Models\Course;
use App\Models\User;
use App\Models\UserCourseTask;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\StreamedResponse;

interface IUserController
{
    /**
     * @OA\Get(
     *     path="/api/admin/users/{user_id}/courses/{course_id}/tasks",
     *     summary="Получение задач курса пользователя",
     *     description="Возвращает список задач курса пользователя",
     *     tags={"AdminUsers"},
     *     operationId="getUserTasksByCourse",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="Id пользователя",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/UserTaskInfo"
     *            )
     *         )
     *     )
     * )
     */
    public function getTasksByCourse(User $user, Course $course): array;

    /**
     * @OA\Get(
     *     path="/api/admin/users/{user_id}/tasks/{task_id}",
     *     summary="Получение информации о задачи пользователя",
     *     description="Возвращает информацию о задачи пользователя",
     *     operationId="getInfoAboutUserTask",
     *     tags={"AdminUsers"},
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="Id пользователя",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="task_id",
     *         in="path",
     *         description="Id задачи",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/UserTask"
     *            )
     *         )
     *     )
     * )
     */
    public function getInfoAboutUserTask(User $user, UserCourseTask $task): array;

    /**
     * @OA\Get(
     *     path="/api/admin/groups/{group_id}/exportUsers",
     *     summary="Получение пользователей группы в CSV",
     *     description="Получение пользователей группы в CSV",
     *     tags={"AdminGroups"},
     *     operationId="exportUsers",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="group_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="CSV файл",
     *         @OA\MediaType(
     *             mediaType="text/csv",
     *             @OA\Schema(
     *                 type="string",
     *                 format="binary"
     *             )
     *         )
     *     )
     * )
     */
    public function exportUsers(int $groupId): StreamedResponse;
}
