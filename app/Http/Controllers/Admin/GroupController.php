<?php

namespace App\Http\Controllers\Admin;

use App\DTO\Collections\CourseCollection;
use App\DTO\Collections\GroupCollection;
use App\DTO\Collections\SetCourseCollection;
use App\DTO\Collections\UserInfoCollection;
use App\DTO\CourseUserInfo;
use App\DTO\CreateGroup;
use App\DTO\UserTask;
use App\Exceptions\HandleException;
use App\Http\Controllers\Admin\Interfaces\IGroupController;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Group;
use App\Models\GroupCourse;
use App\Models\GroupUser;
use App\Models\Role;
use App\Models\Task;
use App\Models\User;
use App\Models\UserCourse;
use App\Models\UserCourseTask;
use http\Env\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\StreamedResponse;

class GroupController extends Controller implements IGroupController
{
    public function getAll(): array
    {
        return (new GroupCollection(Group::all()))->getData();
    }

    public function getUsers(Group $group): array
    {
        return (new UserInfoCollection($group->users))->getData();
    }

    public function createGroup(CreateGroup $createGroup): array
    {
        if (Group::whereRaw("LOWER(name) = ?", Str::lower($createGroup->name))->first()) {
            throw new HandleException("Группа с таким названием уже существует");
        }

        try {
            DB::beginTransaction();

            $group = Group::create(["name" => $createGroup->name]);

            for ($i = 0; $i < $createGroup->count_generate; $i++) {
                $name = Str::upper(Str::slug($createGroup->name, '_'));
                $lastName = "_user".($i+1);

                $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()';
                $password = collect(str_split($characters))->random(8)->implode('');

                $user = User::create(
                    [
                        "name" => "$name$lastName",
                        "login" => "$name$lastName",
                        "password" => Hash::make($password),
                        "open_password" => $password
                    ]
                );

                $role = Role::where('name', '=', 'student')->first();

                DB::insert(
                    'insert into users_roles (role_id, user_id) values (?,?)',
                    [
                        $role->id,
                        $user->id
                    ]
                );


                GroupUser::create(["user_id" => $user->id, "group_id" => $group->id]);
            }

            DB::commit();

            return (new \App\DTO\Group($group))->toArray();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function getCoursesByGroup(Group $group): array
    {
        return (new CourseCollection($group->courses))->getData();
    }

    public function deleteGroupFromCourse(Group $group, Course $course): void
    {
        try {
            DB::beginTransaction();

            GroupCourse::where('group_id', $group->id)->where('course_id', $course->id)->forceDelete();

            $group->users->each(function(User $user) use ($course) {
                $user->userCourse($course)->delete();
            });

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function getGroupUsersByCourse(Group $group, Course $course): array
    {
        if (!GroupCourse::where("group_id", $group->id)->where("course_id", $course->id)->first()) {
            throw new HandleException("Данного курса у группы $group->name нет");
        }

        return $group->users->map(function(User $user) use ($course) {
            return new CourseUserInfo($user, $course);
        })->toArray();
    }

    public function getStaticByGroup(Group $group, Course $course)
    {
        $headers = [
            'Content-Type' => 'text/csv, charset=utf-8',
            'Content-Disposition' => 'attachment; filename="stat_users.csv"',
        ];

        $users = $group->users->map(function (User $user) use ($course, $group, &$max) {
            $userTasks = $user->userTasksByCourse($course);

            $dataTasks = $userTasks->map(function (UserCourseTask $task) {
                $userTask = (new UserTask($task, true));

                return $userTask->status == "success" ? 1 : -count($userTask->history);
            });

            return [
                'name' => $user->name,
                'group' => $group->name,
                ...$dataTasks
            ];
        });

        $maxLength = max(array_map('count', $users->toArray())) - 2;

        foreach ($users as &$subArr) {
            $subArr = array_pad($subArr, $maxLength, 'Такой задачи не было');
        }

        $array = ['Name', 'Group'];

        for ($i = 0; $i < $maxLength; $i++) {
            $array[] = "Task".($i+1);
        }

        $callback = function() use ($users, $array) {
            $file = fopen('php://output', 'w');

            fwrite($file, chr(0xEF) . chr(0xBB) . chr(0xBF));

            fputcsv($file, $array, ";");

            $users->each(function ($user) use ($file) {
                fputcsv($file, array_values($user), ";");
            });

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function setRandomCourseTasksToGroup(Group $group, Course $course, SetCourseCollection $collection): void
    {
        try {
            DB::beginTransaction();

            if (!GroupCourse::where("group_id", $group->id)->where("course_id", $course->id)->first()) {
                GroupCourse::create(
                    [
                        "group_id" => $group->id,
                        "course_id" => $course->id
                    ]
                );
            }

            $group->users->each(function(User $user) use ($collection, $course) {
                if ($user->userCourse($course)) {
                    $user->userCourse($course)->delete();
                }

                $userCourse = UserCourse::create([
                    "user_id" => $user->id,
                    "course_id" => $course->id,
                    "tutor_id" => Auth::user()->getAuthIdentifier()
                ]);

                foreach ($collection->getData() as $set) {
                    $tasks = $course->tasks()->where('rating', $set->rating)
                        ->get()
                        ->filter(fn (Task $task) => !$task->variants->isEmpty());

                    $randomTasks = $tasks->count() > $set->count ? $tasks->random($set->count) : $tasks;

                    $randomTasks->each(function(Task $task) use ($userCourse) {
                        UserCourseTask::create([
                            "user_course_id" => $userCourse->id,
                            "task_id" => $task->id,
                            "variant_id" => $task->variants->random()->id
                        ]);
                    });
                }
            });

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function exportUsers(Group $group): StreamedResponse
    {
        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="export_users.csv"',
        ];

        $users = $group->users()->select(['users.id', 'users.name', 'users.login', 'users.open_password'])->get();

        $callback = function() use ($users) {
            $file = fopen('php://output', 'w');
            fputcsv($file, [
                "id",
                "name",
                "login",
                "password"
            ], ";");

            $users->each(function ($user) use ($file) {
                fputcsv($file, [$user->id, $user->name, $user->login, $user->open_password], ";");
            });

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function deleteGroup(Group $group): void
    {
        DB::beginTransaction();

        try {
            $group->users()->forceDelete();

            $group->forceDelete();

            DB::commit();
        } catch (\Throwable $exception)
        {
            DB::rollBack();
            throw $exception;
        }
    }


}
