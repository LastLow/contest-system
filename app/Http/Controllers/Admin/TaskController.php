<?php

namespace App\Http\Controllers\Admin;

use App\DTO\ChangeTask;
use App\DTO\VariantTest;
use App\Exceptions\HandleException;
use App\Http\Controllers\Admin\Interfaces\ITaskController;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Task;
use App\Models\Test;
use App\Models\Variant;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller implements ITaskController
{

    public function getTask(Task $task)
    {
        return response()->streamDownload(function() use ($task) {
            echo json_encode((new \App\DTO\Task($task))->toArray(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        }, 'task.json', [
            'Content-Type' => 'application/json'
        ]);
    }
    public function createTasksFromShitFormat(Request $request, Course $course): JsonResponse
    {
        if (empty($request->all())) {
            throw new HandleException("Пустой массив", 400);
        }

        $validator = Validator::make($request->all(), [
            '*.rating' => 'required|integer',
            '*.task' => 'required|string',
            '*.variants' => 'required|array',
            '*.variants.*' => 'required|array',
            '*.variants.*.*.in' => 'nullable|string',
            '*.variants.*.*.out' => 'nullable|string',
            '*.variants.*.*.score' => 'required|integer',
            '*.variants.*.*.time' => 'nullable|integer'
        ]);

        if ($validator->fails()) {
            $errors = array_map(function(array $message) {
                return (string) $message[0];
            }, $validator->errors()->toArray());

            throw new HandleException("Пропуск полей", 400, array_values($errors));
        }


        try {
            DB::beginTransaction();

            foreach ($request->all() as $task) {
                $createdTask = Task::create(
                    [
                        "description" => $task['task'],
                        "rating"      => $task['rating'],
                        "course_id"   => $course->id
                    ]
                );

                foreach ($task['variants'] as $number => $tests) {
                    $variant = Variant::create(
                        [
                            "number"  => $number,
                            "task_id" => $createdTask->id
                        ]
                    );

                    foreach ($tests as $key => $test) {
                        Test::create(
                            [
                                'in'         => $test['in'],
                                'out'        => $test['out'],
                                'score'      => $test['score'],
                                'time'       => $test['time'] ?? 40,
                                'variant_id' => $variant->id
                            ]
                        );
                    }
                }
            }

            DB::commit();

            return response()->json(["message" => "Успешно созданы задачи"], 201);
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
    }


    public function updateTask(Task $task, ChangeTask $changeTask): array
    {
        $errors = [];

        if ($task->id != $changeTask->id) {
            $errors[] = "Введен неправильный id задачи";
        }

        foreach ($changeTask->variants as $changeVariant) {
            $variant = Variant::whereId($changeVariant->id)->first();

            if ($changeVariant->id && !$variant) {
                $errors[] = "Неправильный id -> $changeVariant->id, варианта не существует";
            }

            if ($variant && $variant->task->id != $changeTask->id) {
                $errors[] = "Неправильный id -> $changeVariant->id, вариант относится не к этой задаче";
            }

            if (!$variant) {
                foreach ($changeVariant->tests as $test)
                    if ($test->id) $errors[] = "У нового варианта не может быть теста с id. Id теста $test->id"; continue;
            }

            $idTests = [];

            foreach ($changeVariant->tests as $changeTest) {
                if ($changeTest->id) {
                    if (array_search($changeTest->id, $idTests) !== false) {
                        throw new HandleException("$changeTest->id Id теста должен быть индивидуальным");
                    } else {
                        $idTests[] = $changeTest->id;
                    }

                    $test = Test::whereId($changeTest->id)->first();

                    if (!$test) {
                        $errors[] = "Неправильный id -> $changeTest->id, теста не существует";
                        continue;
                    }


                    if ($test->variant->id != $changeVariant->id) $errors[] = "Неправильный id -> $changeTest->id, тест относится не к этому варианту $changeVariant->id";
                }
            }
        }

        if ($errors) {
            throw new HandleException("Неправильный json", 400, $errors);
        }

        DB::beginTransaction();

        try {
            $task->description = $changeTask->description;
            $task->rating = $changeTask->rating;

            $task->save();

            $deleteVariants = array_diff(
                $task->variants()->pluck('id')->toArray(),
                array_map(fn(\App\DTO\Variant $variant) => $variant->id, $changeTask->variants)
            );

            $deleteTests = [];
            foreach ($changeTask->variants as $deleteVar) {
                if (!$deleteVar->id) continue;

                $deleteTests[$deleteVar->id] = array_map(fn (VariantTest $test) => $test->id, $deleteVar->tests);
            }



            $task->variants->each(function(Variant $variant) use ($deleteVariants, $deleteTests) {
                if (array_search($variant->id, $deleteVariants)) {
                    $variant->delete();
                    return;
                }

                if (!isset($deleteTests[$variant->id])) return;

                $deleteTestsElems = array_diff(
                    $variant->tests()->pluck('id')->toArray(),
                    $deleteTests[$variant->id]
                );

                $variant->tests->each(function(Test $test) use ($variant, $deleteTestsElems) {
                    if (array_search($test->id, $deleteTestsElems)) {
                        $test->delete();
                    }
                });
            });

            foreach ($changeTask->variants as $changeVariant) {
                $variant = Variant::whereId($changeVariant->id)->first();

                if ($variant) {
                    $variant->number = $changeVariant->number;
                    $variant->save();
                } else {
                    $variant = Variant::create(
                        [
                            'number'  => $changeVariant->number,
                            "task_id" => $changeTask->id
                        ]
                    );
                }

                foreach ($changeVariant->tests as $changeTest) {
                    if ($changeTest->id) {
                        $test = Test::whereId($changeTest->id)->first();

                        $test->in = $changeTest->in ?? null;
                        $test->out = $changeTest->out ?? null;
                        $test->score = $changeTest->score;
                        $test->time = $changeTest->time ?? 50;

                        $test->save();
                        continue;
                    }

                    Test::create(array_merge(["variant_id" => $variant->id], $changeTest->toArray()));
                }
            }

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }

        return (new \App\DTO\Task(Task::whereId($changeTask->id)->first()))->toArray();
    }

    public function deleteTask(Task $task): void
    {
        try {
            DB::beginTransaction();

            $task->delete();

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
