<?php

namespace App\Http\Controllers\Admin;

use App\DTO\Collections\UserTaskInfoCollection;
use App\DTO\UserTask;
use App\Exceptions\HandleException;
use App\Http\Controllers\Admin\Interfaces\IUserController;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\User;
use App\Models\UserCourse;
use App\Models\UserCourseTask;
use Symfony\Component\HttpFoundation\StreamedResponse;

class UserController extends Controller implements IUserController
{
    public function getAll(): array
    {
        return User::all()->map(fn(User $user) => new \App\DTO\User($user))->toArray();
    }

    public function getCoursesByUser(User $user)
    {
        return $user->userCourses->map(fn(UserCourse $course) => new \App\DTO\UserCourse($course))->toArray();
    }

    public function getTasksByCourse(User $user, Course $course): array
    {
        return (new UserTaskInfoCollection($user->userTasksByCourse($course)))->getData();
    }

    public function getInfoAboutUserTask(User $user, UserCourseTask $task): array
    {
        if (!$user->userTasksByCourse($task->userCourse->course)->contains($task)) {
            throw new HandleException("Задачи с id '$task->id' у пользователя '$user->name' нет");
        }

        return (new UserTask($task, false))->toArray();
    }

    public function exportUsers(int $groupId): StreamedResponse
    {
        // TODO: Implement exportUsers() method.
    }
}
