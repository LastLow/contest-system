<?php

namespace App\Http\ControllersNew;

use App\DTOs\UserCourseDto;
use App\DTOs\UserTaskInfoDto;
use App\Http\ControllersNew\Interfaces\ICourseController;
use App\Layer\Interfaces\Repositories\IAuthService;
use App\Layer\Interfaces\Services\ICourseService;
use App\Layer\Interfaces\Services\ITaskService;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller implements ICourseController
{
    public function __construct(
        private readonly ICourseService $courseService,
        private readonly ITaskService   $taskService,
        private readonly IAuthService $authService
    ) {
    }

    /**
     * @param Request $request
     * @return Collection<UserCourseDto>
     */
    public function getCoursesByUser(Request $request): Collection
    {
        return $this->courseService->getCoursesByUser($this->authService->getAuthorizeUserId());
    }

    /**
     * @param Request $request
     * @param int $course
     * @return Collection<UserTaskInfoDto>
     */
    public function getTasksByCourseId(Request $request, int $course): Collection
    {
        return $this->taskService->getTasksInCourseByUser($course, $this->authService->getAuthorizeUserId());
    }
}
