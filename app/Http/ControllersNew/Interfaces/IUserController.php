<?php

namespace App\Http\ControllersNew\Interfaces;

use OpenApi\Annotations as OA;

interface IUserController
{
    /**
     * @OA\Get(
     *     path="/api/user",
     *     summary="Получение информации о пользователе",
     *     tags={"User"},
     *     operationId="getInfoUser",
     *     security={{"sanctum":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Успешный ответ",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     ),
     *      @OA\Response(
     *          response="401",
     *          description="Не авторизирован"
     *      )
     * )
     */

    public function getInfoUser();
}
