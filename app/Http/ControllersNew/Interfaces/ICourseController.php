<?php

namespace App\Http\ControllersNew\Interfaces;

use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

interface ICourseController
{
    /**
     * @OA\Get(
     *     path="/api/courses",
     *     summary="Получение курсов пользователя",
     *     description="Возвращает список курсов у пользователя",
     *     tags={"Courses"},
     *     operationId="getStudentCourses",
     *     security={{"sanctum":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *          @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/UserCourseDto"
     *            )
     *         )
     *     )
     * )
     */
    public function getCoursesByUser(Request $request);

    /**
     * @OA\Get(
     *     path="/api/courses/{course_id}/tasks",
     *     summary="Получение задач",
     *     description="Возвращает список задач",
     *     tags={"Courses"},
     *     operationId="getStudentTasks",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/UserTaskInfoDto"
     *            )
     *         )
     *     )
     * )
     */
    public function getTasksByCourseId(Request $request, int $course);
}
