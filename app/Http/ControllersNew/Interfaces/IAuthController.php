<?php

namespace App\Http\ControllersNew\Interfaces;

use App\Http\Requests\LoginRequest;
use OpenApi\Annotations as OA;

interface IAuthController
{

    /**
     * @OA\Post(
     *     path="/api/login",
     *     summary="Авторизация пользователя",
     *     tags={"Auth"},
     *     operationId="login",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Login")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успешный ответ",
     *         @OA\JsonContent(ref="#/components/schemas/UserToken")
     *     )
     * )
     */
    public function login(LoginRequest $request);


    /**
     * @OA\Post(
     *     path="/api/logout",
     *     summary="Разлогирование пользователя",
     *     tags={"Auth"},
     *     operationId="logout",
     *     security={{"sanctum":{}}},
     *     @OA\Response(
     *         response=204,
     *          description=""
     *     )
     * )
     */
    public function logout();
}
