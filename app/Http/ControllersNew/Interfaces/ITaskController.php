<?php

namespace App\Http\ControllersNew\Interfaces;

use App\Http\Requests\CheckCodeRequest;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

interface ITaskController
{
    /**
     * @OA\Get(
     *     path="/api/tasks/{task_id}",
     *     summary="Получение задачи",
     *     description="Возвращает задачу",
     *     operationId="getStudentTask",
     *     tags={"Tasks"},
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="task_id",
     *         in="path",
     *         description="Id задачи",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/UserTask")
     *     )
     * )
     */

    public function getTaskById(Request $request, int $taskId);


    /**
     * @OA\Post(
     *     path="/api/check",
     *     summary="Закинуть задачу на проверку",
     *     tags={"Check"},
     *     operationId="setCheckTask",
     *     security={{"sanctum":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/CheckCode")
     *         )
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description=""
     *     )
     * )
     */
    public function sendTaskToCheck(CheckCodeRequest $request);
}
