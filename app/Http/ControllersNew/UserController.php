<?php

namespace App\Http\ControllersNew;

use App\Http\Controllers\Controller;
use App\Http\ControllersNew\Interfaces\IUserController;
use App\Layer\Interfaces\Repositories\IAuthService;
use App\Layer\Interfaces\Services\IUserService;
use Illuminate\Http\JsonResponse;

class UserController extends Controller implements IUserController
{
    public function __construct(
        readonly private IUserService $userService,
        private readonly IAuthService $authService
    ) {
    }

    public function getInfoUser(): JsonResponse
    {
        return response()->json(
            $this->userService->getInfoUserById(
                $this->authService->getAuthorizeUserId()
            )
        );
    }
}
