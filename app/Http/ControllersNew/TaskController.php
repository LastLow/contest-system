<?php

namespace App\Http\ControllersNew;

use App\DTO\CheckCode;
use App\Http\Controllers\Controller;
use App\Http\ControllersNew\Interfaces\ITaskController;
use App\Http\Requests\CheckCodeRequest;
use App\Layer\Interfaces\Repositories\IAuthService;
use App\Layer\Interfaces\Services\IHistoryService;
use App\Layer\Interfaces\Services\ITaskService;
use App\Layer\Types\Permission;
use App\Layer\Types\Status;
use App\Models\History;
use App\Models\UserCourseTask;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller implements ITaskController
{
    public function __construct(
        private readonly ITaskService $taskService,
        private readonly IHistoryService $historyService,
        private readonly IAuthService $authService
    ) {
    }

    public function getTaskById(Request $request, int $taskId): JsonResponse
    {
        return response()->json(
            $this->taskService->getTaskByUser(
                $taskId,
                $this->authService->getAuthorizeUserId()
            )
        );
    }

    public function sendTaskToCheck(CheckCodeRequest $request): Response
    {
        $checkCode = $request->toDto();

        $lastCheck = $this->historyService->getLastHistoryByTaskNUser(
            $checkCode->taskId, $this->authService->getAuthorizeUserId()
        );

        // TODO: Добавить в UserService проверку на роль или на право
        if ($lastCheck->status == Status::Success
            && !Auth::user()->hasPermission(Permission::AccessAdmin)) {
            throw new \RuntimeException("Проверка после успешного результата не доступна");
        }

        $this->historyService->createCheckCode(
            $checkCode,
            $this->authService->getAuthorizeUserId()
        );

        // TODO: Добавить отправку по Event

        return response('', 204);
    }
}
