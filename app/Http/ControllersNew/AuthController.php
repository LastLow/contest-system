<?php

namespace App\Http\ControllersNew;

use App\Http\Controllers\Controller;
use App\Http\ControllersNew\Interfaces\IAuthController;
use App\Http\Requests\LoginRequest;
use App\Layer\Interfaces\Repositories\IAuthService;
use App\Layer\Interfaces\Services\IUserService;
use Illuminate\Http\Client\HttpClientException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cookie;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller implements IAuthController
{
    public function __construct(
        readonly private IAuthService $authService
    ) {
    }

    public function login(LoginRequest $request): JsonResponse
    {
        try {
            $user = $this->authService->login($request->toDto());

            return \response()->json($user);
        } catch (\Throwable $exception) {
            // TODO: Передалать логику ошибок
            throw new HttpClientException('Неправильные данные', Response::HTTP_UNAUTHORIZED);
        }
    }

    public function logout(): \Illuminate\Http\Response
    {
        try {
            $this->authService->logout();

            return response('', 204);
        } catch (\Throwable $exception) {
            throw new HttpClientException('Что-то не так при выходе из аккаунта', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
