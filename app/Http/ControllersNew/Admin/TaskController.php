<?php

namespace App\Http\ControllersNew\Admin;

use App\DTOs\TaskDto;
use App\DTOs\TaskInfoDto;
use App\DTOs\UserTaskInfoDto;
use App\Exceptions\HandleException;
use App\Http\ControllersNew\Admin\Interfaces\ITaskController;
use App\Http\ControllersNew\Controller;
use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Layer\Interfaces\Services\ITaskService;
use App\Models\Course;
use App\Models\Task;
use App\Models\Test;
use App\Models\Variant;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller implements ITaskController
{

    public function __construct(
        private readonly ITaskService $taskService
    )
    {
    }

    /**
     * @return Collection<UserTaskInfoDto>
     */
    public function getTasksByCourseNUser(Request $request, int $courseId, int $userId): Collection
    {
        return $this->taskService->getTasksInCourseByUser($courseId, $userId);
    }

    public function getTaskByUser(Request $request, int $taskId, int $userId)
    {
        return $this->taskService->getTaskByUser($taskId, $userId);
    }

    /**
     * @param Request $request
     * @param int $courseId
     * @return Collection<TaskInfoDto>
     */
    public function getTasksByCourse(Request $request, int $courseId): Collection
    {
        return $this->taskService->getTasksByCourseId($courseId);
    }

    public function getTask(Request $request, int $taskId): TaskDto
    {
        return $this->taskService->getTask($taskId);
    }

    public function getTasksByCourseNGroup(Request $request, int $courseId, int $groupId)
    {
        // Данный метод получает задачи курса, которые рандомно были назначены пользователем группы,
        // На данном этапе смысла в этой функции я не вижу
        throw new HandleException("getTasksByCourseNGroup NULL");
    }

    /**
     * Функция создания курса через нестандартную и неудобную структуру передаваемых данных
     * Ее код я рефакторить не стал, а то просто понапишу бесполезного "правильного" кода
     */
    public function createTasksFromShitFormat(Request $request, int $courseId)
    {
        if (empty($request->all())) {
            throw new HandleException("Пустой массив", 400);
        }

        $validator = Validator::make($request->all(), [
            '*.rating' => 'required|integer',
            '*.task' => 'required|string',
            '*.variants' => 'required|array',
            '*.variants.*' => 'required|array',
            '*.variants.*.*.in' => 'nullable|string',
            '*.variants.*.*.out' => 'nullable|string',
            '*.variants.*.*.score' => 'required|integer',
            '*.variants.*.*.time' => 'nullable|integer'
        ]);

        if ($validator->fails()) {
            $errors = array_map(function(array $message) {
                return (string) $message[0];
            }, $validator->errors()->toArray());

            throw new HandleException("Пропуск полей", 400, array_values($errors));
        }

        $course = Course::find($courseId);

        try {
            DB::beginTransaction();

            foreach ($request->all() as $task) {
                $createdTask = Task::create(
                    [
                        "description" => $task['task'],
                        "rating"      => $task['rating'],
                        "course_id"   => $course->id
                    ]
                );

                foreach ($task['variants'] as $number => $tests) {
                    $variant = Variant::create(
                        [
                            "number"  => $number,
                            "task_id" => $createdTask->id
                        ]
                    );

                    foreach ($tests as $key => $test) {
                        Test::create(
                            [
                                'in'         => $test['in'],
                                'out'        => $test['out'],
                                'score'      => $test['score'],
                                'time'       => $test['time'] ?? 40,
                                'variant_id' => $variant->id
                            ]
                        );
                    }
                }
            }

            DB::commit();

            return response()->json(["message" => "Успешно созданы задачи"], 201);
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
    }


    public function createTask(CreateTaskRequest $request): TaskDto
    {
        return $this->taskService->updateOrCreateTask($request->toDto());
    }

    public function updateTask(UpdateTaskRequest $request): TaskDto
    {
        return $this->taskService->updateOrCreateTask($request->toDto());
    }

    public function deleteTask(int $taskId): Application|Response|\Illuminate\Contracts\Foundation\Application|ResponseFactory
    {
        $this->taskService->delete($taskId);
        return response("", 204);
    }
}
