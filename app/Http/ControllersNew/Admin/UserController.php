<?php

namespace App\Http\ControllersNew\Admin;

use App\DTOs\UserInfo;
use App\Exceptions\HandleException;
use App\Http\ControllersNew\Admin\Interfaces\IUserController;
use App\Http\ControllersNew\Controller;
use App\Layer\Interfaces\Services\IUserService;
use Illuminate\Support\Collection;

class UserController extends Controller implements IUserController
{
    public function __construct(
        readonly private IUserService $userService
    ) {
    }

    public function getUsersByCourse(int $courseId)
    {
        // TODO: Метод возвращает пользователей курса
        throw new HandleException("getUsersByCourse NOT NULL");
    }

    public function getUsersOutCourse(int $courseId): array
    {
        // Получение студентов, которым можно назначить курс, так как курс назначается на данный момент только группе
        // текущий функционал не предполагает такой логики, эта функция бесполезна
        throw new HandleException("getUsersByCourse NULL");
    }

    public function getUsersByGroup(int $groupId): Collection
    {
        return $this->userService->getUsersByGroup($groupId);
    }

    public function getUsersByCourseNGroup(int $groupId, int $courseId): Collection
    {
        return $this->userService->getUsersByGroup($groupId)
            ->map(fn (UserInfo $user) => $this->userService->getInfoAboutCourse($user, $courseId));
    }
}
