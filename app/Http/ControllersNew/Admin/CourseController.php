<?php

namespace App\Http\ControllersNew\Admin;

use App\Exceptions\HandleException;
use App\Http\ControllersNew\Admin\Interfaces\ICourseController;
use App\Http\ControllersNew\Controller;
use App\Http\Requests\CourseRequest;
use App\Layer\Interfaces\Services\ICourseService;

use App\Models\Course;
use App\Models\Task;
use App\Models\Test;
use App\Models\Variant;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller implements ICourseController
{
    public function __construct(
        readonly private ICourseService $courseService
    )
    {}

    public function getCourses(): Collection
    {
        return $this->courseService->getCourses();
    }

    public function createByShitFormat(Request $request): JsonResponse
    {
        if (!$request->has('tasks') || !$request->has('course_name')) {
            throw new HandleException("Нет tasks или course_name", 400);
        }

        $validator = Validator::make($request->input('tasks'), [
            '*.rating'             => 'required|integer',
            '*.task'               => 'required|string',
            '*.variants.*.*'       => 'array',
            '*.variants.*.*.in'    => 'nullable|string',
            '*.variants.*.*.out'   => 'nullable|string',
            '*.variants.*.*.score' => 'required|integer',
            '*.variants.*.*.time'  => 'nullable|integer'
        ]);

        if ($validator->fails()) {
            $errors = array_map(function(array $message) {
                return (string)$message[0];
            }, $validator->errors()->toArray());

            throw new HandleException("Пропуск полей в задачах", 400, array_values($errors));
        }

        try {
            DB::beginTransaction();

            $course = Course::create(['name' => $request->input('course_name')]);

            foreach ($request->input('tasks') as $task) {
                $createdTask = Task::create(
                    [
                        "description" => $task['task'],
                        "rating"      => $task['rating'],
                        "course_id"   => $course->id
                    ]
                );

                foreach ($task['variants'] as $number => $tests) {
                    $variant = Variant::create(
                        [
                            "number"  => $number,
                            "task_id" => $createdTask->id
                        ]
                    );

                    foreach ($tests as $key => $test) {
                        Test::create(
                            [
                                'in'         => $test['in'],
                                'out'        => $test['out'],
                                'score'      => $test['score'],
                                'time'       => $test['time'] ?? 40,
                                'variant_id' => $variant->id
                            ]
                        );
                    }
                }
            }

            DB::commit();

            return response()->json(
                  [
                      "message" => "Успешно создан курс {$request->input('course_name')}"
                  ]
                , 201
            );
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function create(CourseRequest $request): JsonResponse
    {
        return response()->json($this->courseService->create($request->toDto()), 201);
    }

    public function updateCourse(CourseRequest $request, int $courseId): JsonResponse
    {
        return response()->json($this->courseService->update($courseId, $request->toDto()));
    }

    public function deleteCourse(Request $request, int $courseId): Response
    {
        $this->courseService->delete($courseId);

        return response(status: 204);
    }
}
