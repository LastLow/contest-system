<?php

namespace App\Http\ControllersNew\Admin\Interfaces;

use App\Models\Course;
use Illuminate\Support\Collection;
use OpenApi\Annotations as OA;

interface IUserController
{
    /**
     * @deprecated Пока, что не нужен
     *
     * @OA\Get(
     *     path="/api/admin/courses/{course_id}/users",
     *     summary="Получение пользователей курса",
     *     description="Возвращает список пользователей, которым назаначен курс, вне группы",
     *     tags={"AdminCourses"},
     *     operationId="getUsersByCourse",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/UserInfo"
     *            )
     *         )
     *     )
     * )
     */
    public function getUsersByCourse(int $courseId);

    /**
     * Получение студентов, которым можно назначить курс
     * @deprecated Пока, что не нужен
     *
     * @param Course $course
     * @return array
     */
    public function getUsersOutCourse(int $courseId): array;

    /**
     * @OA\Get(
     *     path="/api/admin/groups/{group_id}/users",
     *     summary="Получение пользователей в группе",
     *     description="Возвращает список пользователей в группе",
     *     tags={"AdminGroups"},
     *     operationId="getUsers",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="group_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/UserInfo"
     *            )
     *         )
     *     )
     * )
     */
    public function getUsersByGroup(int $groupId): Collection;

    /**
     * @OA\Get(
     *     path="/api/admin/groups/{group_id}/courses/{course_id}/users",
     *     summary="Получение пользователей, которые имею курс и находятся в группе",
     *     description="Возвращает список пользователей, которые имею курс и находятся в группе",
     *     tags={"AdminGroups"},
     *     operationId="getGroupUsersByCourse",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="group_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/CourseUserInfo"
     *            )
     *         )
     *     )
     * )
     */
    public function getUsersByCourseNGroup(int $groupId, int $courseId): Collection;
}
