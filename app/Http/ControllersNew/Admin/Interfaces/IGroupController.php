<?php

namespace App\Http\ControllersNew\Admin\Interfaces;

use App\Http\Requests\CreateGroupRequest;
use App\Http\Requests\SetCourseForGroupRequest;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use OpenApi\Annotations as OA;

interface IGroupController
{
    /**
     * @OA\Get(
     *     path="/api/admin/groups",
     *     summary="Получение всех групп",
     *     description="Возвращает список всех групп",
     *     tags={"AdminGroups"},
     *     operationId="getAll",
     *     security={{"sanctum":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/Group"
     *            )
     *         )
     *     )
     * )
     */
    public function getGroups(Request $request): JsonResponse;

    /**
     * @OA\Get(
     *     path="/api/admin/courses/{course_id}/groups",
     *     summary="Получение групп курса",
     *     description="Возвращает список групп, которым назаначен курс",
     *     tags={"AdminCourses"},
     *     operationId="getGroupsByCourse",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/Group"
     *            )
     *         )
     *     )
     * )
     */
//    public function getGroupsByCourse(int $courseId);

    /**
     * @OA\Get(
     *     path="/api/admin/courses/{course_id}/groups/out",
     *     summary="Получение групп, которые не имею курса",
     *     description="Возвращает список групп, которым не назаначен данный курс",
     *     tags={"AdminCourses"},
     *     operationId="getGroupsOutCourse",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/Group"
     *            )
     *         )
     *     )
     * )
     */
//    public function getGroupsOutCourse(int $courseId);

    /**
     * @OA\Post(
     *     path="/api/admin/groups",
     *     summary="Создание группы",
     *     description="Создание группы",
     *     tags={"AdminGroups"},
     *     operationId="createGroup",
     *     security={{"sanctum":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/CreateGroup")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="object",
     *             ref="#/components/schemas/Group"
     *         )
     *     )
     * )
     */
    public function createGroup(CreateGroupRequest $createGroup);

    /**
     * @OA\Post(
     *     path="/api/admin/groups/{group_id}/courses/{course_id}/random",
     *     summary="Распределение задач курса по группе",
     *     description="Распределение задач курса по группе",
     *     tags={"AdminGroups"},
     *     operationId="setRandomCourseTasksToGroup",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="group_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(ref="#/components/schemas/SetCourseRating")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *      )
     * )
     */
    public function setCourseToGroup(SetCourseForGroupRequest $request, int $groupId);

    /**
     * @OA\Delete(
     *     path="/api/admin/groups/{group_id}/courses/{course_id}",
     *     summary="Удаление курса у группы",
     *     description="Удаляет курс у группы",
     *     tags={"AdminGroups"},
     *     operationId="deleteGroupFromCourse",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="group_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *     )
     * )
     */
    public function deleteCourseFromGroup(Request $request, int $groupId);

    /**
     * @OA\Delete(
     *     path="/api/admin/groups/{group_id}",
     *     summary="Удаление курса",
     *     description="Удаляет курс",
     *     operationId="deleteGroup",
     *     tags={"AdminGroups"},
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="group_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *     )
     * )
     */
    public function deleteGroup(int $groupId);
}
