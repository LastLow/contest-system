<?php

namespace App\Http\ControllersNew\Admin\Interfaces;

use App\Http\Requests\CourseRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

interface ICourseController
{
    // TODO: Реализовать создание просто курса, а после вызывать метод добавления задач
    // public function create();

    /**
     * @OA\Post(
     *     path="/api/admin/courses/format",
     *     tags={"AdminCourses"},
     *     summary="Создание курса",
     *     operationId="createCourse",
     *     security={{"sanctum":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="object",
     *             required={"course_name", "tasks"},
     *             @OA\Property(
     *                 property="course_name",
     *                 type="string",
     *                 example="Курс по строению json"
     *             ),
     *             @OA\Property(
     *                 property="tasks",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     required={"task", "rating", "variants"},
     *                     @OA\Property(
     *                         property="task",
     *                         type="string",
     *                         example="Выстроить json"
     *                     ),
     *                     @OA\Property(
     *                         property="rating",
     *                         type="integer",
     *                         example=1
     *                     ),
     *                     @OA\Property(
     *                         property="variants",
     *                         type="object",
     *                         example={"1":{"1":{"in":"1223445799","out":"4","score":10,"time":30}}},
     *                         @OA\AdditionalProperties(
     *                             type="object",
     *                             required={"score"},
     *                             @OA\Property(
     *                                 property="in",
     *                                 type="string",
     *                                 nullable=true
     *                             ),
     *                             @OA\Property(
     *                                 property="out",
     *                                 type="string",
     *                                 nullable=true
     *                             ),
     *                             @OA\Property(
     *                                 property="score",
     *                                 type="integer"
     *                             ),
     *                              @OA\Property(
     *                                 property="time",
     *                                 type="integer",
     *                                 nullable=true
     *                             ),
     *                             example={"1": {"in": "example_in", "out": "example_out", "score": 1}}
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *     )
     * )
     */
    public function createByShitFormat(Request $request): JsonResponse;

    /**
     * @OA\Get(
     *     path="/api/admin/courses",
     *     summary="Получение курсов",
     *     description="Возвращает список курсов",
     *     tags={"AdminCourses"},
     *     operationId="getCourses",
     *     security={{"sanctum":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *          @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/Course"
     *            )
     *         )
     *     )
     * )
     */
    public function getCourses();
    public function updateCourse(CourseRequest $request, int $courseId);
    public function deleteCourse(Request $request, int $courseId);
}
