<?php

namespace App\Http\ControllersNew\Admin\Interfaces;

use App\DTOs\TaskDto;
use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Http\Requests\CreateTasksOldDataRequest;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

interface ITaskController
{
    /**
     * @OA\Get(
     *     path="/api/admin/users/{user_id}/courses/{course_id}/tasks",
     *     summary="Получение задач курса пользователя",
     *     description="Возвращает список задач курса пользователя",
     *     tags={"AdminUsers"},
     *     operationId="getUserTasksByCourse",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="Id пользователя",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/UserTaskInfo"
     *            )
     *         )
     *     )
     * )
     */
    public function getTasksByCourseNUser(Request $request, int $courseId, int $userId);

    /**
     * @OA\Get(
     *     path="/api/admin/users/{user_id}/tasks/{task_id}",
     *     summary="Получение информации о задачи пользователя",
     *     description="Возвращает информацию о задачи пользователя",
     *     operationId="getInfoAboutUserTask",
     *     tags={"AdminUsers"},
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="Id пользователя",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="task_id",
     *         in="path",
     *         description="Id задачи",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/UserTask"
     *            )
     *         )
     *     )
     * )
     */
    public function getTaskByUser(Request $request, int $taskId, int $userId);

    /**
     * @OA\Get(
     *     path="/api/admin/courses/{course_id}/tasks",
     *     summary="Получение задач курса",
     *     operationId="getTasksByCourse",
     *     description="Возвращает список задач курса",
     *     tags={"AdminCourses"},
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/TaskInfo"
     *            )
     *         )
     *     )
     * )
     */
    public function getTasksByCourse(Request $request, int $courseId);

    /**
     * @OA\Get(
     *     path="/api/admin/tasks/{task_id}",
     *     summary="Получение задачи",
     *     description="Получение задачу",
     *     tags={"AdminTasks"},
     *     operationId="getTask",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="task_id",
     *         in="path",
     *         description="Id задачи",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="JSON файл",
     *         @OA\MediaType(
     *             mediaType="text/json",
     *             @OA\Schema(
     *                  type="object",
     *                  ref="#/components/schemas/Task"
     *              )
     *         )
     *     )
     *   )
     * )
     */
    public function getTask(Request $request, int $taskId);

    /**
     * @OA\Get(
     *     path="/api/admin/courses/{course_id}/groups/{group_id}/tasks",
     *     summary="Получение задач курса группы",
     *     description="Возвращает список задач, которые назаначены пользователям данной группы",
     *     tags={"AdminCourses"},
     *     operationId="getGroupTasksByCourse",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Parameter(
     *         name="group_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 ref="#/components/schemas/GroupTaskInfo"
     *            )
     *         )
     *     )
     * )
     */
    public function getTasksByCourseNGroup(Request $request, int $courseId, int $groupId);

    /**
     * @OA\Post(
     *     path="/api/admin/tasks/courses/{course_id}/format",
     *     tags={"AdminTasks"},
     *     summary="Создание задач",
     *     security={{"sanctum":{}}},
     *     operationId="createTasksFromShitFormat",
     *     @OA\Parameter(
     *         name="course_id",
     *         in="path",
     *         description="Id курса",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     required={"task", "rating", "variants"},
     *                     @OA\Property(
     *                         property="task",
     *                         type="string",
     *                         example="Выстроить json"
     *                     ),
     *                     @OA\Property(
     *                         property="rating",
     *                         type="integer",
     *                         example=1
     *                     ),
     *                     @OA\Property(
     *                         property="variants",
     *                         type="object",
     *                         example={"1":{"1":{"in":"1223445799","out":"4","score":10,"time":30}}},
     *                         @OA\AdditionalProperties(
     *                             type="object",
     *                             required={"score"},
     *                             @OA\Property(
     *                                 property="in",
     *                                 type="string",
     *                                 nullable=true
     *                             ),
     *                             @OA\Property(
     *                                 property="out",
     *                                 type="string",
     *                                 nullable=true
     *                             ),
     *                             @OA\Property(
     *                                 property="score",
     *                                 type="integer"
     *                             ),
     *                              @OA\Property(
     *                                 property="time",
     *                                 type="integer",
     *                                 nullable=true
     *                             ),
     *                             example={"1": {"in": "example_in", "out": "example_out", "score": 1}}
     *                         )
     *                     )
     *                 )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *     )
     * )
     */
    public function createTasksFromShitFormat(CreateTasksOldDataRequest $request, int $courseId);

    public function createTask(CreateTaskRequest $request): TaskDto;

    /**
     * @OA\Put(
     *     path="/api/admin/tasks/{task_id}",
     *     summary="Изменение задачи",
     *     description="Изменяет задачу",
     *     tags={"AdminTasks"},
     *     security={{"sanctum":{}}},
     *     operationId="updateTask",
     *     @OA\Parameter(
     *         name="task_id",
     *         in="path",
     *         description="Id группы",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(ref="#/components/schemas/Task")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *     @OA\JsonContent(
     *             type="object",
     *             ref="#/components/schemas/Task"
     *         )
     *     )
     * )
     */
    public function updateTask(UpdateTaskRequest $request);

    /**
     * @OA\Delete(
     *     path="/api/admin/tasks/{task_id}",
     *     summary="Удаление задачи",
     *     description="Удаляет задачу",
     *     tags={"AdminTasks"},
     *     operationId="deleteTask",
     *     security={{"sanctum":{}}},
     *     @OA\Parameter(
     *         name="task_id",
     *         in="path",
     *         description="Id задачи",
     *         required=true,
     *         @OA\Schema(type="number")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *     )
     * )
     */
    public function deleteTask(int $taskId);
}
