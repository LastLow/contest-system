<?php

namespace App\Http\ControllersNew\Admin;

use App\Http\ControllersNew\Admin\Interfaces\IGroupController;
use App\Http\ControllersNew\Controller;
use App\Http\Requests\CreateGroupRequest;
use App\Http\Requests\SetCourseForGroupRequest;
use App\Layer\Interfaces\Services\IGroupService;
use App\Layer\Types\GroupFilter;
use App\Layer\Types\GroupFilterType;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class GroupController extends Controller implements IGroupController
{
    public function __construct(
        protected readonly IGroupService $groupService
    ) {}

    public function getGroups(Request $request): JsonResponse
    {
        return response()->json($this->groupService->get(new GroupFilter(
            GroupFilterType::tryFrom($request->string('type')),
            $request->integer('page'),
            $request->integer('perPage'),
            $request->has('course_id') ? $request->integer('course_id') : null
        )));
    }

    public function createGroup(CreateGroupRequest $createGroup): JsonResponse
    {
        return response()->json($this->groupService->create($createGroup->toDto()), 201);
    }

    public function setCourseToGroup(SetCourseForGroupRequest $request, int $groupId): Response
    {
        $this->groupService->setCourseToGroup($request->toDto(), $groupId);
        return response()->noContent();
    }

    public function deleteGroup(int $groupId): Response
    {
        $this->groupService->deleteGroup($groupId);
        return response()->noContent();
    }

    public function deleteCourseFromGroup(Request $request, int $groupId): Response
    {
        $request->validate(['course_id' => 'required|integer']);

        $this->groupService->deleteCourseFromGroup($groupId, $request->integer('course_id'));
        return response()->noContent();
    }
}
