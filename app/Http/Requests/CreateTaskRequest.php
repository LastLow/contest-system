<?php

namespace App\Http\Requests;

use App\DTOs\CreateTaskDto;
use App\DTOs\CreateTestDto;
use App\DTOs\CreateVariantDto;
use Illuminate\Foundation\Http\FormRequest;

class CreateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'description' => 'required|string',
            'rating' => 'required|number|min:1|max:5',
            'course_id' => 'required|number|exists:courses,id',
            'variants' => 'required|array',
            'variants.*.number' => 'required|string',
            'variants.*.tests' => 'required|array',
            'variants.*.tests.*.in' => 'nullable|string',
            'variants.*.tests.*.out' => 'nullable|string',
            'variants.*.tests.*.score' => 'required|number',
            'variants.*.tests.*.time' => 'required|number',
        ];
    }

    public function toDto(): CreateTaskDto
    {
        $variants = collect($this->validated('variants'))->map(function ($variant) {
            $tests = collect($variant['tests'])->map(function ($test) {
                return new CreateTestDto($test['in'], $test['out'], $test['score'], $test['time']);
            })->toArray();

            return new CreateVariantDto($variant['number'], $tests);
        });

        return new CreateTaskDto(
            $this->validated('description'),
            $this->validated('rating'),
            $variants->toArray(),
            $this->validated('course_id')
        );

    }
}
