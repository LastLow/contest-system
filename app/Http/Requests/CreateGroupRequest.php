<?php

namespace App\Http\Requests;

use App\DTOs\CreateGroupDto;
use Illuminate\Foundation\Http\FormRequest;

class CreateGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:100',
            'count' => 'required|integer|min:1',
        ];
    }

    public function toDto(): CreateGroupDto
    {
        return new CreateGroupDto(
            $this->validated('name'),
            $this->validated('count')
        );
    }
}
