<?php

namespace App\Http\Requests;

use App\DTOs\ItemRating;
use App\DTOs\SetCourseDto;
use Illuminate\Foundation\Http\FormRequest;

class SetCourseForGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'settings.*.rating' => 'required|numeric',
            'settings.*.count' => 'required|numeric',
            'course_id' => 'required|numeric|exists:tasks,id'
        ];
    }

    public function toDto(): SetCourseDto
    {
        return new SetCourseDto(
            collect($this->validated('settings'))
                ->map(fn($item) => new ItemRating($item['rating'], $item['count'])),
            $this->validated('course_id')
        );
    }
}
