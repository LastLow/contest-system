<?php

namespace App\Http\Requests;

use App\DTOs\CreateTestDto;
use App\DTOs\CreateVariantDto;
use App\DTOs\TestDto;
use App\DTOs\UpdateTaskDto;
use App\DTOs\VariantDto;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'id' => 'required|number|exists:tasks,id',
            'description' => 'required|string',
            'rating' => 'required|number|min:1|max:5',
            'course_id' => 'required|number|exists:courses,id',
            'variants' => 'required|array',
            'variants.*.id' => 'nullable|number|exists:variants,id',
            'variants.*.number' => 'required|string',
            'variants.*.tests' => 'required|array',
            'variants.*.tests.*.id' => 'nullable|number|exists:tests,id',
            'variants.*.tests.*.in' => 'nullable|string',
            'variants.*.tests.*.out' => 'nullable|string',
            'variants.*.tests.*.score' => 'required|number',
            'variants.*.tests.*.time' => 'required|number',
        ];
    }

    public function toDto(): UpdateTaskDto
    {
        $variants = collect($this->validated('variants'))->map(function ($variant) {
            $tests = collect($variant['tests'])->map(function ($test) {
                return isset($test['id'])
                    ? new TestDto($test['id'], $test['in'], $test['out'], $test['score'], $test['time'])
                    : new CreateTestDto($test['in'], $test['out'], $test['score'], $test['time']);
            })->toArray();

            return isset($variant['id'])
                ? new VariantDto($variant['id'], $variant['number'], $tests)
                : new CreateVariantDto($variant['number'], $tests);
        });

        return new UpdateTaskDto(
            $this->validated('id'),
            $this->validated('description'),
            $this->validated('rating'),
            $variants->toArray(),
            $this->validated('course_id')
        );

    }
}
