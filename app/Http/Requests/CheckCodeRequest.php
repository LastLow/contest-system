<?php

namespace App\Http\Requests;

use App\DTOs\CheckCodeDto;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class CheckCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'task_id' => 'required|integer|exists:tasks,id',
            'code' => 'required|string',
            'language' => 'nullable|string',
        ];
    }

    public function toDto(): CheckCodeDto
    {
        return new CheckCodeDto(
            $this->validated('task_id'),
            $this->validated('code'),
            $this->validated('language')
        );
    }
}
