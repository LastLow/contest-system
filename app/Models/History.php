<?php

namespace App\Models;

use App\Layer\Implements\History\HistoryEntity;
use App\Layer\Types\Status;
use App\Models\Contracts\IModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class History extends Model implements IModel
{
    use HasFactory, SoftDeletes;

    protected $table = "history";

    protected $fillable = [
          'id',
          'code',
          'status',
          'score',
          'max_score',
          'language',
          'user_course_task_id'
    ];

    public function userTask(): HasOne
    {
        return $this->hasOne(UserCourseTask::class, 'id',
            'user_course_task_id');
    }

    public function delete(): void
    {
        parent::delete();
        $this->resultTests->each(function(ResultTest $test) {
            $test->delete();
        });
    }

    public function resultTests(): HasMany
    {
        return $this->hasMany(ResultTest::class, 'history_id', 'id');
    }



    public function toEntity(): HistoryEntity
    {
        return new HistoryEntity(
            $this->id,
            $this->code,
            Status::from($this->status),
            $this->score,
            $this->max_score
        );
    }
}
