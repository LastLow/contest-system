<?php

namespace App\Models\Contracts;

interface IModel
{
    public function toEntity();
}
