<?php

namespace App\Models;

use App\Exceptions\HandleException;
use App\Layer\Implements\User\UserEntity;
use App\Layer\Traits\HasRolesAndPermissions;
use App\Models\Contracts\IModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Auth;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Auth implements IModel
{
    use HasApiTokens, HasFactory, Notifiable, HasRolesAndPermissions, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'login',
        'password',
        'open_password'
    ];

    protected $hidden = [
        'password',
        'pivot'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function userCourses(): HasMany
    {
        return $this->hasMany(UserCourse::class);
    }

    public function userTasksByCourse(Course $course)
    {
        $userCourse = UserCourse::where('user_id', $this->id)
            ->where('course_id', $course->id)
            ->first();

        if (!$userCourse) {
            throw new HandleException(Course::getBadRequestErrorMessage() . " у пользователя $this->name", 404);
        }

        return $userCourse->userTasks;
    }

    public function userCourse(Course $course): UserCourse|null
    {
        return UserCourse::where('course_id', $course->id)->where('user_id', $this->id)->first();
    }

    public function groups(): BelongsToMany
    {
        return $this->belongsToMany(Group::class, 'groups_users');
    }

    public function toEntity(): UserEntity
    {
        return new UserEntity(
            $this->id,
            $this->login,
            $this->name,
            $this->password,
            $this->created_at,
            $this?->updated_at ?? null
        );
    }
}
