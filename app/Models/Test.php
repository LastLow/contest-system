<?php

namespace App\Models;

use App\Layer\Implements\Test\TestEntity;
use App\Models\Contracts\IModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Test extends Model implements IModel
{
    use HasFactory, SoftDeletes;

    protected $table = "tests";

    protected $fillable = [
          "id",
          "in",
          "out",
          "score",
          "time",
          "variant_id"
    ];

    public function variant(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Variant::class, 'id', 'variant_id');
    }


    public function toEntity(): TestEntity
    {
        return new TestEntity(
            $this->id,
            $this->in,
            $this->out,
            $this->score,
            $this->time
        );
    }
}
