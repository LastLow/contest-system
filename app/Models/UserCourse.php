<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class UserCourse extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "user_courses";

    protected $fillable = [
          'id',
          'user_id',
          'course_id',
          'tutor_id'
    ];

    public function delete()
    {
        parent::delete();
        $this->userTasks->each(function(UserCourseTask $task) {
             $task->delete();
        });
    }

    public function course(): HasOne
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function userTasks(): HasMany
    {
        return $this->hasMany(UserCourseTask::class, 'user_course_id');
    }

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public static function findByCourseNUser(int $courseId, int $userId): UserCourse
    {
        // TODO: При удалении задачи у пользователя в админ панели, удалять кеш, если он есть
        $userCourse = Cache::remember('user_course_' . $userId . '_' . $courseId, 600, function () use ($userId, $courseId) {
            return self::where('user_id', '=', $userId)
                ->where('course_id', '=', $courseId)
                ->first();
        });

        if (!$userCourse) throw new \RuntimeException("Курс у пользователя не найден");

        return $userCourse;
    }

    public static function getBadRequestErrorMessage(): string
    {
        return "Такого курса не существует";
    }
}
