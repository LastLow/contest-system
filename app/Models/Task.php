<?php

namespace App\Models;

use App\DTO\CreateTask;
use App\Layer\Implements\Task\TaskEntity;
use App\Models\Contracts\IModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model implements IModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'tasks';

    protected $fillable = [
        'id',
        'description',
        'rating',
        'course_id'
    ];

    public function delete()
    {
        parent::delete();

        $this->userTasks->each(function (UserCourseTask $userTask) {
            $userTask->delete();
        });
    }

    public function variants(): HasMany
    {
        return $this->hasMany(Variant::class, 'task_id', 'id');
    }

    public function userTasks(): HasMany
    {
        return $this->hasMany(UserCourseTask::class, 'task_id', 'id');
    }

    public function course(): HasOne
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public static function createTask(Course $course, CreateTask $task): void
    {
        $task = Task::create([
                                 "description" => $task->description,
                                 "rating"      => $task->rating,
                                 "course_id"   => $course->id
                             ]);

        foreach ($task->variants as $variantItem) {
            $variant = Variant::create([
                                           "number" => $variantItem->number,
                                           "task_id" => $task->id
                                       ]);

            foreach ($variantItem->tests as $testItem) {
                Test::create(array_merge(
                                 ["variant_id" => $variant->id],
                                 $testItem->toArray()
                             ));
            }
        }
    }

    public static function getBadRequestErrorMessage(): string
    {
        return "Такой задачи не существует";
    }

    public function toEntity(): TaskEntity
    {
        return new TaskEntity(
            $this->id,
            $this->description,
            $this->rating
        );
    }
}
