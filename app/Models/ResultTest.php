<?php

namespace App\Models;

use App\Layer\Implements\ResultTest\ResultTestEntity;
use App\Layer\Types\Status;
use App\Models\Contracts\IModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResultTest extends Model implements IModel
{
    use HasFactory, SoftDeletes;

    protected $table = "result_test";

    protected $fillable = [
          "in",
          "out",
          "score",
          "time",
          "status",
          "history_id",
          "test_out"
    ];

    protected $dates = ['deleted_at'];

    public function history() : HasOne
    {
        return $this->hasOne(History::class);
    }

    public function toEntity(): ResultTestEntity
    {
        return new ResultTestEntity(
            $this->id,
            $this->score,
            $this->in,
            $this->out,
            $this->test_out,
            Status::from($this->status)
        );
    }
}
