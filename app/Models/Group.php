<?php

namespace App\Models;

use App\Layer\Implements\Group\GroupEntity;
use App\Models\Contracts\IModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model implements IModel
{
    use HasFactory, SoftDeletes;

    protected $table = "groups";

    protected $fillable = [
          'id',
          'name'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'groups_users');
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class, 'group_course');
    }

    public static function getBadRequestErrorMessage(): string
    {
        return "Такой группы не существует";
    }

    public function toEntity(): GroupEntity
    {
        return new GroupEntity(
            $this->id,
            $this->name
        );
    }
}
