<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class UserCourseTask extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = "user_course_tasks";

    protected $fillable = [
        'id',
        'user_course_id',
        'task_id',
        'variant_id'
    ];

    public function delete()
    {
        parent::delete();

        $this->history->each(function(History $history) {
            $history->delete();
        });
    }

    public function variant(): HasOne
    {
        return $this->hasOne(Variant::class, 'id', 'variant_id');
    }

    public function task(): HasOne
    {
        return $this->hasOne(Task::class, 'id', 'task_id');
    }

    public function userCourse(): HasOne
    {
        return $this->hasOne(UserCourse::class, 'id', 'user_course_id');
    }

    public function history(): HasMany
    {
        return $this->hasMany(History::class, 'user_course_task_id', 'id');
    }

    public function histories(): HasMany
    {
        return $this->hasMany(History::class, 'user_course_task_id', 'id');
    }

    public static function findByTaskNUser(int $taskId, int $userId): UserCourseTask
    {
        $userCourse = UserCourse::findByCourseNUser(Task::find($taskId)->course->id, $userId);

        $userTask = Cache::remember('user_course_task_' . $userCourse->id . '_' . $taskId, 600, function () use ($taskId, $userCourse) {
            return self::where('user_course_id', $userCourse->id)
                ->where('task_id', $taskId)
                ->first();
        });

        if (!$userTask) throw new \RuntimeException("Задача у пользователя не найдена");

        return $userTask;
    }

    public static function getBadRequestErrorMessage(): string
    {
        return "Такой задачи не существует";
    }
}
