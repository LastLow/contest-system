<?php

namespace App\Models;

use App\Layer\Implements\Course\CourseEntity;
use App\Models\Contracts\IModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model implements IModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'courses';

    protected $fillable = [
        'id',
        'name'
    ];

    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class, 'course_id', 'id');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_course')
            ->whereNull('group_course.deleted_at');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_courses');
    }

    public static function getBadRequestErrorMessage(): string
    {
        return "Такого курса не существует";
    }

    public function toEntity(): CourseEntity
    {
        return new CourseEntity(
            $this->id,
            $this->name,
            $this->tasks->count()
        );
    }
}
