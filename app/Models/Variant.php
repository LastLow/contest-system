<?php

namespace App\Models;

use App\Layer\Implements\Variant\VariantEntity;
use App\Models\Contracts\IModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Variant extends Model implements IModel
{
    use HasFactory, SoftDeletes;

    protected $table = "variants";

    protected $fillable = [
        "id",
        "number",
        "task_id"
    ];

    public function delete()
    {
        parent::delete();
        $this->userTasks->each(function(UserCourseTask $task) {
            $task->delete();
        });
    }

    public function tests(): HasMany
    {
        return $this->hasMany(Test::class);
    }

    public function getMaxScore(): int
    {
        // TODO: Необходимо вынести данную функцию в репозиторий тестов и переписать HistoryService::createCheckCode
        return $this->tests()->sum('score');
    }

    public function task(): HasOne
    {
        return $this->hasOne(Task::class, 'id', 'task_id');
    }

    public function userTasks(): HasMany
    {
        return $this->hasMany(UserCourseTask::class, 'variant_id', 'id');
    }

    public function toEntity(): VariantEntity
    {
        return new VariantEntity(
            $this->id,
            $this->number
        );
    }
}
