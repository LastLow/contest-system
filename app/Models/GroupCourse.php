<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupCourse extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "group_course";

    protected $fillable = [
        'group_id',
        'course_id'
    ];
}
