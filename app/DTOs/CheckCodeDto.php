<?php

namespace App\DTOs;

class CheckCodeDto
{
    public function __construct(
        public int     $taskId,
        public string  $code,
        public ?string $language
    ) {
        $this->language = $language ?? "python";
    }
}
