<?php

namespace App\DTOs;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="CreateTaskDto",
 *     description="CreateTask schema",
 *     @OA\Xml(
 *         name="CreateTaskDto"
 *     )
 * )
 * @OA\Property(property="description", type="string"),
 * @OA\Property(property="rating", type="number"),
 * @OA\Property(property="variants", type="array", @OA\Items(ref="#/components/schemas/Variant"))
 * @OA\Property(property="courseId", type="number"),
 */
class CreateTaskDto
{
    /**
     * @param string $description
     * @param int $rating
     * @param CreateVariantDto[] $variants
     * @param int $courseId
     */
    public function __construct(
        public string $description,
        public int    $rating,
        public array  $variants,
        public int    $courseId
    )
    {
    }
}
