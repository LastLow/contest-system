<?php

namespace App\DTOs;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="LoginDTO",
 *     description="Login schema",
 *     @OA\Xml(
 *         name="LoginDTO"
 *     )
 * )
 *
 * @OA\Property(property="login", type="string"),
 * @OA\Property(property="password", type="string")
 */

class LoginDto
{
    public function __construct(
        public string $login,
        public string $password
    ) {
    }
}
