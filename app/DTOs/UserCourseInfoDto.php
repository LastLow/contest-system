<?php

namespace App\DTOs;

use App\DTOs\UserInfo;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="UserCourseInfoDto",
 *     description="UserCourseInfoDto schema",
 *     @OA\Xml(
 *         name="UserCourseInfoDto"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="name", type="string"),
 * @OA\Property(property="login", type="string")
 * @OA\Property(property="success_count_tasks", type="number"),
 * @OA\Property(property="count_tasks", type="number"),
 * @OA\Property(property="adding_score_tasks", type="number"),
 * @OA\Property(property="score_tasks", type="number"),
 */
class UserCourseInfoDto
{
    public function __construct(
        public int    $id,
        public string $name,
        public string $login,
        public int    $success_count_tasks = 0,
        public int    $count_tasks = 0,
        public int    $adding_score_tasks = 0,
        public int    $score_tasks = 0
    )
    {
    }
}
