<?php

namespace App\DTOs;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="UserDTO",
 *     description="User schema",
 *     @OA\Xml(
 *         name="UserDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="name", type="string"),
 * @OA\Property(property="login", type="string"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string", nullable=true),
 * @OA\Property(property="roles", type="array", nullable=true, @OA\Items(type="string")),
 * @OA\Property(property="permissions", type="array", nullable=true, @OA\Items(type="string"))
 */


class UserDto
{
    public function __construct(
        public int         $id,
        public string      $name,
        public string      $login,
        public string      $created_at,
        public string|null $updated_at,
        public array|null  $roles,
        public array|null  $permissions
    ) {
    }
}
