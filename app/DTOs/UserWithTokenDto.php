<?php

namespace App\DTOs;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="UserDTO",
 *     description="User schema",
 *     @OA\Xml(
 *         name="UserDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="name", type="string"),
 * @OA\Property(property="login", type="string"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string", nullable=true),
 * @OA\Property(property="roles", type="array", nullable=true, @OA\Items(type="string")),
 * @OA\Property(property="permissions", type="array", nullable=true, @OA\Items(type="string")),
 * @OA\Property(property="token", type="string")
 */

class UserWithTokenDto extends UserDto
{
    public function __construct(
        UserDto $dto,
        public string $token
    ) {
        parent::__construct(
            $dto->id,
            $dto->name,
            $dto->login,
            $dto->created_at,
            $dto->updated_at,
            $dto->roles,
            $dto->permissions
        );
    }
}
