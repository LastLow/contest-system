<?php

namespace App\DTOs;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="CreateTestDto",
 *     description="CreateTest schema",
 *     @OA\Xml(
 *         name="CreateTestDto"
 *     )
 * )
 * @OA\Property(property="in", type="string", nullable=true),
 * @OA\Property(property="out", type="string", nullable=true),
 * @OA\Property(property="score", type="number"),
 * @OA\Property(property="time", type="number")
 */

class CreateTestDto
{
    public function __construct(
        public string|null $in,
        public string|null $out,
        public int         $score,
        public int         $time
    )
    {
    }
}
