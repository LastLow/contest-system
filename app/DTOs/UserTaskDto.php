<?php

namespace App\DTOs;

use App\Layer\Types\Status;
use Illuminate\Support\Collection;

class UserTaskDto extends UserTaskInfoDto
{
    public Collection $histories;

    public function __construct(
        int    $id,
        string $description,
        int    $rating,
        int    $variant,
        Status $status,
        int    $score,
        int    $max_score,
        Collection  $histories
    ) {
        parent::__construct($id, $description, $rating, $variant, $status, $score, $max_score);

        $this->histories = $histories;
    }
}
