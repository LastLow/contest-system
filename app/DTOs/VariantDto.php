<?php

namespace App\DTOs;

use App\DTO\BaseSchema;
use App\DTO\Collections\VariantTestCollection;
use App\DTO\VariantTest;
use OpenApi\Annotations as OA;


/**
 * @OA\Schema(
 *     title="VariantDTO",
 *     description="Variant schema",
 *     @OA\Xml(
 *         name="VariantDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="number", type="string"),
 * @OA\Property(property="tests", type="array", @OA\Items(ref="#/components/schemas/VariantTest"))
 */

class VariantDto
{
    /**
     * @param int $id
     * @param int $number
     * @param TestDto[] $tests
     */
    public function __construct(
        public int   $id,
        public int   $number,
        public array $tests,
    )
    {
    }
}
