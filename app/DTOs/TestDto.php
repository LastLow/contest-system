<?php

namespace App\DTOs;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="TestDTO",
 *     description="Test schema",
 *     @OA\Xml(
 *         name="TestDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="in", type="string", nullable=true),
 * @OA\Property(property="out", type="string", nullable=true),
 * @OA\Property(property="score", type="number"),
 * @OA\Property(property="time", type="number")
 */

class TestDto
{
    public function __construct(
        public int    $id,
        public string|null $in,
        public string|null $out,
        public int         $score,
        public int         $time
    )
    {
    }
}
