<?php

namespace App\DTOs;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="TaskDto",
 *     description="TaskDto schema",
 *     @OA\Xml(
 *         name="TaskDto"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="description", type="string"),
 * @OA\Property(property="rating", type="number")
 * @OA\Property(property="variant_count", type="number")
 */
class TaskInfoDto
{
    public function __construct(
        public int    $id,
        public string $description,
        public int    $rating,
        public int    $variant_count
    )
    {
    }
}
