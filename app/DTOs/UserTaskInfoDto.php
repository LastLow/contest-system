<?php

namespace App\DTOs;

use App\Layer\Types\Status;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="UserTaskInfoDTO",
 *     description="UserTaskInfo model",
 *     @OA\Xml(
 *         name="UserTaskInfoDTO"
 *     )
 * )
 *  @OA\Property(property="id", type="number"),
 *  @OA\Property(property="description", type="string"),
 *  @OA\Property(property="rating", type="number"),
 *  @OA\Property(property="variant", type="number"),
 *  @OA\Property(property="status", type="string", enum={"success", "error", "zero"}),
 *  @OA\Property(property="score", type="number"),
 *  @OA\Property(property="max_score", type="number")
 */

class UserTaskInfoDto
{
    public function __construct(
        public int    $id,
        public string $description,
        public int    $rating,
        public int    $variant,
        public Status $status,
        public int    $score,
        public int    $max_score
    ) {
    }
}
