<?php

namespace App\DTOs;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="UpdateTaskDto",
 *     description="UpdateTask schema",
 *     @OA\Xml(
 *         name="UpdateTaskDto"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="description", type="string"),
 * @OA\Property(property="rating", type="number"),
 * @OA\Property(property="variants", type="array", @OA\Items(
 *                     anyOf={
 *                          @OA\Schema(ref="#/components/schemas/VariantDto"),
 *                          @OA\Schema(ref="#/components/schemas/CreateVariantDto")
 *                     }
 *                 ))
 * @OA\Property(property="courseId", type="number"),
 */
class UpdateTaskDto
{
    /**
     * @param int $id
     * @param string $description
     * @param int $rating
     * @param Array<VariantDto|CreateVariantDto> $variants
     * @param int $courseId
     */
    public function __construct(
        public int    $id,
        public string $description,
        public int    $rating,
        public array  $variants,
        public int    $courseId,
    )
    {
    }
}
