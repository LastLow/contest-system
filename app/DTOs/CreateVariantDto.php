<?php

namespace App\DTOs;

use OpenApi\Annotations as OA;


/**
 * @OA\Schema(
 *     title="CreateVariantDto",
 *     description="CreateVariant schema",
 *     @OA\Xml(
 *         name="CreateVariantDto"
 *     )
 * )
 * @OA\Property(property="number", type="string"),
 * @OA\Property(property="tests", type="array", @OA\Items(ref="#/components/schemas/VariantTest"))
 */

class CreateVariantDto
{
    /**
     * @param int $number
     * @param CreateTestDto[] $tests
     */
    public function __construct(
        public int   $number,
        public array $tests,
    )
    {
    }
}
