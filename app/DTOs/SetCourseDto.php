<?php

namespace App\DTOs;

use Illuminate\Support\Collection;

class ItemRating {
    public function __construct(
        public int $rating,
        public int $count,
    ) {}
}

class SetCourseDto
{
    /**
     * @param Collection<ItemRating> $settings
     * @param int $courseId
     */
    public function __construct(
        public Collection $settings,
        public int $courseId,
    ) {}
}
