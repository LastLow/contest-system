<?php

namespace App\DTOs;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="UserInfoDTO",
 *     description="UserInfo schema",
 *     @OA\Xml(
 *         name="UserInfoDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="name", type="string"),
 * @OA\Property(property="login", type="string")
 */
class UserInfo
{
    public function __construct(
        public int    $id,
        public string $name,
        public string $login
    )
    {

    }
}
