<?php

namespace App\DTOs;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="UserCourseDTO",
 *     description="Course model",
 *     @OA\Xml(
 *         name="UserCourseDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="name", type="string"),
 * @OA\Property(property="count", type="number"),
 * @OA\Property(property="success_count", type="number")
 *
 */

class UserCourseDto
{
    public function __construct(
        public int    $id,
        public string $name,
        public int    $count,
        public int    $success_count,
    ) {
    }
}
