<?php

namespace App\DTOs;

class CreateGroupDto
{
    public function __construct(
        public string $name,
        public string $count
    )
    {
    }
}
