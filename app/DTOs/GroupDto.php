<?php

namespace App\DTOs;

class GroupDto
{
    public function __construct(
        public int    $id,
        public string $name,
        public int    $count_users
    )
    {
    }
}
