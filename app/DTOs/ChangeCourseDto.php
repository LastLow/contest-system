<?php

namespace App\DTOs;

class ChangeCourseDto
{
    public function __construct(
        public string $name
    ) {
    }
}
