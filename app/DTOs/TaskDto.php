<?php

namespace App\DTOs;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="TaskDTO",
 *     description="Task schema",
 *     @OA\Xml(
 *         name="TaskDTO"
 *     )
 * )
 * @OA\Property(property="id", type="number"),
 * @OA\Property(property="description", type="string"),
 * @OA\Property(property="rating", type="number"),
 * @OA\Property(property="variants", type="array", @OA\Items(ref="#/components/schemas/Variant"))
 */

class TaskDto
{
    /**
     * @param int $id
     * @param string $description
     * @param int $rating
     * @param VariantDto[] $variants
     */
    public function __construct(
        public int    $id,
        public string $description,
        public int    $rating,
        public array  $variants,
    )
    {
    }
}
