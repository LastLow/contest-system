<?php

namespace App\Layer\Implements\Variant;

class VariantEntity
{
    public function __construct(
        public int $id,
        public int $number
    ) {
    }
}
