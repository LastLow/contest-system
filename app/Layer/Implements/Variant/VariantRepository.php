<?php

namespace App\Layer\Implements\Variant;

use App\DTOs\CreateVariantDto;
use App\DTOs\VariantDto;
use App\Layer\Interfaces\Repositories\IVariantRepository;
use App\Layer\Repository;
use App\Models\Task;
use App\Models\UserCourseTask;
use App\Models\Variant;
use Illuminate\Support\Collection;

class VariantRepository extends Repository implements IVariantRepository
{
    public function getVariantsByTaskId(int $taskId): Collection
    {
        return Task::find($taskId)->variants()->get()
            ->map(fn (Variant $variant) => $variant->toEntity());
    }
    public function getVariantByTaskNUser(int $taskId, int $userId): VariantEntity
    {
        return UserCourseTask::findByTaskNUser($taskId, $userId)->variant->toEntity();
    }

    public function getVariantCountByTask(int $taskId): int
    {
        return Task::find($taskId)->variants()->count();
    }

    public function getMaxScoreById(int $variantId): int
    {
        return Variant::find($variantId)->getMaxScore();
    }

    public function updateOrCreate(int $taskId, CreateVariantDto|VariantDto $variant): VariantEntity
    {
        if ($variant instanceof CreateVariantDto) {
            $variantModel = Variant::create([
                'number' => $variant->number,
                'task_id' => $taskId
            ]);
        } else {
            $variantModel = Variant::update([
                'id' => $variant->id,
                'number' => $variant->number,
                'task_id' => $taskId
            ]);
        }

        return $variantModel->toEntity();
    }

    protected function getModel(): string
    {
        return Variant::class;
    }
}
