<?php

namespace App\Layer\Implements\Variant;

use App\DTOs\CreateVariantDto;
use App\DTOs\VariantDto;
use App\Layer\Interfaces\Repositories\IVariantRepository;
use App\Layer\Interfaces\Services\ITestService;
use App\Layer\Interfaces\Services\IVariantService;
use Illuminate\Support\Collection;

class VariantService implements IVariantService
{
    public function __construct(
        readonly private IVariantRepository $repository,
        readonly private ITestService $testService
    ) {
    }

    /**
     * @param int $taskId
     * @return Collection<VariantDto>
     */
    public function getVariantsByTaskId(int $taskId): Collection
    {
        return $this->repository->getVariantsByTaskId($taskId)->map(function (VariantEntity $variant) {
            return new VariantDto(
                $variant->id,
                $variant->number,
                $this->testService->getTestsByVariantId($variant->id)->toArray()
            );
        });
    }

    public function getVariantCountByTask(int $taskId): int
    {
        return $this->repository->getVariantCountByTask($taskId);
    }

    public function getVariantByTaskNUser(int $taskId, int $userId): VariantEntity
    {
        return $this->repository->getVariantByTaskNUser($taskId, $userId);
    }

    public function getMaxScoreById(int $variantId): int
    {
        return $this->repository->getMaxScoreById($variantId);
    }

    public function updateOrCreate(int $taskId, CreateVariantDto|VariantDto $variantDto): VariantEntity
    {
        return $this->repository->updateOrCreate($taskId, $variantDto);
    }

    public function deleteByTaskId(int $taskId): void
    {
        try {
            $this->repository->beginTransaction();

            $this->repository->getVariantsByTaskId($taskId)->each(function (VariantEntity $variant) {
                $this->testService->deleteByVariantId($variant->id);

                $this->repository->delete($variant->id);
            });

            $this->repository->commit();
        } catch (\Exception $e) {
            $this->repository->rollBack();

            throw $e;
        }
    }
}
