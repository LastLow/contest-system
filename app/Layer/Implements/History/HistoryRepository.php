<?php

namespace App\Layer\Implements\History;

use App\Layer\Interfaces\Repositories\IHistoryRepository;
use App\Layer\Repository;
use App\Layer\Types\CreateHistory;
use App\Models\History;
use App\Models\UserCourseTask;
use Illuminate\Support\Collection;

class HistoryRepository extends Repository implements IHistoryRepository
{
    public function getLastHistoryByTaskNUser(int $taskId, int $userId): ?HistoryEntity
    {
        return UserCourseTask::findByTaskNUser($taskId, $userId)
            ->histories()->latest()->first()?->toEntity();
    }

    public function getHistoriesByTaskNUser(int $taskId, int $userId): Collection
    {
        return UserCourseTask::findByTaskNUser($taskId, $userId)
            ->histories()->latest()->get()->map(fn (History $history) => $history->toEntity());
    }

    public function create(CreateHistory $createHistory): void
    {
        $userTaskId = UserCourseTask::findByTaskNUser($createHistory->taskId, $createHistory->userId)->id;

        History::create(
            [
                "code" => $createHistory->code,
                "language" => $createHistory->language,
                "max_score" => $createHistory->max_score,
                "user_course_task_id" => $userTaskId
            ]
        );
    }

    protected function getModel(): string
    {
        return History::class;
    }
}
