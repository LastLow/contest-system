<?php

namespace App\Layer\Implements\History;

use App\Layer\Types\Status;

class HistoryEntity
{
    public function __construct(
        public int    $id,
        public string $code,
        public Status $status,
        public int    $score,
        public int    $max_score
    ) {
    }
}
