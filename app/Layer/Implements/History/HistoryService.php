<?php

namespace App\Layer\Implements\History;

use App\DTOs\CheckCodeDto;
use App\Layer\Interfaces\Repositories\IHistoryRepository;
use App\Layer\Interfaces\Services\IHistoryService;
use App\Layer\Interfaces\Services\IResultTestService;
use App\Layer\Interfaces\Services\IVariantService;
use App\Layer\Types\CreateHistory;
use App\Layer\Types\HistoryWithResultTests;
use App\Models\User;
use Illuminate\Support\Collection;

class HistoryService implements IHistoryService
{
    public function __construct(
        readonly private IHistoryRepository $repository,
        readonly private IResultTestService $resultTestService,
        readonly private IVariantService $variantService
    ) {
    }

    public function getLastHistoryByTaskNUser(int $taskId, int $userId): ?HistoryEntity
    {
        return $this->repository->getLastHistoryByTaskNUser($taskId, $userId);
    }

    public function getHistoriesWithResultTestByTaskNUser(int $taskId, int $userId): Collection
    {
        $histories = $this->repository->getHistoriesByTaskNUser($taskId, $userId);

        return $histories->map(function(HistoryEntity $entity) {
            return new HistoryWithResultTests(
                $entity,
                $this->resultTestService->getTestsByHistoryId($entity->id)
            );
        });
    }

    public function createCheckCode(CheckCodeDto $checkCodeDto, int $userId): void
    {
        $variant = $this->variantService->getVariantByTaskNUser($checkCodeDto->taskId, $userId);

        $this->repository->create(
            new CreateHistory(
                $checkCodeDto->language,
                $checkCodeDto->code,
                $this->variantService->getMaxScoreById($variant->id),
                $checkCodeDto->taskId,
                $userId
            )
        );
    }
}
