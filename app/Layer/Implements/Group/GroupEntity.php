<?php

namespace App\Layer\Implements\Group;

class GroupEntity
{
    public function __construct(
        public int    $id,
        public string $name
    )
    {
    }
}
