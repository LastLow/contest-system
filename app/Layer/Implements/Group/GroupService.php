<?php

namespace App\Layer\Implements\Group;

use App\DTOs\CreateGroupDto;
use App\DTOs\GroupDto;
use App\DTOs\ItemRating;
use App\DTOs\SetCourseDto;
use App\DTOs\TaskInfoDto;
use App\DTOs\UserInfo;
use App\Layer\Interfaces\Repositories\IAuthService;
use App\Layer\Interfaces\Repositories\IGroupRepository;
use App\Layer\Interfaces\Services\ICourseService;
use App\Layer\Interfaces\Services\IGroupService;
use App\Layer\Interfaces\Services\ITaskService;
use App\Layer\Interfaces\Services\IUserService;
use App\Layer\Types\CreateUser;
use App\Layer\Types\GroupFilter;
use App\Layer\Types\PaginateData;
use Illuminate\Support\Str;

class GroupService implements IGroupService
{
    public function __construct(
        protected readonly IGroupRepository $repository,
        protected readonly IUserService     $userService,
        protected readonly ITaskService     $taskService,
        protected readonly ICourseService   $courseService,
        protected readonly IAuthService     $authService
    )
    {
    }

    public function get(GroupFilter $filter): PaginateData
    {
        return $this->repository->getGroups($filter);
    }

    public function create(CreateGroupDto $dto): GroupDto
    {
        try {
            $this->repository->beginTransaction();

            $group = $this->repository->create($dto);

            for ($i = 0; $i < $dto->count; $i++) {
                $name = Str::upper(Str::slug($dto->name, '_')) . "_user" . ($i + 1);
                $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()';
                $password = collect(str_split($characters))->random(8)->implode('');

                $user = $this->userService->create(new CreateUser(
                    $name,
                    $name,
                    $password,
                    ['student']
                ));

                $this->repository->addUserInGroup($group->id, $user->id);
            }

            $this->repository->commit();

            return new GroupDto(
                $group->id,
                $group->name,
                $dto->count
            );
        } catch (\Throwable $throwable) {
            $this->repository->rollback();
            throw $throwable;
        }
    }

    public function setCourseToGroup(SetCourseDto $dto, int $groupId): void
    {
        try {
            $this->repository->beginTransaction();
            $this->repository->addCourseInGroup($groupId, $dto->courseId);

            $tasks = $this->taskService->getTasksByCourseId($dto->courseId)
                ->filter(fn(TaskInfoDto $task) => $task->variant_count !== 0);

            $this->userService->getUsersByGroup($groupId)->each(function (UserInfo $user) use ($dto, $tasks) {
                $this->courseService->setCourseInUser($dto->courseId, $user->id, $this->authService->getAuthorizeUserId());

                $dto->settings->each(function (ItemRating $setting) use ($user, $tasks, $dto) {
                    $selectedTasks = $tasks
                        ->filter(fn(TaskInfoDto $task) => $task->rating === $setting->rating);

                    if ($selectedTasks->isEmpty()) return;

                    if ($selectedTasks->count() > $setting->count)
                        $selectedTasks = $selectedTasks->random($setting->count);

                    if ($selectedTasks instanceof TaskInfoDto) {
                        $this->taskService->setTaskInUser($selectedTasks->id, $user->id, $dto->courseId);
                    } else {
                        $selectedTasks->each(function (TaskInfoDto $task) use ($user, $dto, $setting) {
                            $this->taskService->setTaskInUser($task->id, $user->id, $dto->courseId);
                        });
                    }
                });
            });

            $this->repository->commit();
        } catch (\Throwable $throwable) {
            $this->repository->rollback();
            throw $throwable;
        }
    }

    public function deleteGroup(int $id): void
    {
        $this->repository->delete($id);
    }

    public function deleteCourseFromGroup(int $groupId, int $courseId): void
    {
        try {
            $this->repository->beginTransaction();
            $this->repository->deleteCourseInGroup($groupId, $courseId);

            $this->userService->getUsersByGroup($groupId)->each(function (UserInfo $user) use ($courseId) {
                $this->taskService->deleteTasksInCourseByUser($user->id, $courseId);
                $this->courseService->deleteCourseInUser($courseId, $user->id);
            });

            $this->repository->commit();
        } catch (\Throwable $throwable) {
            $this->repository->rollback();
            throw $throwable;
        }
    }
}
