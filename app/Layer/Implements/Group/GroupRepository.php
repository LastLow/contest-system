<?php

namespace App\Layer\Implements\Group;

use App\DTOs\CreateGroupDto;
use App\Exceptions\HandleException;
use App\Layer\Interfaces\Repositories\IGroupRepository;
use App\Layer\Repository;
use App\Layer\Types\GroupFilter;
use App\Layer\Types\GroupFilterType;
use App\Layer\Types\PaginateData;
use App\Models\Group;
use App\Models\GroupCourse;
use App\Models\GroupUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class GroupRepository extends Repository implements IGroupRepository
{
    public function getGroups(GroupFilter $filter): PaginateData
    {
        $query = $this->model::query();

        if ($filter->courseId) {
            $query->join('group_course', 'group_course.group_id', '=', 'groups.id');

            if ($filter->type === GroupFilterType::IN) {
                $query->where("group_course.course_id", $filter->courseId);
            } else {
                $query->whereNot("group_course.course_id", $filter->courseId);
            }
        }

        return $this->get($query, $filter->page, $filter->perPage);
    }

    public function create(CreateGroupDto $dto): GroupEntity
    {
        if ($this->findByName($dto->name)) {
            throw new HandleException("Группа с таким названием уже существует");
        }

        return Group::create(['name' => $dto->name])->toEntity();
    }

    public function findByName(string $name): GroupEntity|null
    {
        $group = Group::whereRaw("LOWER(name) = ?", Str::lower($name))->first();
        if (!$group) {
            return null;
        }

        return $group;
    }

    public function addCourseInGroup(int $groupId, int $courseId): void
    {
        if (GroupCourse::where("group_id", $groupId)->where("course_id", $courseId)->first())
            throw new HandleException("Этот курс уже назначен данной группе");

        GroupCourse::create(["group_id" => $groupId, "course_id" => $courseId]);
    }

    public function deleteCourseInGroup(int $groupId, int $courseId): void
    {
        GroupCourse::where("group_id", $groupId)->where("course_id", $courseId)->delete();
    }

    public function addUserInGroup(int $groupId, int $userId): void
    {
        GroupUser::create(["user_id" => $userId, "group_id" => $groupId]);
    }

    public function delete(int $id): void
    {
        DB::beginTransaction();

        try {
            $group = $this->getModelById($id);

            // TODO: Переместить логику удаления пользователей в UserService
            $group->users()->forceDelete();

            $group->forceDelete();

            DB::commit();
        } catch (\Throwable $exception)
        {
            DB::rollBack();
            throw $exception;
        }
    }

    protected function getModel(): string
    {
        return Group::class;
    }
}
