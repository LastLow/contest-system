<?php

namespace App\Layer\Implements\Auth;

use App\DTOs\LoginDto;
use App\Layer\Interfaces\Repositories\IAuthRepository;
use Illuminate\Support\Facades\Auth;

class AuthRepository implements IAuthRepository
{
    public function login(LoginDto $loginDto): string
    {
        if (!Auth::attempt(['login' => $loginDto->login, 'password' => $loginDto->password])) {
            throw new \RuntimeException('Неправильные данные');
        }

        return Auth::user()->createToken('token')->plainTextToken;
    }

    public function getAuthorizeUserId(): int
    {
        return Auth::user()->getAuthIdentifier();
    }

    public function logout(): void
    {
        Auth::user()->currentAccessToken()->delete();
    }
}
