<?php

namespace App\Layer\Implements\Auth;

use App\DTOs\LoginDto;
use App\DTOs\UserWithTokenDto;
use App\Layer\Interfaces\Repositories\IAuthRepository;
use App\Layer\Interfaces\Repositories\IAuthService;
use App\Layer\Interfaces\Services\IUserService;

class AuthService implements IAuthService
{
    public function __construct(
        readonly private IAuthRepository $repository,
        readonly private IUserService $userService
    ) {
    }

    public function getAuthorizeUserId(): int
    {
        return $this->repository->getAuthorizeUserId();
    }

    public function login(LoginDto $loginDto): UserWithTokenDto
    {
        $token = $this->repository->login($loginDto);

        return new UserWithTokenDto(
            $this->userService->getInfoUserById($this->repository->getAuthorizeUserId()),
            $token
        );
    }

    public function logout(): void
    {
        $this->repository->logout();
    }
}
