<?php

namespace App\Layer\Implements\Task;

class TaskEntity
{
    public function __construct(
        public int    $id,
        public string $description,
        public int    $rating
    ) {
    }
}
