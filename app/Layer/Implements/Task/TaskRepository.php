<?php

namespace App\Layer\Implements\Task;

use App\DTOs\CreateTaskDto;
use App\DTOs\UpdateTaskDto;
use App\Exceptions\HandleException;
use App\Layer\Interfaces\Repositories\ITaskRepository;
use App\Layer\Repository;
use App\Models\Course;
use App\Models\Task;
use App\Models\UserCourse;
use App\Models\UserCourseTask;
use Illuminate\Support\Collection;

class TaskRepository extends Repository implements ITaskRepository
{
    /**
     * @param int $courseId
     * @param int $userId
     * @return Collection<TaskEntity>
     */
    public function getTasksInCourseByUser(int $courseId, int $userId): Collection
    {
        return UserCourse::findByCourseNUser($courseId, $userId)
            ->userTasks->map(fn (UserCourseTask $userTask) => $userTask->task->toEntity());
    }

    public function getCountTasksInCourseByUser(int $courseId, int $userId): int
    {
        return UserCourse::findByCourseNUser($courseId, $userId)
            ->userTasks()->count();
    }

    public function getSuccessTasksInCourseByUser(int $courseId, int $userId): int
    {
        return UserCourse::findByCourseNUser($courseId, $userId)
            ->userTasks()
            ->whereHas('history', function($query) {
                $query->where('status', 'success');
            })
            ->count();
    }

    public function findTaskByUser(int $taskId, int $userId): TaskEntity
    {
        return UserCourseTask::findByTaskNUser($taskId, $userId)
            ->task->toEntity();
    }

    public function updateOrCreate(CreateTaskDto|UpdateTaskDto $taskDto): TaskEntity
    {
        if ($taskDto instanceof CreateTaskDto) {
            $task = Task::create([
                'description' => $taskDto->description,
                'rating' => $taskDto->rating,
                'course_id' => $taskDto->courseId
            ]);
        } else {
            $task = Task::update([
                'id' => $taskDto->id,
                'description' => $taskDto->description,
                'rating' => $taskDto->rating,
                'course_id' => $taskDto->courseId
            ]);
        }

        return $task->toEntity();
    }

    public function setTaskInUser(int $taskId, int $userId, int $courseId, int $variantId): void
    {
        $userCourse = UserCourse::where('user_id', $userId)->where('course_id', $courseId)->first();

        if (!$userCourse) throw new HandleException("Указанного курса у пользователя нет");

        if (UserCourseTask::where("task_id", $taskId)->where("user_course_id", $userCourse->id)->exists()) return;

        UserCourseTask::create([
            "user_course_id" => $userCourse->id,
            "task_id" => $taskId,
            "variant_id" => $variantId
        ]);
    }

    public function deleteTasksInCourseByUser(int $userId, int $courseId): void
    {
        $userCourse = UserCourse::where('user_id', $userId)->where('course_id', $courseId)->first();
        if (!$userCourse) return;

        UserCourseTask::where('user_course_id', $userCourse->id)->delete();
    }

    public function getTasksByCourseId(int $courseId): Collection
    {
        return Course::find($courseId)->tasks()->get()->map(fn (Task $task) => $task->toEntity());
    }

    protected function getModel(): string
    {
        return Task::class;
    }
}
