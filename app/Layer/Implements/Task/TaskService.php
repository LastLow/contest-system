<?php

namespace App\Layer\Implements\Task;

use App\DTOs\CreateTaskDto;
use App\DTOs\CreateTestDto;
use App\DTOs\CreateVariantDto;
use App\DTOs\TaskDto;
use App\DTOs\TaskInfoDto;
use App\DTOs\TestDto;
use App\DTOs\UpdateTaskDto;
use App\DTOs\UserTaskDto;
use App\DTOs\UserTaskInfoDto;
use App\DTOs\VariantDto;
use App\Exceptions\HandleException;
use App\Layer\Interfaces\Repositories\ITaskRepository;
use App\Layer\Interfaces\Services\IHistoryService;
use App\Layer\Interfaces\Services\ITaskService;
use App\Layer\Interfaces\Services\ITestService;
use App\Layer\Interfaces\Services\IVariantService;
use App\Layer\Types\Status;
use Illuminate\Support\Collection;

class TaskService implements ITaskService
{
    public function __construct(
        protected readonly ITaskRepository $repository,
        protected readonly IVariantService $variantService,
        protected readonly IHistoryService $historyService,
        protected readonly ITestService $testService
    ) {
    }

    public function getTask(int $id): TaskDto
    {
        $task = $this->repository->find($id);

        return new TaskDto(
            $task->id,
            $task->description,
            $task->rating,
            $this->variantService->getVariantsByTaskId($task->id)->toArray()
        );
    }

    public function getTasksByCourseId(int $courseId): Collection
    {
        return $this->repository->getTasksByCourseId($courseId)->map(function (TaskEntity $task) {
            return new TaskInfoDto(
                $task->id,
                $task->description,
                $task->rating,
                $this->variantService->getVariantCountByTask($task->id)
            );
        });
    }

    public function setTaskInUser(int $taskId, int $userId, int $courseId): void
    {
        $variants = $this->variantService->getVariantsByTaskId($taskId);

        if ($variants->isEmpty())
            throw new HandleException("Для задачи с id $taskId нет вариантов для добавления ее пользователю");

        $this->repository->setTaskInUser(
            $taskId,
            $userId,
            $courseId,
            $variants->random()->id
        );
    }

    public function deleteTasksInCourseByUser(int $userId, int $courseId): void
    {
        $this->repository->deleteTasksInCourseByUser($userId, $courseId);
    }

    /**
     * @param int $courseId
     * @param int $userId
     * @return Collection<UserTaskInfoDto>
     */
    public function getTasksInCourseByUser(int $courseId, int $userId): Collection
    {
        $tasks = $this->repository->getTasksInCourseByUser($courseId, $userId);

        return $tasks->map(function(TaskEntity $task) use ($userId) {
            $resultHistory = $this->historyService->getLastHistoryByTaskNUser($task->id, $userId);
            $variant = $this->variantService->getVariantByTaskNUser($task->id, $userId);

            $maxScore = $resultHistory?->maxScore ?? $this->variantService->getMaxScoreById($variant->id);

            return new UserTaskInfoDto(
                $task->id,
                $task->description,
                $task->rating,
                $variant->number,
                $resultHistory?->status ?? Status::Zero,
                $resultHistory?->score ?? 0,
                $maxScore
            );
        });
    }

    public function getTaskByUser(int $taskId, int $userId): UserTaskDto
    {
        $task = $this->repository->findTaskByUser($taskId, $userId);

        $variant = $this->variantService->getVariantByTaskNUser($task->id, $userId);
        $histories = $this->historyService->getHistoriesWithResultTestByTaskNUser($task->id, $userId);

        return new UserTaskDto(
            $task->id,
            $task->description,
            $task->rating,
            $variant->number,
            $histories->first()?->status ?? Status::Zero,
            $histories->first()?->score ?? 0,
            $histories->first()?->maxScore ?? $this->variantService->getMaxScoreById($variant->id),
            $histories
        );
    }

    public function getCountTasksInCourseByUser(int $courseId, int $userId): int
    {
        return $this->repository->getCountTasksInCourseByUser($courseId, $userId);
    }

    public function getSuccessTasksInCourseByUser(int $courseId, int $userId): int
    {
        return $this->repository->getSuccessTasksInCourseByUser($courseId, $userId);
    }

    public function updateOrCreateTask(CreateTaskDto|UpdateTaskDto $updateTaskDto): TaskDto
    {
        try {
            $this->repository->beginTransaction();

            $variants = collect($updateTaskDto->variants)->map(
                function (CreateVariantDto|VariantDto $variantDto) use ($updateTaskDto) {
                    $variant = $this->variantService->updateOrCreate($updateTaskDto->id, $variantDto);

                    $tests = collect($variantDto->tests)->map(
                        function (CreateTestDto|TestDto $test) use ($variant) {
                            $changedTest = $this->testService->updateOrCreate($variant->id, $test);

                            return new TestDto(
                                $changedTest->id,
                                $changedTest->in,
                                $changedTest->out,
                                $changedTest->score,
                                $changedTest->time
                            );
                        }
                    )->toArray();

                    return new VariantDto($variant->id, $variant->number, $tests);
                }
            )->toArray();

            $task = $this->repository->updateOrCreate($updateTaskDto);

            $this->repository->commit();

            return new TaskDto(
                $task->id,
                $task->description,
                $task->rating,
                $variants
            );
        } catch (\Throwable $throwable) {
            $this->repository->rollBack();
            throw new HandleException("Что-то не так при обновлении задачи");
        }
    }

    public function deleteByCourseId(int $courseId): void
    {
        try {
            $this->repository->beginTransaction();

            $this->repository->getTasksByCourseId($courseId)->each(function (TaskEntity $taskEntity) {
                $this->delete($taskEntity->id);
            });

            $this->repository->commit();
        } catch (\Exception $e) {
            $this->repository->rollBack();

            throw $e;
        }
    }

    public function delete(int $id): void
    {
        try {
            $this->repository->beginTransaction();

            $this->variantService->deleteByTaskId($id);

            $this->repository->delete($id);

            $this->repository->commit();
        } catch (\Exception $e) {
            $this->repository->rollBack();

            throw $e;
        }
    }
}
