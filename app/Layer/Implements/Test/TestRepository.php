<?php

namespace App\Layer\Implements\Test;

use App\DTOs\CreateTestDto;
use App\DTOs\TestDto;
use App\Layer\Interfaces\Repositories\ITestRepository;
use App\Layer\Repository;
use App\Models\Test;
use App\Models\Variant;
use Illuminate\Support\Collection;

class TestRepository extends Repository implements ITestRepository
{
    protected function getModel(): string
    {
        return Test::class;
    }

    public function getTestsByVariantId($variantId): Collection
    {
        return Variant::find($variantId)->tests()->get()->map(fn (Test $test) => $test->toEntity());
    }

    public function updateOrCreate(int $variantId, CreateTestDto|TestDto $test): TestEntity
    {
        if ($test instanceof CreateTestDto) {
            $testModel = Test::create([
                'in' => $test->in,
                'out' => $test->out,
                'score' => $test->score,
                'time' => $test->time,
                'variant_id' => $variantId
            ]);
        } else {
            $testModel = Test::update([
                'id' => $test->id,
                'in' => $test->in,
                'out' => $test->out,
                'score' => $test->score,
                'time' => $test->time,
                'variant_id' => $variantId
            ]);
        }

        return $testModel->toEntity();
    }

    public function deleteByVariantId(int $variantId): void
    {
        $this->getModel()::where('variant_id', $variantId)->delete();
    }
}
