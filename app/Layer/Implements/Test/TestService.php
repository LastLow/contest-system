<?php

namespace App\Layer\Implements\Test;

use App\DTOs\CreateTestDto;
use App\DTOs\TestDto;
use App\Layer\Interfaces\Repositories\ITestRepository;
use App\Layer\Interfaces\Services\ITestService;
use Illuminate\Support\Collection;

class TestService implements ITestService
{
    public function __construct(
        private readonly ITestRepository $repository
    )
    {
    }

    public function getTestsByVariantId($variantId): Collection
    {
        return $this->repository->getTestsByVariantId($variantId)
            ->map(fn (TestEntity $test) => new TestDto(
                $test->id,
                $test->in,
                $test->out,
                $test->score,
                $test->time
            ));
    }

    public function updateOrCreate(int $variantId, CreateTestDto|TestDto $test): TestEntity
    {
        return $this->repository->updateOrCreate($variantId, $test);
    }

    public function deleteByVariantId(int $variantId): void
    {
        $this->repository->deleteByVariantId($variantId);
    }
}
