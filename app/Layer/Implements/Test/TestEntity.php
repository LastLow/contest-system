<?php

namespace App\Layer\Implements\Test;

class TestEntity
{
    public function __construct(
        public int     $id,
        public string  $in,
        public string  $out,
        public string  $score,
        public ?string $time
    )
    {
    }
}
