<?php

namespace App\Layer\Implements\ResultTest;

use App\Layer\Interfaces\Repositories\IResultTestRepository;
use App\Layer\Repository;
use App\Models\History;
use App\Models\ResultTest;
use Illuminate\Support\Collection;

class ResultTestRepository extends Repository implements IResultTestRepository
{

    protected function getModel(): string
    {
        return ResultTest::class;
    }

    public function getTestsByHistoryId(int $historyId): Collection
    {
        return History::find($historyId)->resultTests
            ->map(fn(ResultTest $test) => $test->toEntity());
    }
}
