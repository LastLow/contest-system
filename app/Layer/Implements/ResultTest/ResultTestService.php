<?php

namespace App\Layer\Implements\ResultTest;

use App\Layer\Interfaces\Repositories\IResultTestRepository;
use App\Layer\Interfaces\Services\IResultTestService;
use Illuminate\Support\Collection;

class ResultTestService implements IResultTestService
{

    public function __construct(
        readonly private IResultTestRepository $repository
    )
    {
    }

    public function getTestsByHistoryId(int $historyId): Collection
    {
        return $this->repository->getTestsByHistoryId($historyId);
    }
}
