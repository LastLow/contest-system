<?php

namespace App\Layer\Implements\ResultTest;

use App\Layer\Types\Status;

class ResultTestEntity
{
    public function __construct(
        public int     $id,
        public int     $score,
        public ?string $test_in,
        public ?string $test_out,
        public ?string $out,
        public Status  $status
    ) {
    }
}
