<?php

namespace App\Layer\Implements\User;

class UserEntity
{
    public function __construct(
        public int $id,
        public string $login,
        public string $name,
        public string $password,
        public ?string $created_at,
        public ?string $updated_at
    )
    {}
}
