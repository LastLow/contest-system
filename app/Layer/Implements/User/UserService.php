<?php

namespace App\Layer\Implements\User;

use App\DTOs\UserCourseInfoDto;
use App\DTOs\UserDto;
use App\DTOs\UserInfo;
use App\DTOs\UserTaskInfoDto;
use App\Layer\Interfaces\Repositories\IUserRepository;
use App\Layer\Interfaces\Services\ITaskService;
use App\Layer\Interfaces\Services\IUserService;
use App\Layer\Types\CreateUser;
use App\Layer\Types\Status;
use Illuminate\Support\Collection;

class UserService implements IUserService
{
    public function __construct(
        readonly private IUserRepository $repository,
        readonly private ITaskService $taskService
    ) {
    }

    /**
     * @return Collection<UserDto>
     */
    public function getUsers(): Collection
    {
        return $this->repository->get()
            ->map(fn(UserEntity $entity) => $this->getInfoUserById($entity->id));
    }

    public function getInfoAboutCourse(UserInfo $user, int $courseId): UserCourseInfoDto
    {
        $data = new UserCourseInfoDto($user->id, $user->name, $user->login);

        $tasks = $this->taskService->getTasksInCourseByUser($courseId, $user->id);
        $tasks->each(function (UserTaskInfoDto $task) use ($data, $user) {
            $data->adding_score_tasks += $task->score;
            if ($task->status == Status::Success) {
                $data->success_count_tasks++;
            }

            $data->score_tasks += $task->max_score;
        });

        return $data;
    }
    public function create(CreateUser $createUser): UserEntity
    {
        return $this->repository->create($createUser);
    }

    /**
     * @return Collection<UserInfo>
     */
    public function getUsersByGroup(int $groupId): Collection
    {
        return $this->repository->getUsersByGroup($groupId)
            ->map(fn(UserEntity $entity) => new UserInfo($entity->id, $entity->name, $entity->login));
    }

    public function getInfoUserById(int $userId): UserDto
    {
        $user = $this->repository->find($userId);

        return new UserDto(
            $user->id,
            $user->name,
            $user->login,
            $user->created_at,
            $user->updated_at,
            $this->repository->getRolesUserById($userId),
            $this->repository->getPermissionsUserById($userId)
        );
    }
}
