<?php

namespace App\Layer\Implements\User;

use App\Layer\Interfaces\Repositories\IUserRepository;
use App\Layer\Repository;
use App\Layer\Types\CreateUser;
use App\Layer\Types\Permission;
use App\Models\Group;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserRepository extends Repository implements IUserRepository
{
    public function find(int $id): ?UserEntity
    {
        return $this->getModelById($id)?->toEntity();
    }

    public function getUsersByGroup(int $groupId): Collection
    {
        return Group::find($groupId)->users()->get()->map(fn (User $user) => $user->toEntity());
    }

    public function create(CreateUser $createUser): UserEntity
    {
        $user = User::create([
            "name" => $createUser->name,
            "login" => $createUser->login,
            "password" => Hash::make($createUser->password),
            "open_password" => $createUser->password
        ]);

        if ($createUser->roles) {
            collect($createUser->roles)->each(function ($role) use ($user) {
                $role = Role::where('name', '=', $role)->first();
                DB::insert('insert into users_roles (role_id, user_id) values (?,?)', [$role->id, $user->id]);
            });
        }

        if ($createUser->permissions) {
            collect($createUser->permissions)->each(function ($permission) use ($user) {
                $permission = Permission::where('name', '=', $permission)->first();
                DB::insert(
                    'insert into users_permissions (permission_id, user_id) values (?,?)',
                    [$permission->id, $user->id]
                );
            });
        }

        return $user->toEntity();
    }

    public function getRolesUserById(int $userId): ?array
    {
        // TODO: Сделать возврат просто ролей, без отделения только имени
        // TODO: Добавить Тип данных Role - там id, name, slug
        return $this->getModelById($userId)?->roles
            ->pluck('name')->toArray();
    }

    public function getPermissionsUserById(int $userId): ?array
    {
        // TODO: Сделать возврат просто прав, без отделения только имени
        // TODO: Добавить Тип данных Role - там id, name, slug
        return $this->getModelById($userId)
            ?->getAllPermissionsInUser()->pluck('name')->toArray();
    }

    protected function getModel(): string
    {
        return User::class;
    }
}
