<?php

namespace App\Layer\Implements\Course;

use App\Exceptions\HandleException;
use App\Layer\Interfaces\Repositories\ICourseRepository;
use App\Layer\Repository;
use App\Layer\Types\CreateCourse;
use App\Models\Course;
use App\Models\UserCourse;
use Illuminate\Support\Collection;

class CourseRepository extends Repository implements ICourseRepository
{
    public function create(CreateCourse $createCourse): CourseEntity
    {
        $course = $this->getModel()::create(['name' => $createCourse->name]);
        return $course->toEntity();
    }

    public function getCoursesByUserId(int $id): Collection
    {
        return UserCourse::where('user_id', '=', $id)->get()
            ->map(fn(UserCourse $userCourse) => $userCourse->course->toEntity());
    }

    public function update(int $id, CreateCourse $createCourse): CourseEntity
    {
        $course = $this->getModelById($id);
        $course->update(['name' => $createCourse->name]);
        $course->save();

        return $this->getModelById($id)->toEntity();
    }

    public function setCourseInUser(int $courseId, int $userId, int $tutorId = null): void
    {
        $userCourse = UserCourse::where('user_id', $userId)->where('course_id', $courseId)->first();
        if ($userCourse)
            throw new HandleException("Данный курс уже назначен группе");

        UserCourse::create(['course_id' => $courseId, 'user_id' => $userId, 'tutor_id' => $tutorId]);
    }

    public function deleteCourseInUser(int $courseId, int $userId): void
    {
        $userCourse = UserCourse::where('user_id', $userId)->where('course_id', $courseId)->first();
        if (!$userCourse) return;

        $userCourse->delete();
    }

    protected function getModel(): string
    {
        return Course::class;
    }
}
