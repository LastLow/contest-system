<?php

namespace App\Layer\Implements\Course;

class CourseEntity
{
    public function __construct(
        public int $id,
        public string $name,
        public int $count,
    ){}
}
