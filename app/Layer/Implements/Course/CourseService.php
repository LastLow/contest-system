<?php

namespace App\Layer\Implements\Course;

use App\DTOs\ChangeCourseDto;
use App\DTOs\UserCourseDto;
use App\Layer\Interfaces\Repositories\ICourseRepository;
use App\Layer\Interfaces\Services\ICourseService;
use App\Layer\Interfaces\Services\ITaskService;
use App\Layer\Types\CreateCourse;
use Illuminate\Support\Collection;

class CourseService implements ICourseService
{
    public function __construct(
        protected readonly ICourseRepository $repository,
        protected readonly ITaskService $taskService
    ) {}

    public function getCourses(): Collection
    {
        return $this->repository->get();
    }

    /**
     * @param int $userId
     * @return Collection<UserCourseDto>
     */
    public function getCoursesByUser(int $userId): Collection
    {
        return $this->repository->getCoursesByUserId($userId)
            ->map(fn (CourseEntity $course) => new UserCourseDto(
                $course->id,
                $course->name,
                $this->taskService->getCountTasksInCourseByUser($course->id, $userId),
                $this->taskService->getSuccessTasksInCourseByUser($course->id, $userId)
            ));
    }

    public function create(ChangeCourseDto $dto): CourseEntity
    {
        return $this->repository->create(new CreateCourse($dto->name));
    }

    public function update(int $courseId, ChangeCourseDto $dto): CourseEntity
    {
        return $this->repository->update($courseId, new CreateCourse($dto->name));
    }

    public function setCourseInUser(int $courseId, int $userId, int $tutorId = null): void
    {
        $this->repository->setCourseInUser($courseId, $userId, $tutorId);
    }

    public function deleteCourseInUser(int $courseId, int $userId): void
    {
        $this->repository->deleteCourseInUser($courseId, $userId);
    }

    public function delete(int $courseId, bool $withTasks = true): void
    {
        try {
            $this->repository->beginTransaction();

            if ($withTasks) {
                $this->taskService->deleteByCourseId($courseId);
            }

            $this->repository->delete($courseId);

            $this->repository->commit();
        } catch (\Throwable $throwable) {
            $this->repository->rollBack();

            throw $throwable;
        }
    }
}
