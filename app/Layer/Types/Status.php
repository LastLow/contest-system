<?php

namespace App\Layer\Types;

enum Status: string
{
    case Zero = 'zero';
    case Error = 'error';
    case Success = 'success';
}
