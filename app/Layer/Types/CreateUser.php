<?php

namespace App\Layer\Types;

class CreateUser
{
    public function __construct(
        public string $name,
        public string $login,
        public string $password,
        public ?array $roles = [],
        public ?array $permissions = []
    )
    {
    }
}
