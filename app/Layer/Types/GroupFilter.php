<?php

namespace App\Layer\Types;

class GroupFilter
{
    public function __construct(
        public ?GroupFilterType $type,
        public ?int $page,
        public ?int $perPage,
        public ?int $courseId,
    )
    {
        $this->type = $type ?? GroupFilterType::IN;
        $this->page = $page ?: 1;
        $this->perPage = $perPage ?: 15;
    }
}
