<?php

namespace App\Layer\Types;

class CreateHistory
{
    public function __construct(
        public string $language,
        public string $code,
        public int    $max_score,
        public int    $taskId,
        public int    $userId
    ) {
    }
}
