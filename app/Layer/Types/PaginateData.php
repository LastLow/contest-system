<?php

namespace App\Layer\Types;

use Illuminate\Support\Collection;

class PaginateData
{
    public function __construct(
        public Collection $items,
        public int $total,
        public int $page,
        public int $lastPage,
        public int $perPage,
    )
    {
    }
}
