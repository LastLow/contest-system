<?php

namespace App\Layer\Types;

enum GroupFilterType: string {
    case IN = 'in';
    case OUT = 'out';
}
