<?php

namespace App\Layer\Types;

class CreateCourse
{
    public function __construct(
        public string $name
    ) {
    }
}
