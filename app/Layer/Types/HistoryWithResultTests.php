<?php

namespace App\Layer\Types;

use App\Layer\Implements\History\HistoryEntity;
use App\Layer\Implements\ResultTest\ResultTestEntity;
use Illuminate\Support\Collection;

class HistoryWithResultTests extends HistoryEntity
{
    public function __construct(HistoryEntity $entity, Collection $tests)
    {
        parent::__construct(
            $entity->id,
            $entity->code,
            $entity->status,
            $entity->score,
            $entity->max_score
        );

        $this->tests = $tests;
    }

    /**
     * @var Collection<ResultTestEntity>
     */
    public Collection $tests;
}
