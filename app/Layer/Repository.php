<?php

namespace App\Layer;

use App\Layer\Interfaces\Repositories\IRepository;
use App\Layer\Types\PaginateData;
use App\Models\Contracts\IModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

abstract class Repository implements IRepository
{
    protected IModel $model;

    public function __construct()
    {
        $this->model = new ($this->getModel());
    }

    public function beginTransaction(): void
    {
        DB::beginTransaction();
    }

    public function commit(): void
    {
        DB::commit();
    }

    public function rollback(): void
    {
        DB::rollBack();
    }

    abstract protected function getModel(): string;
    public function find(int $id)
    {
        return $this->getModelById($id)?->toEntity();
    }

    protected function getModelById(int $id): ?IModel
    {
        return $this->model::where('id', '=', $id)->first();
    }

    public function get(Builder $query = null, ?int $page = 1, ?int $perPage = 15): PaginateData
    {
        if (!isset($query)) $query = $this->model::query();

        $total = $query->count();

        if (!$total) return new PaginateData(collect(), 0, 1, 1, 0);

        $lastPage = max(ceil($total / $perPage), 1);

        if (isset($page)) {
            $query->offset(($page * $perPage) - $perPage);
        }

        $items = $query->limit($perPage)->get()
            ->map(fn (IModel $model) => $model->toEntity());

        return new PaginateData(
            $items,
            $total,
            $page,
            $lastPage,
            ($items->count() < $perPage) ? $items->count() : $perPage
        );
    }

    public function delete(int $id): void
    {
        $this->getModelById($id)->delete();
    }
}
