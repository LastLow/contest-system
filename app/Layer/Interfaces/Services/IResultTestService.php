<?php

namespace App\Layer\Interfaces\Services;

use App\Layer\Implements\ResultTest\ResultTestEntity;
use Illuminate\Support\Collection;

interface IResultTestService
{
    /** @return Collection<ResultTestEntity> */
    public function getTestsByHistoryId(int $historyId): Collection;
}
