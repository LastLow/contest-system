<?php

namespace App\Layer\Interfaces\Services;

use App\DTOs\CreateTaskDto;
use App\DTOs\TaskDto;
use App\DTOs\UpdateTaskDto;
use App\DTOs\UserTaskInfoDto;
use Illuminate\Support\Collection;

interface ITaskService
{
    public function getTask(int $id): TaskDto;
    public function getTasksByCourseId(int $courseId): Collection;
    /**
     * @param int $courseId
     * @param int $userId
     * @return Collection<UserTaskInfoDto>
     */
    public function getTasksInCourseByUser(int $courseId, int $userId): Collection;
    public function getTaskByUser(int $taskId, int $userId);
    public function updateOrCreateTask(CreateTaskDto|UpdateTaskDto $updateTaskDto): TaskDto;
    public function setTaskInUser(int $taskId, int $userId, int $courseId): void;
    public function deleteTasksInCourseByUser(int $userId, int $courseId): void;
    public function deleteByCourseId(int $courseId): void;
    public function delete(int $id): void;
}
