<?php

namespace App\Layer\Interfaces\Services;

use App\DTOs\UserCourseInfoDto;
use App\DTOs\UserDto;
use App\DTOs\UserInfo;
use App\Layer\Implements\User\UserEntity;
use App\Layer\Types\CreateUser;
use Illuminate\Support\Collection;

interface IUserService
{
    public function getUsers(): Collection;
    public function getInfoUserById(int $userId): UserDto;
    public function create(CreateUser $createUser): UserEntity;
    /**
     * @return Collection<UserInfo>
     */
    public function getUsersByGroup(int $groupId): Collection;
    public function getInfoAboutCourse(UserInfo $user, int $courseId): UserCourseInfoDto;
}
