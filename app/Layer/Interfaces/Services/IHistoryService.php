<?php

namespace App\Layer\Interfaces\Services;

use App\DTOs\CheckCodeDto;
use App\Layer\Implements\History\HistoryEntity;
use App\Layer\Types\HistoryWithResultTests;
use Illuminate\Support\Collection;

interface IHistoryService
{
    public function getLastHistoryByTaskNUser(int $taskId, int $userId): ?HistoryEntity;

    /** @return Collection<HistoryWithResultTests> */
    public function getHistoriesWithResultTestByTaskNUser(int $taskId, int $userId): Collection;
    public function createCheckCode(CheckCodeDto $checkCodeDto, int $userId): void;
}
