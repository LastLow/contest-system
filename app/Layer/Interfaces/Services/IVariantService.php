<?php

namespace App\Layer\Interfaces\Services;

use App\DTOs\CreateVariantDto;
use App\DTOs\VariantDto;
use App\Layer\Implements\Variant\VariantEntity;
use Illuminate\Support\Collection;

interface IVariantService
{
    public function getVariantsByTaskId(int $taskId): Collection;
    public function getVariantCountByTask(int $taskId): int;
    public function getVariantByTaskNUser(int $taskId, int $userId): VariantEntity;
    public function getMaxScoreById(int $variantId): int;
    public function updateOrCreate(int $taskId, CreateVariantDto|VariantDto $variantDto): VariantEntity;
    public function deleteByTaskId(int $taskId): void;
}
