<?php

namespace App\Layer\Interfaces\Services;

use App\DTOs\CreateGroupDto;
use App\DTOs\GroupDto;
use App\DTOs\SetCourseDto;
use App\Layer\Types\GroupFilter;
use App\Layer\Types\PaginateData;

interface IGroupService
{
    public function get(GroupFilter $filter): PaginateData;
    public function create(CreateGroupDto $dto): GroupDto;
    public function setCourseToGroup(SetCourseDto $dto, int $groupId): void;
    public function deleteGroup(int $id): void;
    public function deleteCourseFromGroup(int $groupId, int $courseId): void;
}
