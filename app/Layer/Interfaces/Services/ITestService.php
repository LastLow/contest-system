<?php

namespace App\Layer\Interfaces\Services;

use App\DTOs\CreateTestDto;
use App\DTOs\TestDto;
use App\Layer\Implements\Test\TestEntity;
use Illuminate\Support\Collection;

interface ITestService
{
    /**
     * @param $variantId
     * @return Collection<TestDto>
     */
    public function getTestsByVariantId($variantId): Collection;
    public function updateOrCreate(int $variantId, CreateTestDto|TestDto $test): TestEntity;
    public function deleteByVariantId(int $variantId): void;
}
