<?php

namespace App\Layer\Interfaces\Services;

use App\DTOs\ChangeCourseDto;
use App\DTOs\UserCourseDto;
use App\Layer\Implements\Course\CourseEntity;
use Illuminate\Support\Collection;

interface ICourseService
{
    public function getCourses(): Collection;

    /** @return Collection<UserCourseDto> */
    public function getCoursesByUser(int $userId): Collection;
    public function create(ChangeCourseDto $dto): CourseEntity;
    public function setCourseInUser(int $courseId, int $userId, int $tutorId = null): void;
    public function update(int $courseId, ChangeCourseDto $dto): CourseEntity;
    public function deleteCourseInUser(int $courseId, int $userId): void;
    public function delete(int $courseId, bool $withTasks): void;
}
