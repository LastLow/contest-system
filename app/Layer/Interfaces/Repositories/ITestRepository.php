<?php

namespace App\Layer\Interfaces\Repositories;

use App\DTOs\CreateTestDto;
use App\DTOs\TestDto;
use App\Layer\Implements\Test\TestEntity;
use Illuminate\Support\Collection;

interface ITestRepository extends IRepository
{
    /**
     * @param $variantId
     * @return Collection<TestEntity>
     */
    public function getTestsByVariantId($variantId): Collection;
    public function updateOrCreate(int $variantId, CreateTestDto|TestDto $test): TestEntity;
    public function deleteByVariantId(int $variantId): void;
}
