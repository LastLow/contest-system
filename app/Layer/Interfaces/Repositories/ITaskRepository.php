<?php

namespace App\Layer\Interfaces\Repositories;

use App\DTOs\CreateTaskDto;
use App\DTOs\UpdateTaskDto;
use App\Layer\Implements\Task\TaskEntity;
use Illuminate\Support\Collection;

interface ITaskRepository extends IRepository
{
    public function findTaskByUser(int $taskId, int $userId): TaskEntity;
    /**
     * @param int $courseId
     * @param int $userId
     * @return Collection<TaskEntity>
     */
    public function getTasksInCourseByUser(int $courseId, int $userId): Collection;
    public function getTasksByCourseId(int $courseId): Collection;
    public function getCountTasksInCourseByUser(int $courseId, int $userId): int;
    public function getSuccessTasksInCourseByUser(int $courseId, int $userId): int;
    public function setTaskInUser(int $taskId, int $userId, int $courseId, int $variantId): void;
    public function updateOrCreate(CreateTaskDto|UpdateTaskDto $taskDto): TaskEntity;
    public function deleteTasksInCourseByUser(int $userId, int $courseId): void;
}
