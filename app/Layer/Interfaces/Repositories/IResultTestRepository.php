<?php

namespace App\Layer\Interfaces\Repositories;

use App\Layer\Implements\ResultTest\ResultTestEntity;
use Illuminate\Support\Collection;

interface IResultTestRepository extends IRepository
{
    /** @return Collection<ResultTestEntity> */
    public function getTestsByHistoryId(int $historyId): Collection;
}
