<?php

namespace App\Layer\Interfaces\Repositories;

use App\DTOs\LoginDto;

interface IAuthRepository
{
    public function getAuthorizeUserId(): int;
    public function login(LoginDto $loginDto): string;
    public function logout(): void;
}
