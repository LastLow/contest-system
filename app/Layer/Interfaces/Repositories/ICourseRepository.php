<?php

namespace App\Layer\Interfaces\Repositories;

use App\Layer\Implements\Course\CourseEntity;
use App\Layer\Types\CreateCourse;
use Illuminate\Support\Collection;

interface ICourseRepository extends IRepository
{
    public function create(CreateCourse $createCourse): CourseEntity;
    /**
     * @return Collection<CourseEntity>
     */
    public function getCoursesByUserId(int $id): Collection;
    public function update(int $id, CreateCourse $createCourse): CourseEntity;
    public function setCourseInUser(int $courseId, int $userId, int $tutorId = null): void;
    public function deleteCourseInUser(int $courseId, int $userId): void;
}
