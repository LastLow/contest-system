<?php

namespace App\Layer\Interfaces\Repositories;

use App\Layer\Types\PaginateData;
use Illuminate\Database\Eloquent\Builder;

interface IRepository
{
    public function find(int $id);
    public function get(Builder $query = null, ?int $page = null, ?int $perPage = 15): PaginateData;
    public function delete(int $id): void;
    public function beginTransaction(): void;
    public function commit(): void;
    public function rollback(): void;
}
