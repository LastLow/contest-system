<?php

namespace App\Layer\Interfaces\Repositories;

use App\DTOs\LoginDto;
use App\DTOs\UserWithTokenDto;

interface IAuthService
{
    public function getAuthorizeUserId(): int;
    public function login(LoginDto $loginDto): UserWithTokenDto;
    public function logout(): void;
}
