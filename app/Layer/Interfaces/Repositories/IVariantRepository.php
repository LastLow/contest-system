<?php

namespace App\Layer\Interfaces\Repositories;

use App\Layer\Implements\Variant\VariantEntity;
use Illuminate\Support\Collection;

interface IVariantRepository extends IRepository
{
    /**
     * @param int $taskId
     * @return Collection<VariantEntity>
     */
    public function getVariantsByTaskId(int $taskId): Collection;
    public function getVariantByTaskNUser(int $taskId, int $userId): VariantEntity;
    public function getVariantCountByTask(int $taskId): int;
    public function getMaxScoreById(int $variantId): int;
}
