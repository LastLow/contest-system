<?php

namespace App\Layer\Interfaces\Repositories;

use App\DTOs\CreateGroupDto;
use App\Layer\Implements\Group\GroupEntity;
use App\Layer\Types\GroupFilter;
use App\Layer\Types\PaginateData;

interface IGroupRepository extends IRepository
{
    public function create(CreateGroupDto $dto): GroupEntity;
    public function getGroups(GroupFilter $filter): PaginateData;
    public function findByName(string $name): GroupEntity|null;
    public function addUserInGroup(int $groupId, int $userId): void;
    public function addCourseInGroup(int $groupId, int $courseId): void;
    public function deleteCourseInGroup(int $groupId, int $courseId): void;
}
