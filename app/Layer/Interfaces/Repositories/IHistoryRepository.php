<?php

namespace App\Layer\Interfaces\Repositories;

use App\Layer\Implements\History\HistoryEntity;
use App\Layer\Types\CreateHistory;
use Illuminate\Support\Collection;

interface IHistoryRepository extends IRepository
{
    public function getLastHistoryByTaskNUser(int $taskId, int $userId): ?HistoryEntity;

    /** @return Collection<HistoryEntity> */
    public function getHistoriesByTaskNUser(int $taskId, int $userId): Collection;
    public function create(CreateHistory $createHistory): void;
}
