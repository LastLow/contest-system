<?php

namespace App\Layer\Interfaces\Repositories;

use App\Layer\Implements\User\UserEntity;
use App\Layer\Types\CreateUser;
use Illuminate\Support\Collection;

interface IUserRepository extends IRepository
{
    public function getRolesUserById(int $userId): ?array;
    public function getPermissionsUserById(int $userId): ?array;
    public function getUsersByGroup(int $groupId): Collection;
    public function create(CreateUser $createUser): UserEntity;
}
