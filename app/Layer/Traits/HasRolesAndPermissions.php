<?php

namespace App\Layer\Traits;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait HasRolesAndPermissions
{
    /**
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'users_roles');
    }

    /**
     * @return BelongsToMany
     */
    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class, 'users_permissions');
    }

    public function hasRole(...$roles): bool
    {
        foreach ($roles as $role) {
            if ($this->roles->contains('name', $role)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $permission
     * @return bool
     */
    private function hasOnlyPermission($permission): bool
    {
        return (bool)$this->permissions->where('name', $permission)->count();
    }

    /**
     * Проверка имеется ли право у пользователя
     *
     * @param $permissionName
     * @return bool
     */
    public function hasPermission($permissionName): bool
    {
        $permission = Permission::where('name', '=', $permissionName)->first();
        if (!$permission) return false;

        return $this->hasPermissionThroughRole($permission) || $this->hasOnlyPermission($permission->name);
    }


    public function getAllPermissionsInUser()
    {
        $permissions = $this->permissions;

        foreach ($this->roles as $role) {
            foreach ($role->permissions as $permission) {
                $permissions->push($permission);
            }
        }

        return $permissions;
    }

    /**
     * @param Permission $permission
     * @return bool
     */
    public function hasPermissionThroughRole(Permission $permission)
    {
        foreach ($permission->roles as $role) {
            if ($this->roles->contains($role)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param array $permissions
     * @return mixed
     */
    public function getAllPermissions(array $permissions)
    {
        return Permission::whereIn('name', $permissions)->get();
    }

    /**
     * @param mixed ...$permissions
     * @return $this
     */
    public function givePermissionsTo(...$permissions)
    {
        $permissions = $this->getAllPermissions($permissions);
        if ($permissions === null) {
            return $this;
        }
        $this->permissions()->saveMany($permissions);
        return $this;
    }

    /**
     * @param mixed ...$permissions
     * @return $this
     */
    public function deletePermissions(... $permissions )
    {
        $permissions = $this->getAllPermissions($permissions);
        $this->permissions()->detach($permissions);
        return $this;
    }

    /**
     * @param mixed ...$permissions
     * @return HasRolesAndPermissions
     */
    public function refreshPermissions(... $permissions )
    {
        $this->permissions()->detach();
        return $this->givePermissionsTo($permissions);
    }
}
