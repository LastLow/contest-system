<?php

namespace App\Providers;

use App\DTO\ChangeTask;
use App\DTO\CheckCode;
use App\DTO\Collections\CreateCourseTasksCollection;
use App\DTO\Collections\SetCourseCollection;
use App\DTO\CreateGroup;
use App\DTO\CreateTask;
use App\DTO\Login;
use App\Exceptions\HandleException;
use App\Layer\Implements\Auth\AuthRepository;
use App\Layer\Implements\Auth\AuthService;
use App\Layer\Implements\Course\CourseRepository;
use App\Layer\Implements\Course\CourseService;
use App\Layer\Implements\Group\GroupRepository;
use App\Layer\Implements\Group\GroupService;
use App\Layer\Implements\History\HistoryRepository;
use App\Layer\Implements\History\HistoryService;
use App\Layer\Implements\ResultTest\ResultTestRepository;
use App\Layer\Implements\ResultTest\ResultTestService;
use App\Layer\Implements\Task\TaskRepository;
use App\Layer\Implements\Task\TaskService;
use App\Layer\Implements\Test\TestRepository;
use App\Layer\Implements\Test\TestService;
use App\Layer\Implements\User\UserRepository;
use App\Layer\Implements\User\UserService;
use App\Layer\Implements\Variant\VariantRepository;
use App\Layer\Implements\Variant\VariantService;
use App\Layer\Interfaces\Repositories\IAuthRepository;
use App\Layer\Interfaces\Repositories\IAuthService;
use App\Layer\Interfaces\Repositories\ICourseRepository;
use App\Layer\Interfaces\Repositories\IGroupRepository;
use App\Layer\Interfaces\Repositories\IHistoryRepository;
use App\Layer\Interfaces\Repositories\IResultTestRepository;
use App\Layer\Interfaces\Repositories\ITaskRepository;
use App\Layer\Interfaces\Repositories\ITestRepository;
use App\Layer\Interfaces\Repositories\IUserRepository;
use App\Layer\Interfaces\Repositories\IVariantRepository;
use App\Layer\Interfaces\Services\ICourseService;
use App\Layer\Interfaces\Services\IGroupService;
use App\Layer\Interfaces\Services\IHistoryService;
use App\Layer\Interfaces\Services\IResultTestService;
use App\Layer\Interfaces\Services\ITaskService;
use App\Layer\Interfaces\Services\ITestService;
use App\Layer\Interfaces\Services\IUserService;
use App\Layer\Interfaces\Services\IVariantService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        foreach ($this->requestDTOs as $abstract => $class) {

            $reflection = new \ReflectionClass($abstract);
            $constructor = $reflection->getConstructor();

            if (!$constructor) continue;

            $this->app->bind($abstract, function ($app) use ($class, $constructor) {
                try {

                    $paramsClass = array_column($constructor->getParameters(), 'name');

                    if (count($paramsClass) == 1 && $paramsClass[0] == "data") {
                        return new $class($app['request']->all());
                    }

                    $params = $app['request']->only(array_column($constructor->getParameters(), 'name'));

                    $requiredParameters = array_filter(array_map(function ($param) {
                        if (!$param->isOptional()) {
                            return $param->name;
                        }

                        return null;
                    }, $constructor->getParameters()));

                    if (count($params) != count($requiredParameters)) {
                        $missingParams = array_diff(
                            $requiredParameters,
                            array_keys($app['request']->only($requiredParameters))
                        );

                        if (!empty($missingParams)) {
                            throw new \ArgumentCountError();
                        }
                    }

                    return new $class(...array_values($params));
                } catch (\ArgumentCountError $exception) {
                    $requiredParams = array_filter(array_map(function ($param) {
                        if (!$param->isOptional()) {
                            return $param->name;
                        }

                        return null;
                    }, $constructor->getParameters()));

                    $missingParams = array_diff(
                        $requiredParams,
                        array_keys($app['request']->only($requiredParams))
                    );

                    throw new HandleException('Пропущенны значения: ' . implode(', ', $missingParams), 400);
                }
            });
        }


        $this->app->bind(ICourseRepository::class, CourseRepository::class);
        $this->app->bind(IUserRepository::class, UserRepository::class);
        $this->app->bind(ITaskRepository::class, TaskRepository::class);
        $this->app->bind(IHistoryRepository::class, HistoryRepository::class);
        $this->app->bind(IVariantRepository::class, VariantRepository::class);
        $this->app->bind(IResultTestRepository::class, ResultTestRepository::class);
        $this->app->bind(IAuthRepository::class, AuthRepository::class);
        $this->app->bind(ITestRepository::class, TestRepository::class);
        $this->app->bind(IGroupRepository::class, GroupRepository::class);

        $this->app->bind(ICourseService::class, CourseService::class);
        $this->app->bind(ITaskService::class, TaskService::class);
        $this->app->bind(IHistoryService::class, HistoryService::class);
        $this->app->bind(IVariantService::class, VariantService::class);
        $this->app->bind(IResultTestService::class, ResultTestService::class);
        $this->app->bind(IUserService::class, UserService::class);
        $this->app->bind(IAuthService::class, AuthService::class);
        $this->app->bind(ITestService::class, TestService::class);
        $this->app->bind(IGroupService::class, GroupService::class);
    }

    private array $requestDTOs = [
        'App\DTO\Login' => Login::class,
        'App\DTO\Collections\CreateCourseTasksCollection' => CreateCourseTasksCollection::class,
        'App\DTO\CheckCode' => CheckCode::class,
        'App\DTO\Collections\SetCourseCollection' => SetCourseCollection::class,
        'App\DTO\CreateGroup' => CreateGroup::class,
        'App\DTO\CreateTask' => CreateTask::class,
        'App\DTO\ChangeTask' => ChangeTask::class
    ];

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }


}
