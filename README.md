# Contest

## Сборка проекта

### Клонирование реп
```
git clone https://gitlab.com/vsitchikhin/contest-frontend-quasar.git
git clone https://gitlab.com/LastLow/contest-system.git
```
так чтобы директории были вида
```
├── /
│ ├── contest-frontend-quasar
│ ├── contest-system
```

### Настройка проекта
 - ### Frontend
```
cd contest-frontend-quasar # переходим в директорию фронта
touch .env # создаем файл
```
В него необходимо записать внешний хост на котором будет приложение
```
API_HOST=your_host # example http://localhost:9000
```

- ### Backend
```
cd ../contest-system
```
Необходимо изменить docker-compose.yml изменить пароль к базе

А также внешний порт, если это необходимо
```
mysql:
    ...
    environment:
        MYSQL_DATABASE: contest_system
        MYSQL_ROOT_PASSWORD=your_password <- Изменение пароля
        MYSQL_TABLE_OPEN_CACHE: 400
        MYSQL_LOWER_CASE_TABLE_NAMES: 1

nginx:
    ...
    ports:
        - "8876 <- Изменение внешнего порта :80"
    container_name: app_nginx

```
 - Настройка сервиса проверки

Необходимо изменить ```"db_info"."password"``` в ./services/ContestService/contest_server.json

Настройка env
```
cp .env.example .env # Копируем .env 
```
В нем необходимо изменить следующие переменные
```
APP_URL=http://localhost:8876 # внешний хост на котором будет приложение

ADMIN_PASSWORD=123456 

DB_PASSWORD=root # пароль базы, который был изменен в docker-compose.yml

L5_SWAGGER_CONST_HOST=http://localhost:8876 # внешний хост 
L5_SWAGGER_BASE_PATH=localhost:8876 # внешний хост 
```
 - ### Переход к установке
```
[WORKDIR contest-system]

docker-compose up -d 

docker exec -it app bash

> cd frontend
> npm install && npx quasar build # установка зависимостей и билд frontend'а

> cd ../backend
> composer install # установка зависимостей
> chmod 777 -R ./ # изменение прав
> php artisan migrate # запуск миграции
> php artisan db:seed MainSeeder # запуск заполнения базы необходимыми данными
> php artisan l5:generate # генерация Swagger
```
