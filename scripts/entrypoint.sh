#!/bin/sh
set -e # Завершение работы если команда вернула ошибку

cd /var/wwwfrontend
echo "Инициализация фронтенд"
npm install && npx quasar build

cd /var/www/backend
echo "Запуск composer"
composer install

echo "Запуск npm"
npm install
npm run build

while ! nc -z "$DB_HOST" $DB_PORT; do
    sleep 1
done

echo "Запуск миграции"
php artisan migrate

echo "Запуск сидеров"
php artisan db:seed MainSeeder

echo "генерация Swagger"
php artisan l5:generate

exec php-fpm # Прододжение работы контейнера через php-fpm

#php artisan serve --port=8080 --host=0.0.0.0
